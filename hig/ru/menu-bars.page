<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="menu-bars" xml:lang="ru">

  <info>
    <credit type="author">
      <name>Алан Дэй (Allan Day)</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Калум Бенсон (Calum Benson)</name>
    </credit>
    <credit>
      <name>Адам Элман (Adam Elman)</name>
    </credit>
    <credit>
      <name>Сэт Никел (Seth Nickell)</name>
    </credit>
    <credit>
      <name>Колин Робертсон (Colin Robertson)</name>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/menu-bar.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Панели меню</title>

<p>Панель меню представляет собой полосу из выпадающих меню. Панель меню обычно располагается в верхней части главного окна под заголовочной панелью окна.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/menu-bar.svg"/>

<section id="when-to-use">
<title>Когда использовать</title>

<p>Под панель меню отводится дополнительное место по вертикали, она содержит множество точек раскрытия. В панели располгается фиксированный набор параметров. По этим причинам вместо панели меню рекомендуется применять <link xref="header-bars">заголовочные панели</link> и <link xref="header-bar-menus">меню заголовочной панели</link>, а также другие шаблоны проектирования, которые могут управлять появлением элементами интерфейса, например <link xref="selection-mode">режим выделения</link>, <link xref="action-bars">панель действий</link> и <link xref="popovers">всплывающие виджеты</link>.</p>

<p>В то же время, применение панели меню уместно в сложных приложениях, в которых уже есть панель меню. Кроме того, некоторые платформы включают область для панели меню в свои пользовательские окружения, и модель меню может оказаться полезной при кросс-платформенной интеграции.</p>

</section>

<section id="standard-menus">
<title>Стандартные меню</title>

<p>В этом разделе даётся описание наиболее распространённых меню и его элементов в панели меню. Подробнее о том, какие стандартные элементы включать в каждое из этих меню, см. раздел «<link xref="keyboard-input#application-shortcuts">Ввод с клавиатуры</link>».</p>

<table>
<tr>
<td><p><gui>Файл</gui></p></td>
<td><p>Команды, управляющие текущим документом или самим содержимым. Находится слева от остальных элементов в панели меню, поскольку является наиболее важным и часто используемым элементом. Этот элемент применяется во многих приложениях.</p>
<p>Если приложение не работает с файлами, назовите это меню по типу объекта, котоый оно показывает. Например, в музыкальном прогрывателе меню <gui>Файл</gui> может называться <gui>Музыка</gui>.</p></td>
</tr>
<tr>
<td><p><gui>Правка</gui></p></td>
<td><p>Меню <gui>Правка</gui> содержит элементы, которые связаны с редактированием документа, например обработка буфера обмены, поиск и замена, вставка объектов.</p></td>
</tr>
<tr>
<td><p><gui>Вид</gui></p></td>
<td><p>Содержит элементы, которые управляют внешним видом, например режимом отображения текущего документа или страницы или способом представления элементов. Меню <gui>Вид</gui> не должно содержать каких-либо элементов, которые влияют на содержимое.</p></td>
</tr>
<tr>
<td><p><gui>Вставка</gui></p></td>
<td><p>Содержит список типов объектов, которые можно вставлять в текущий документ (например, изображения, ссылки или разрывы страниц). Предоставляйте это меню, если количество типов объектов, которые можно вставлять, больше шести, иначе элементы типов объектов должны размещаться в меню <gui>Правка</gui>.</p></td>
</tr>
<tr>
<td><p><gui>Формат</gui></p></td>
<td><p>Содержит команды для изменения внешнего вида документа. Например, изменение шрифта, цвета, межстрочный интервал выделенного текста.</p>
<p>Различие между этими командами и командами из меню <gui>Вид</gui> в том, что изменения, совершаемые с помощью команд из меню <gui>Формат</gui>, являются постоянными и сохраняются как часть документа.</p>
<p>Элементы в меню <gui>Формат</gui> бывают очень разными.</p></td>
</tr>
<tr>
<td><p><gui>Закладки</gui></p></td>
<td><p>Предоставьте меню <gui>Закладки</gui>, если в приложении можно просматривать файлы и папки, справочные документы, веб-страницы или другие большие информационные области.</p></td>
</tr>
<tr>
<td><p><gui>Переход</gui></p></td>
<td><p>Меню <gui>Переход</gui> содержит команды для быстрого перемещения по одному или нескольким документам или по большим информационным областям таким как деревья каталогов или веб-ресуры.</p>
<p>Содержимое этого меню сильно зависит от типа приложения.</p></td>
</tr>
<tr>
<td><p><gui>Окна</gui></p></td>
<td><p>Команды, применяемые ко всех открытых окнам приложения. Это меню можно переименовать в <gui>Документы</gui>, <gui>Буферы</gui> или какое-нибудь другое называние, связанное с типом документа, с которым работает приложение.</p>
<p>В конце меню должен находиться пронумерованный список главных окон приложения (например, <gui>1список_покупок.abw</gui>). Выбор любого из этих элементов показывает соответствующее окно.</p></td>
</tr>
</table>

</section>

<section id="general-guidelines">
<title>Общие рекомендации</title>

<list>
<item><p>The menubar is normally visible at all times and is always accessible from the keyboard, so make all the commands available in your application available on the menubar. (This guideline is unique to menu bars — other menus should not seek to reproduce functionality that is made available by other controls).</p></item>
<item><p>Treat <link xref="application-menus">application menus</link> as part of the menu bar — it is not necessary to reproduce items from the application menu in other menus.</p></item>
<item><p>Не выключайте заголовки меню. Позвольте пользователю исследовать меню, даже если на данный момент в нём нет доступных элементов.</p></item>
<item><p>В качестве заголовка меню используйте одно слово, записанное с прописной буквы. Не используйте пробел в заголовках, поскольку это может внести путаницу, и одно меню будет выглядеть как два. Не склеивайте слова (например, <gui>ПараметрыОкна</gui>) и не используйте дефисное написание (например, <gui>Параметры-окна</gui>).</p></item>
<item><p>Не используйте механизм для скрытия панели меню, пользователь может ненарочно скрыть панель меню и будет искать, как вернуть её обратно.</p></item>
</list>

</section>

<section id="api-reference">
<title>API reference</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkMenu.html">GtkMenu</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkMenuBar.html">GtkMenuBar</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkMenuItem.html">GtkMenuItem</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkRadioMenuItem.html">GtkRadioMenuItem</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkCheckMenuItem.html">GtkCheckMenuItem</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkSeparatorMenuItem.html">GtkSeparatorMenuItem</link></p></item>
</list>
</section>

</page>
