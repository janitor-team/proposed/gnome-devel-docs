<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="pointer-and-touch-input" xml:lang="ru">

  <info>
    <credit type="author">
      <name>Алан Дэй (Allan Day)</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Якуб Штайнер (Jakub Steiner)</name>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <desc>Работа с мышью, сенсорными панелью и экраном.</desc>
  </info>

<title>Указатель и сенсорный ввод</title>

<p>Указатель и сенсорный ввод — два главных способа взаимодействия с вашим приложением.</p>

<section id="pointer-input">
<title>Ввод с помощью указателя</title>

<p>A pointing device is any input device that allows the manipulation of a pointer — typically represented as an arrow, and often called a cursor — on screen. While mice and touchpads are the most common, there are a wide variety of such devices, including graphics tablets, track balls, track points and joysticks.</p>

<section id="primary-and-secondary-buttons">
<title>Главная и вспомогательная кнопки</title>

<p>У мышей и сенсорных панелей чаще всего имеется две основные кнопки. Одна из кнопок называется главной, а вторая — вспомогательной. Левая кнопка обычно используется в качестве главной, а правая — вспомогательной. Назначение кнопок можно поменять местами. Переназначение кнопок не влияет на ввод с помощью сенсорной панели. В этом руководстве используются понятия правой и вспомогательной кнопок, а не левой и правой. </p>

<p>Основное действие используется для выделения элементов и активации управляющих элементов. Вспомогательное действие используется для доступа к дополнительным параметрам, обычно через контекстное меню.</p>

<p>Не привязывайте ввод к вспомогательным или каким-либо другим дополнительным кнопкам. Мало того что дополнительную кнопку сложнее нажать, некоторые указующие устройства и вспомогательные устройства поддерживают или эмулируют только основную кнопку.</p>

<p>На однокнопочных устройствах вспомогательная кнопка должна эмулироваться удерживанием главной кнопки. Не используйте удерживание для каких-либо других целей.</p>

</section>

<section id="general-guidelines">
<title>Общие рекомендации</title>

<list>
<item><p>Не используйте двойной щелчок, так как он неудобен при использовании сенсорного ввода, а контекст его применения неочевиден.</p></item>
<item><p>Если у мыши есть колесо прокрутки, то оно должно прокручивать окно или управляющий элемент под указателем, если этот элемент поддерживает прокрутку. Прокрутка выполняемая этим способом не переводит фокус клавиатуры на прокручиваемое окно или управляющий элемент.</p></item>
<item><p>Не требуйте от пользователя одновременного нажатия несколько кнопок мыши для каких-либо действий.</p></item>
<item><p>Не требуйте от пользователя многократного нажатия (больше двух) для каких-либо действий, только если вы не продумали альтернативный способ выполнения данного действия.</p></item>
<item><p>Любые незавершённые действия с мышью должны иметь возможность отмены. Нажатие клавиши <key>Esc</key> должно отменять любые незавершённые действия с мышью: перетаскивание файлов в файловом менеджере, рисование фигур в графическом редакторе и т. п.</p></item>
<item><p>Не используйте в интерфейсе жёсткую привязку определённых кнопок мыши, если только это не действительно необходимо. Не все используют стандартную привязку кнопок мыши к левой, средней и правой, поэтому любой текст или диаграммы, которые используют жёсткую привязку к определённым кнопкам мыши, могут сбивать с толку.</p></item>
</list>

</section>

<section id="mouse-and-keyboard-equivalents">
<title>Дублирование управления с клавиатуры</title>

<p>Убедитесь, что все действия в вашем приложении, которые можно выполнять с помощью мыши, также могут быть выполнены с помощью клавиатуры. Единственным исключением являются действия, для выполнения которых требуется точная моторика. Например, для управления движением в некоторых играх или для рисования от руки в графических редакторах.</p>

<p>Если в вашем приложении используется выделение элементов, то в нём должна быть поддержка следующих родственных действий.</p>

<table>
<thead>
<tr>
<td><p>Действие</p></td>
<td><p>Мышь</p></td>
<td><p>Клавиатура</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p>Открыть элемент</p></td>
<td><p>Главная кнопка</p></td>
<td><p><key>Space</key></p></td>
</tr>
<tr>
<td><p>Добавить/убрать элемент из выделения</p></td>
<td><p><key>Ctrl</key> и главная кнопка</p></td>
<td><p><keyseq><key>Ctrl</key><key>Space</key></keyseq></p></td>
</tr>
<tr>
<td><p>Расширить выделение</p></td>
<td><p><key>Shift</key> и главная кнопка</p></td>
<td><p><key>Shift</key> в комбинации с любой из следующих клавиш: <key>Space</key> <key>Home</key> <key>End</key> <key>PageUp</key> <key>PageDown</key></p></td>
</tr>
<tr>
<td><p>Изменить выделение</p></td>
<td><p>Главная кнопка</p></td>
<td><p>Одна из кнопок: <key>←</key> <key>↑</key> <key>→</key> <key>↓</key> <key>Home</key> <key>End</key> <key>PageUp</key> <key>PageDown</key></p></td>
</tr>
<tr>
<td><p>Выделить всё</p></td>
<td><p>Выбрать главной кнопкой первый элемент, затем выбрать главной кнопкой последний элемент, удерживая клавишу <key>Shift</key></p></td>
<td><p><keyseq><key>Ctrl</key><key>A</key></keyseq></p></td>
</tr>
<tr>
<td><p>Убрать выделение</p></td>
<td><p>Нажать главную кнопку на фоне контейнера</p></td>
<td><p><keyseq><key>Shift</key><key>Ctrl</key><key>A</key></keyseq></p></td>
</tr>
</tbody>
</table>

</section>
</section>

<section id="touch-input">
<title>Сенсорный ввод</title>

<p>Touch screens are also an increasingly common part of modern computer hardware, and applications created with GTK are likely to be used with hardware that incorporates a touch screen. To make the most of this hardware, and to conform to users’ expectations, it is therefore important to consider touch input as a part of application design.</p>

<section id="application-touch-conventions">
<title>Прикладные соглашения о сенсорном управлении</title>

<p>Использование сенсорного ввода в вашем приложении не должно отличаться от других приложений, это упростит пользователям работу с вашим приложением. Рекомендуется следовать следующим соглашениям там, где это применимо.</p>

<table>
<thead>
<tr>
<td><p>Действие</p></td>
<td><p>Описание</p></td>
<td><p>Что делает</p></td>
</tr>
</thead>
<tbody>
<tr>
<td colspan="3"><p><em style="strong">Нажатие</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/png" src="figures/touch/tap.svg"/></td>
<td><p>Нажатие на элемент.</p></td>
<td><p>Primary action. Item opens — photo is shown full size, application launches, song starts playing.</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Нажатие с удержанием</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/png" src="figures/touch/tap-and-hold.svg"/></td>
<td><p>Нажать и держать секунду или две.</p></td>
<td><p>Вспомогательное действие. Выбрать элемент и вывести список доступных действий.</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Перетаскивание</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/png" src="figures/touch/drag.svg"/></td>
<td><p>Перенести палец, касаясь поверхности.</p></td>
<td><p>Прокручивает область на экране.</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Сведение или разведение</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/png" src="figures/touch/pinch-or-stretch.svg"/></td>
<td><p>Коснитесь поверхности двумя пальцами, затем сводите или разводите их.</p></td>
<td><p>Changes the zoom level of the view (e.g. Maps, Photos).</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Двойное нажатие</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/png" src="figures/touch/double-tap.svg"/></td>
<td><p>Дважды быстро коснуться экрана.</p></td>
<td><p>Последовательно приближает.</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Рывок</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/png" src="figures/touch/flick.svg"/></td>
<td><p>Очень быстрое перетаскивание с убиранием пальца от поверхности без замедления.</p></td>
<td><p>Удаляет элемент.</p></td>
</tr>
</tbody>
</table>

</section>

<section id="system-touch-conventions">
<title>Системные соглашения о сенсорном управлении</title>

<p>В GNOME 3 несколько жестов зарезервированы под системные нужды. Не используйте эти жесты в своих приложениях.</p>

<table>
<tr>
<td colspan="3"><p><em style="strong">Перетаскивание у границы</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/svg" src="figures/touch/edge-drag.svg"/></td>
<td><p>Проведите пальцем от границы экрана.</p></td>
<td><p>Верхняя левая граница открывает меню приложения.</p>
<p>Верхняя правая граница открывает системное меню.</p>
<p>Левая граница открывает «Обзор» в режиме отображения доступных приложений.</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Щипок тремя пальцами</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/svg" src="figures/touch/3-finger-pinch.svg"/></td>
<td><p>Сведите три или более пальцев друг к другу, касаясь при этом ими поверхности.</p></td>
<td><p>Открывает «Обзор».</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Перетаскивание четыремя пальцами</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/svg" src="figures/touch/4-finger-drag.svg"/></td>
<td><p>Перетаскивайте вверх или вниз четыремся пальцами, касаясь ими поверхности.</p></td>
<td><p>Переключает рабочее место.</p></td>
</tr>
<tr>
<td colspan="3"><p><em style="strong">Удерживание тремя пальцами с нажатием</em></p></td>
</tr>
<tr>
<td><media type="image" mime="image/svg" src="figures/touch/3-finger-hold-and-tap.svg"/></td>
<td><p>Hold three fingers on the surface while tapping with the fourth.</p></td>
<td><p>Переключает приложение.</p></td>
</tr>
</table>

</section>
</section>
</page>
