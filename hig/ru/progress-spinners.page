<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="progress-spinners" xml:lang="ru">

  <info>
    <credit type="author">
      <name>Алан Дэй (Allan Day)</name>
      <email>aday@gnome.org</email>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/progress-spinner.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Индикаторы ожидания</title>

<p>Индикатор ожидания — это стандартный элемент пользовательского интерфейса, который показывает о выполнении некоторой задачи. В отличие от индикатора выполнения индикатор ожидания демонстрирует сам факт наличия задачи, но не показывает, какая часть задачи выполнена.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/progress-spinner.svg"/>

<section id="when-to-use">
<title>Когда использовать</title>

<p>Индикатор выполнения обычно требуется, если операция занимает больше трёх секунд и необходимо явно показать, что она действительно выполняется. Иначе пользователь будет сомневаться, не произошла ли какая-нибудь ошибка.</p>

<p>С другой стороны индикаторы выполнения могут стать источником недоразумений, особенно когда они появляются на короткое время. Если операция занимает меньше трёх секунд, лучше использовать индикатор выполнения, поскольку элементы с анимацией, которые показываются на короткое время отвлекают пользователей от работы с приложением.</p>

<p>Индикаторы ожидания не показывают количественного выполнения задания, они лучше подходят для коротких операций. Если задание выполняется больше минуты, то вместо индикатора ожидания лучше применить <link xref="progress-bars">индикатор выполнения</link>.</p>

<p>Внешний вид индикатора ожидания определяет целесообразность применения индикатора в различных ситуациях. Индикаторы ожидания можно использовать в списках, заголовочных панелях, поскольку индикаторы имеют небольшие размеры и их легко встроить в указанные выше элементы. Их можно эффективно использовать в контейнерах квадратной и прямоугольной формы. Если вертикальная область сильно ограничена, то вместо индикатора ожидания можно применить индикатор выполнения.</p>

</section>

<section id="general-guidelines">
<title>Общие рекомендации</title>

<list>
<item><p>Если время выполнения операции не определено, в течение первых трёх секунд показывайте индикатор ожидания, если этого времени недостаточно для выполнения операции, покажите индикатор выполнения. Индикатор выполнения не нужно показывать, если время выполнения операции меньше трёх секунд.</p></item>
<item><p>Индикатор ожидания должен располагаться рядом с элементами интерфейса, с которыми он связан. Если нажатие на кнопку инициирует выполнение продолжительного действия, индикатор должен располгаться рядом с такой кнопкой. При загрузке содержимого индикатор должен располагаться в месте, в котором содержимое появится после загрузки.</p></item>
<item><p>Generally, only one progress spinner should be displayed at once. Avoid showing a large number of spinners simultaneously — this will often be visually overwhelming.</p></item>
<item><p>Рядом с индикатором ожидания можно расположить текстовую метку с описанием выполняемой задачи.</p></item>
<item><p>If a spinner is displayed for a long time, a label can indicate both the identity of the task and progress through it. This can take the form of a percentage, an indication of the time remaining, or progress through sub-components of the task (e.g. modules downloaded, or pages exported).</p></item>
</list>

</section>

<section id="api-reference">
<title>API reference</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkSpinner.html">GtkSpinner</link></p></item>
</list>
</section>

</page>
