<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="spin-boxes" xml:lang="ru">

  <info>
    <credit type="author">
      <name>Алан Дэй (Allan Day)</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Калум Бенсон (Calum Benson)</name>
    </credit>
    <credit>
      <name>Адам Элман (Adam Elman)</name>
    </credit>
    <credit>
      <name>Сэт Никел (Seth Nickell)</name>
    </credit>
    <credit>
      <name>Колин Робертсон (Colin Robertson)</name>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/spin-boxes.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Счётчики</title>

<p>Счётчик — это поле, принимающее некоторый диапазон значений. Он также содержит две кнопки, с помощью которых можно увеличивать и уменьшать значение.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/spin-boxes.svg"/>

<section id="when-to-use">
<title>Когда использовать</title>

<p>Счётчики служат для установки численных значений, смысл которых понятен пользователям, в противном случае вместо счётчика можно применить <link xref="sliders">ползунок</link>.</p>

<p>Счётчики используются только для численных значений. Для любых нечисленных параметров должны использоваться <link xref="lists">обычные списки</link>, <link xref="drop-down-lists">выпадающие списки</link> и т. п.</p>

</section>

<section id="sliders">
<title>Ползунки</title>

<p>В некоторых случаях значение счётчика можно привязать к положению ползунка: ползунок служит для установки примерного значения, а счётчик — для точного значения. Подобная комбинация имеет смысл в том случае, если пользователь должен иметь возможность видеть точные используемые значения. Ползунок должен использоваться, когда:</p>

<list>
<item><p>Значения в счётчике могут изменяться мгновенно (например, при редактировании изображений).</p></item>
<item><p>необходимо контролировать изменение значения в режиме реального времени. Например, при отслеживании эффектов изменения цвета, вызываемого перемещением ползунков в окне предварительного просмотра.</p></item>
</list>

</section>

<section id="general-guidelines">
<title>Общие рекомендации</title>

<list>
<item><p>Текстовая метка для счётчика должна располагаться слева от счётчика или над счётчиком. При использовании английского языка прописные буквы должны использоваться как в предложениях. Предоставьте ускоритель для метки, чтобы пользователь мог перевести фокус ввода на счётчик.</p></item>
<item><p>Содержимое счётчика должно быть выровнено по правой стороне, если используемая локаль не утверждает противоположного. Такое выравнивание упрощает сравнение нескольких численных значений, если они находятся в одном столбце. Правые границы соответствующих элементов управления также должны быть выровнены.</p></item>
</list>

</section>

<section id="api-reference">
<title>API reference</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkSpinButton.html">GtkSpinButton</link></p></item>
</list>

</section>

</page>
