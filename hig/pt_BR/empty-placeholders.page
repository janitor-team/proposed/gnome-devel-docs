<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="empty-placeholder" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="patterns#secondary"/>
    <desc>Imagem e texto mostrados quando uma grade ou lista está vazia</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Georges Neto</mal:name>
      <mal:email>georges.stavracas@gmail.com</mal:email>
      <mal:years>2014,</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

<title>Espaços reservados vazio</title>

<media type="image" mime="image/svg" src="figures/patterns/empty-placeholder.svg"/>

<p>Uma espaço reservado vazio é uma imagem e texto que preenche o espaço em uma lista ou grade vazia.</p>

<section id="when-to-use">
<title>Quando usar</title>

<p>Os espaços reservados vazios desempenham uma série de funções importantes: impedem a confusão e orientam o usuário e tornam sua interface melhor e mais coesa. Eles também são um desses toques agradáveis que ajudam a comunicar uma atenção aos detalhes.</p>

<p>Um espaço reservado vazio deve ser exibido sempre que uma lista ou grade está vazia.</p>

<p>Os espaços reservados vazios não devem ser exibidos quando um aplicativo está sendo executado pela primeira vez. Nessas situações, um estado vazio é muito negativo e <link xref="initial-state-placeholder">uma experiência mais rica, mais característica e positiva</link> é melhor.</p>

</section>

<section id="guidelines">
<title>Diretrizes</title>

<list>
<item><p>Siga o layout padrão para o tamanho e a colocação da imagem e rótulos, de modo que seu aplicativo seja consistente com outros aplicativos do GNOME 3.</p></item>
<item><p>Para a imagem, use um <link xref="icons-and-artwork#color-vs-symbolic">ícone simbólico</link> que representa o seu aplicativo ou o tipo de conteúdo que normalmente apareceria na grade ou na lista.</p></item>
<item><p>Um espaço reservado vazio deve sempre incluir um rótulo que comunica o estado vazio. Muitas vezes, é apropriado incluir um subtexto menor que forneça orientação adicional (como, por exemplo, como adicionar itens). No entanto, isso só deve ser incluído se houver informações adicionais que seja útil fornecer.</p></item>
<item><p>Se houver controles que permitam adicionar itens, pode ser apropriado destacá-los usando um <link xref="buttons#suggested-and-destructive">estilo sugerido</link> enquanto a lista/grade está vazia.</p></item>
</list>

</section>

</page>
