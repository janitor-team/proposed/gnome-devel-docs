<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="header-bars" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="patterns#primary"/>
    <desc>Elemento que é executado ao longo da parte superior das janelas</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Georges Neto</mal:name>
      <mal:email>georges.stavracas@gmail.com</mal:email>
      <mal:years>2014,</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

<title>Barras de cabeçalho</title>

<media type="image" mime="image/svg" src="figures/patterns/header-bar.svg"/>

<p>As barras de cabeçalho são um elemento horizontal comum que são colocados na parte superior das janelas. Eles desempenham vários papéis:</p>

<list>
<item><p>Controles de janela – as barras de cabeçalho permitem que as janelas sejam movidas arrastando, incluindo botões de controles de janela (normalmente um único botão de fechamento) e forneça acesso a um menu de controles de janela.</p></item>
<item><p>Cabeçalhos – um papel-chave de uma barra de cabeçalho é fornecer contexto para o conteúdo da janela, seja através de um título ou alternador de visão.</p></item>
<item><p>Controles – barras de cabeçalho fornecem um lugar para controles de chave, geralmente na forma de botões.</p></item>
</list>

<section id="when-to-use">
<title>Quando usar</title>

<p>As barras de cabeçalho são recomendadas para todas as janelas do aplicativo. Elas oferecem uma série de vantagens em relação à combinação tradicional de barra de título, barra de menus e barra de ferramentas, incluindo uma pegada vertical menor e mudanças dinâmicas de navegação e modo (como <link xref="selection-mode">modo de seleção</link>).</p>

<p>As barras de cabeçalho são incompatíveis com barras de menu. Se o seu aplicativo já incorpora uma barra de menu, você deve avaliar as alternativas sugeridas nestas diretrizes. Veja as <link xref="menu-bars">diretrizes da barra de menu</link> para obter mais detalhes sobre isso.</p>

</section>

<section id="controls">
<title>Controles</title>

<p>As barras de cabeçalho podem conter controles-chave para a janela, que podem ser colocados no lado esquerdo e direito da barra de cabeçalho. Exemplos desses controles incluem botões para navegar para trás e para frente, pesquisar e selecionar conteúdo.</p>

<p>Certifique-se de que sua barra de cabeçalho contém apenas um pequeno número de controles-chave – isso ajudará os usuários a entender a funcionalidade principal fornecida pela janela e garantirá que a janela pode ser redimensionada para larguras estreitas.</p>

<p>Se uma janela requer mais controles do que pode ser acomodado confortavelmente dentro da barra de cabeçalho, funcionalidades adicionais podem ser incluídas dentro de um menu de barra de cabeçalho.</p>

</section>

<section id="dynamic">
<title>Barras de cabeçalho são dinâmicas</title>

<p>Uma barra de cabeçalho pode – e deve – atualizar com as mudanças de visão ou modo. Isso garante que os controles da barra de cabeçalho sejam sempre relevantes para o contexto atual.</p>

<p>Se a janela incluir várias visões (acessadas através de um <link xref="view-switchers">alternador de visão</link>), a barra de cabeçalho pode incluir controles diferentes para cada visão.</p>

<p>Se a janela incorporar navegação, diferentes controles podem ser exibidos dependendo da localização exibida na própria janela. É comum mostrar um botão de voltar no lado esquerdo da barra de cabeçalho ao navegar.</p>

</section>

<section id="additional-guidance">
<title>Orientação adicional</title>

<list>
<item><p>Uma barra de cabeçalho sempre deve fornecer contexto para a janela a que pertence. Isso ajuda a identificar a janela e esclarece o que é exibido na própria janela. Isso pode ser feito colocando um título no centro da barra de cabeçalho, ou incluindo um <link xref="view-switchers">alternador de visão</link>.</p></item>
<item><p>Organize os controles dentro da barra de cabeçalho de acordo com os três pontos de alinhamento descritos nas diretrizes de <link xref="visual-layout">layout visual</link> – esquerda, centro e direita.</p></item>
<item><p>Botões <gui>New</gui> e de retorno devem ser colocados no lado esquerdo da barra de cabeçalho.</p></item>
<item><p>Certifique-se sempre de haver espaço para arrastar uma barra de cabeçalho. Isso é necessário para permitir que as janelas sejam movidas ou redimensionadas.</p></item>
</list>

</section>

<section id="api-reference">
<title>Referência de API</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkHeaderBar.html">GtkHeaderBar</link></p></item>
</list>

</section>

</page>
