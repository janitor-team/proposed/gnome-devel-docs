<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="grids" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="patterns#primary"/>
    <desc>Grades de miniaturas ou ícones</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Georges Neto</mal:name>
      <mal:email>georges.stavracas@gmail.com</mal:email>
      <mal:years>2014,</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

<title>Grades</title>

<media type="image" mime="image/svg" src="figures/patterns/grid.svg"/>

<p>Uma grade é um dos principais métodos de apresentação de coleções de conteúdo no GNOME 3. As grades geralmente são combinadas com uma série de outros padrões de design, incluindo <link xref="search">pesquisa</link> e <link xref="selection-mode">modo de seleção</link>.</p>

<section id="when-to-use">
<title>Quando usar</title>

<p>Uma vez que a visão de grade utiliza uma imagem para cada item que ela apresenta, ela é mais adequada ao conteúdo que tenha um componente visual, como documentos ou fotos. Se os itens de conteúdo não tiverem um componente visual, uma <link xref="lists">visão de lista</link> pode ser mais apropriada.</p>

<p>Grades e listas podem ser combinados, para oferecer visões diferentes do mesmo conteúdo. Isso pode ser útil se os itens de conteúdo tiverem metadados adicionais associados a eles, como datas de criação ou autoria.</p>

</section>

<section id="general-guidelines">
<title>Diretrizes gerais</title>

<list>
<item><p>Sempre que possível, cada item de conteúdo deve ter uma miniatura única.</p></item>
<item><p>Ordene os itens na grade de acordo com o que será mais útil para as pessoas que usam seu aplicativo. Ordenar o conteúdo de acordo com o uso mais recente é muitas vezes o melhor arranjo.</p></item>
<item><p>Selecionar um item na grade geralmente alternará para uma visão dedicada desse item.</p></item>
<item><p>Considere combinar a pesquisa de visão de grade, modo de seleção e coleções.</p></item>
</list>

</section>

<section id="api-reference">
<title>Referência de API</title>
<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkFlowBox.html">GtkFlowBox</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkIconView.html">GtkIconView</link></p></item>
</list>
</section>

</page>
