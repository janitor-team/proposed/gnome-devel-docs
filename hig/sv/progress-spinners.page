<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="progress-spinners" xml:lang="sv">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/progress-spinner.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2015, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

<title>Väntesnurror</title>

<p>Väntesnurror är ett vanligt element i användargränssnitt för att indikera förloppet för en uppgift. I kontrast till förloppsindikatorer indikerar de endast att framsteg inträffar och visar inte hur stor del av uppgiften som har färdigställts.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/progress-spinner.svg"/>

<section id="when-to-use">
<title>När ska de användas</title>

<p>En förloppsindikering behövs generellt när en åtgärd tar mer än tre sekunder och det är nödvändigt att indikera att en åtgärd faktiskt hanteras. Annars kommer användaren att lämnas i tvivel kring huruvida ett fel har inträffat.</p>

<p>Samtidigt är förloppsindikatorer en potentiell källa till distraktion, speciellt när de visas under kortare tidsperioder. Om en åtgärd tar mindre än tre sekunder är det bättre att undvika att visa en väntesnurra eftersom animerade element som visas under en väldigt kort tid kan störa användarupplevelsen.</p>

<p>Väntesnurror visar inte grafiskt hur stor del av uppgiften som färdigställts och används därför lämpligen för kortare åtgärder. Om uppgiften sannolikt tar mer än en minut kan därför en <link xref="progress-bars">förloppsindikator</link> vara ett bättre val.</p>

<p>Formen på väntesnurror påverkar också deras lämplighet för olika situationer. Eftersom de är effektiva även i små storlekar kan väntesnurror lätt bäddas in i mindre element i användargränssnittet så som listor eller rubrikrader. På samma sätt innebär deras form att de kan vara effektiva när de bäddas in i kvadratiska eller rektangulära behållare. Om det vertikala utrymmet är begränsat kan å andra sidan en förloppsindikator vara ett bättre val.</p>

</section>

<section id="general-guidelines">
<title>Allmänna riktlinjer</title>

<list>
<item><p>Om en åtgärd kan variera i hur lång tid den tar, använd en tidsutlösare för att endast visa väntesnurran efter att tre sekunder har gått. En förloppsindikator behövs inte för kortare tidslängder.</p></item>
<item><p>Placera väntesnurror nära intill eller inom elementen i användargränssnittet som de relaterar till. Om en knapp utlöser en åtgärd som tar lång tid kan väntesnurran till exempel placeras intill den knappen. När innehåll läses in bör väntesnurran placeras inom ytan där innehållet kommer att visas.</p></item>
<item><p>Generellt bör endast en väntesnurra visas åt gången. Undvik att visa ett större antal väntesnurror samtidigt — detta kommer ofta att vara visuellt överväldigande.</p></item>
<item><p>En etikett kan visas intill väntesnurran om det hjälper för att förtydliga uppgiften som väntesnurran relaterar till.</p></item>
<item><p>Om en väntesnurra visas under en längre tid kan en etikett indikera både åtgärdens identitet och dess förlopp. Detta kan ha formen av en procentsats, en indikation på mängden återstående tid eller förloppet genom underkomponenter för åtgärden (t.ex. antalet hämtade moduler eller exporterade sidor).</p></item>
</list>

</section>

<section id="api-reference">
<title>API-referens</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkSpinner.html">GtkSpinner</link></p></item>
</list>
</section>

</page>
