<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="sidebar-lists" xml:lang="sv">

  <info>
    <link type="guide" xref="patterns#secondary"/>
    <desc>Navigeringssidopanel som innehåller en lista</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2015, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

<title>Sidopanelslistor</title>

<media type="image" mime="image/svg" src="figures/patterns/sidebar-list.svg"/>

<p>En sidopanelslista möjliggör växling mellan olika vyer. Dessa vyer kan innehålla grupper av innehållsobjekt, enstaka innehållsobjekt, eller uppsättningar av komponenter. Sidopanelen delar fönstret i två, där innehåll visas i panelen på motsatt sida om sidopanelen.</p>

<p>Sidopanelslistor kan användas i primära fönster, antingen som ett permanent inventarium eller som ett element som visas vid efterfrågan. De kan också användas i dialogfönster.</p>

<p>Sidopanelslistor kan användas tillsammans med designmönstren <link xref="search">sökning</link> och <link xref="selection-mode">markeringsläge</link>.</p>

<section id="when-to-use">
<title>När ska de användas</title>

<p>Använd en sidopanelslista då det är nödvändigt att visa ett större antal vyer än som ryms i en vanlig <link xref="view-switchers">vyväxlare</link>.</p>

<p>Sidopanelslistor tillhandahåller också ett möjligt alternativ till navigering av bläddrarstil. De har ett antal fördelar här:</p>

<list>
<item><p>Då innehållsobjekt har en smal bredd, och inte kräver en fångande upplevelse. En sidopanel skulle vara olämplig för att bläddra bland videoklipp av denna anledning, men passar bra för kontakter.</p></item>
<item><p>Då innehållsobjekt är dynamiska. För meddelandeprogram, där nya innehållsobjekt dyker upp eller äldre uppdateras, tillhandahåller en sidopanelslista förmågan att visa ett objekt under tiden medan man samtidigt är medveten om uppdateringar till den övergripande meddelandelistan.</p></item>
<item><p>Då det är möjligt att filtrera en samling av innehåll, och det finns ett stort antal filter.</p></item>
</list>

<p>Tillfälliga sidopanelslistor kan också visas för specifika vyer i ditt program.</p>

</section>

<section id="guidelines">
<title>Riktlinjer</title>

<list>
<item><p>Sortera listan enligt vad som är mest användbart för ditt programs användare. Det är ofta bäst att placera de senast uppdaterade objekten högst upp i listan.</p></item>
<item><p>Rubrikradskomponenter som påverkar sidopanelslistan bör placeras i rubrikradens listpanelsdel. Komponenter för sökning och markering bör hittas ovanför listan.</p></item>
<item><p>Varje rad i listan kan innehålla flera textrader samt bilder. Var dock försiktig så att inte den viktigaste informationen går förlorad, och arbeta för att säkerställa ett prydligt och attraktivt utseende.</p></item>
</list>

</section>

<section id="api-reference">
<title>API-referens</title>
<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkListBox.html">GtkListBox</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkScrolledWindow.html">GtkScrolledWindow</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkStackSidebar.html">GtkStackSidebar</link></p></item>
</list>
</section>

</page>
