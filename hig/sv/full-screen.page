<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="fullscreen" xml:lang="sv">

  <info>
    <link type="guide" xref="patterns#secondary"/>
    <desc>Läge i vilket ett fönster täcker hela skärmen</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2015, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

<title>Helskärm</title>

<media type="image" mime="image/svg" src="figures/patterns/fullscreen.svg"/>

<p>Helskärm är ett läge i vilket ett fönster täcker hela skärmen. Då helskärm är aktiv döljs vanligen komponenter för att tillhandahålla en fångande innehållsvy.</p>

<section id="when-to-use">
<title>När ska de användas</title>

<p>Det är lämpligt att erbjuda helskärm då användare kan förväntas vilja ha en ostörd vy över innehåll, speciellt om de kan förväntas titta på innehållsobjekt under långa perioder. Det klassiska exemplet för helskärm är vid visning av videoklipp, då synliga komponenter skulle vara distraherande och resultera i mindre skärmutrymme för innehållet. Helskärm kan dock användas för andra typer av innehåll så som dokument eller bilder.</p>

<p>Helskärm används typiskt i visningssituationer. I vissa fall kan det dock vara användbart för redigeringsuppgifter. Helskärm är också typiskt för många spel (hur spel ska använda helskärm täcks inte på denna sida).</p>

</section>

<section id="guidelines">
<title>Riktlinjer</title>

<list>
<item><p>Om ett program använder olika vyer är det bara nödvändigt att aktivera helskärm för de vyer där det är användbart — främst de som visar innehållsobjekt.</p></item>
<item><p>Helskärm kan utlösas automatiskt då ett innehållsobjekt öppnas, eller kan erbjudas som ett läge som kan aktiveras av användaren.</p></item>
<item><p>Om helskärm erbjuds som ett läge som användare aktiverar, bedöm hur framträdande komponenten för att gå in i helskärmsläge ska vara enligt hur ofta den kommer att användas. Typiskt ska en helskärmsknapp placeras i <link xref="header-bars">rubrikraden</link> eller <link xref="header-bar-menus">rubrikradsmenyn</link>.</p></item>
<item><p>När helskärm är aktiverad:</p>
  <list>
    <item><p>Bör alla komponenter döljas, inkluderande fönstrets rubrikrad, <link xref="action-bars">åtgärdsrader</link> eller <link xref="overlaid-controls">överdragskomponenter</link>.</p></item>
    <item><p>Bör komponenter, inklusive rubrikraden, visas när pekaren förflyttas eller när skärmen trycks på.</p></item>
    <item><p>Bör fönsterstängningsknappen ersättas med en vyåterställningsknapp, som inaktiverar helskärmsläge.</p></item>
    <item><p>Bör alla komponenter som inte passar för helskärm döljas eller ersättas. Detta inkluderar alla navigationskomponenter som samtidigt skulle avsluta helskärmsläge, så som bakåtknappar.</p></item>
    <item><p>Gör en ansträngning för att inte låta komponenter eller användargränssnitt vara i vägen för innehåll. Till exempel kommer dialogfönster som överlappar innehåll att vara distraherande och reducera helskärmslägets uppslukande natur.</p></item>
  </list>
</item>
<item><p>Programmet Videoklipp är ett bra exempel på hur helskärm ska bete sig.</p></item>
</list>

</section>

</page>
