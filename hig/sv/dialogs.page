<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="dialogs" xml:lang="sv">

  <info>
    <link type="guide" xref="patterns#primary"/>
    <desc>Sekundära fönster som visas ovanpå primära, överordnade fönster</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2015, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

<title>Dialogfönster</title>

<p>Dialoger är sekundära fönster som visas ovanpå ett primärt överordnat fönster. De används för att presentera ytterligare information eller komponenter, inklusive inställningar och egenskaper, eller för att presentera meddelanden eller frågor.</p>

<p>GTK tillhandahåller ett antal standarddialoger som kan användas, så som de för utskrifter eller färgval.</p>

<p>Det finns tre grundläggande typer av dialoger.</p>

<section id="when-to-use">
<title>När ska de användas</title>

<p>Dialoger är ett vanligt designmönster och det finns etablerade konventioner för de olika typerna av dialoger som du eventuellt vill använda. Riktlinjerna för varje typ av dialog tillhandahåller vidare information om detta.</p>

<p>Även om dialoger kan vara ett effektivt sätt att visa ytterligare komponenter eller information kan de också vara en störningskälla för användaren. Av denna orsak bör du alltid ifrågasätta huruvida dialogen är nödvändig och arbeta på att undvika situationer där de är nödvändiga.</p>

<p>Det finns många sätt att undvika att använda dialoger:</p>

<list>
<item><p>Använd inbäddad komponering för nya meddelanden, poster eller kontakter.</p></item>
<item><p>Aviseringar i programmet är ett alternativ till meddelandedialoger.</p></item>
<item><p><link xref="popovers">Kontextfönster</link> kan vara ett sätt att visa ytterligare komponenter eller alternativ på ett mindre störande sätt.</p></item>
</list>

</section>

<section id="message-dialogs">
<title>Meddelandedialoger</title>

<media type="image" mime="image/svg" src="figures/patterns/message-dialog.svg"/>

<p>Meddelandedialoger är den enklaste typen av dialog. De presenterar ett meddelande eller en fråga tillsammans med 1-3 knappar som kan användas för att svara. De är alltid modala, vilket innebär att de förhindrar åtkomst till sitt överordnade fönster. Meddelandedialoger är ett lämpligt val när det är viktigt att användaren ser och svarar på ett meddelande.</p>

<section id="message-dialog-examples">
<title>Exempel</title>

<p>Bekräftelsedialoger använder en meddelandedialog för att kontrollera, eller bekräfta, att användaren vill utföra en åtgärd. De har två knappar: en för att bekräfta att åtgärden ska utföras och en för att avbryta åtgärden.</p>

<note style="tip"><p>Bekräftelsedialoger kommer ofta att bekräftas av misstag eller per automatik, och kommer inte alltid att förhindra misstag från att ske. Det är ofta bättre att tillhandahålla möjligheten att ångra sig.</p></note>

<p>Feldialoger presenterar ett felmeddelanden till användaren. De inkluderar ofta endast en knapp som låter användaren bekräfta och stänga dialogen.</p>

<note style="tip"><p>Feldialoger bör generellt vara det sista alternativet. Du bör designa ditt program så att fel inte inträffar och att det automatiskt återhämtar sig om något faktiskt går fel.</p></note>

</section>
</section>

<section id="action-dialogs">
<title>Åtgärdsdialoger</title>

<media type="image" mime="image/svg" src="figures/patterns/action-dialog.svg"/>

<p>Åtgärdsdialoger presenterar alternativ och information om en specifik åtgärd innan den utförs. De har en rubrik (som typiskt beskriver åtgärden) och två primära knappar: en som låter åtgärden utföras och en som avbryter den.</p>

<p>Ibland kan användaren behöva välja alternativ innan en åtgärd utförs. I dessa fall bör den bekräftande dialogknappen vara okänslig tills att de nödvändiga alternativen har valts.</p>

<section id="action-dialog-examples">
<title>Exempel</title>

<p>Många av standarddialogerna i GTK är åtgärdsdialoger. Utskriftsdialogen är ett bra exempel: den visas när användaren använder utskriftsåtgärden och presenterar information och alternativ för utskriftsåtgärden. De två rubrikradsknapparna låter utskriftsåtgärden antingen avbrytas eller utföras.</p>

</section>
</section>

<section id="presentation-dialogs">
<title>Presentationsdialoger</title>

<media type="image" mime="image/svg" src="figures/patterns/presentation-dialog.svg"/>

<p>Presentationsdialoger presenterar information eller komponenter. I likhet med åtgärdsdialoger har de en rubrikrad och ett ämne. Men istället för att föregå en åtgärd relaterar deras innehåll till ett program eller ett innehållsobjekt.</p>

<section id="presentation-dialog-examples">
<title>Exempel</title>

<p>Inställningar och egenskaper är båda exempel på presentationsdialoger och båda presenterar information och inställningar som relaterar till en specifik post (antingen ett program eller ett innehållsobjekt). Egenskapsdialoger är ett bra exempel på hur dialoger kan användas för att avslöja ytterligare information som inte alltid behövs i programmets huvudfönster.</p>

<note style="tip"><p>Motstå frestelsen att tillhandahålla ett inställningsfönster för ditt program. Ifrågasätt alltid huruvida ytterligare inställningar verkligen är nödvändiga. De flesta användare kommer inte att bry sig om att undersöka inställningarna du tillhandahåller och konfigurationsinställningar kommer att bidra till komplexiteten i ditt program. Gör en ansträngning för att säkerställa att din programdesign fungerar för alla utan att behöva ändra dess inställningar.</p></note>

</section>

<section id="instant-and-explicit-apply">
<title>Omedelbart och uttryckligt verkställande</title>

<p>Presentationsdialoger får antingen effekt omedelbart eller kräver explicit verkställande. I dialoger med omedelbar effekt kommer ändring av inställningar eller värden att omedelbart uppdateras. Detta står i kontrast till dialoger som kräver explicit verkställande, vilka har en knapp för att verkställa ändringarna.</p>

<p>Dialoger med omedelbart effekt bör användas där så är möjligt. Dialoger med omedelbar effekt har en stängningsknapp i deras rubrikrad, i likhet med ett <link xref="primary-windows">primärfönster</link>.</p>

<p>Dialoger som kräver explicit verkställande är endast nödvändiga om ändringar i dialogen måste tillämpas samtidigt för att få det önskade beteende. Dialoger med explicit verkställande har en <gui>Klar</gui>- och <gui>Avbryt</gui>-knapp. (<gui>Avbryt</gui> återställer alla värden i dialogen till tillståndet de hade innan den öppnades och <gui>Klar</gui> tillämpar alla ändringar och stänger fönstret).</p>

</section>
</section>

<section id="primary-buttons">
<title>Primärknappar</title>

<p>Meddelande- och åtgärdsdialoger inkluderar primärknappar som påverkar hela fönstret. Ordningen på dessa knappar, samt de etiketter de använder, spelar en nyckelroll i dialogen.</p>

<section id="order">
<title>Ordning</title>

<p>När en dialog inkluderar en bekräftelse- och en avbrytknapp, försäkra dig alltid om att avbryt-knappen visas först, innan bekräftelseknappen. I vänster-till-höger-lokaler är detta till vänster.</p>

<p>Denna knappordning säkerställer att användaren blir uppmärksammad på, och påmind, om möjligheten om att avbryta innan bekräftelseknappen anträffas.</p>

</section>

<section id="labels">
<title>Etiketter</title>

<p>Ge den primära bekräftelseknappen en etikett med ett specifikt, imperativt verb, till exempel <gui>Spara</gui>, <gui>Skriv ut</gui>, <gui>Ta bort</gui>. Detta är tydligare än en generell etikett så som <gui>OK</gui> eller <gui>Klar</gui>.</p>

<p>Feldialoger inkluderar typiskt en enda knapp som avfärdar dialogen. I detta fallet behöver en specifik åtgärd inte refereras till, utan detta är ett bra tillfälle att använda humor. <gui>Ursäkten accepterad</gui> eller <gui>Uppfattat</gui> är båda exempel på bra etiketter.</p>

</section>

<section id="default-action-and-escape">
<title>Standardåtgärd och avbrott</title>

<p>Tilldela alltid Retur till att aktivera den primära bekräftelseknappen i en dialog (till exempel <gui>Skriv ut</gui> i en utskriftsdialog). Detta kallas för standardåtgärden och indikeras med en annorlunda visuell stil. Gör inte en knapp till standardalternativ om dess åtgärd är oåterkallelig, destruktiv eller på annat sätt besvärlig för användaren. Om det inte finns någon lämplig knapp att beteckna som standardknappen, ställ inte in någon.</p>

<p>Du bör också säkerställa att Escape-tangenten aktiverar avbryt- eller stängknappen, om endera av dessa existerar. Meddelandedialoger med en enda knapp kan binda både Escape och Retur till samma knapp.</p>

<p>Att binda Retur och Escape på detta sätt tillhandahåller ett förutsägbart och behändigt sätt för att fortsätta vidare efter en dialog, eller att gå tillbaka.</p>

</section>

</section>

<section id="general-guidelines">
<title>Allmänna riktlinjer</title>

<list>
<item><p>Dialogfönster bör aldrig visas oväntat och de för endast användas för att visa omedelbar respons till en avsiktlig användaråtgärd.</p></item>
<item><p>Dialoger bör alltid ha ett överordnat fönster.</p></item>
<item><p>Följ <link xref="visual-layout">layoutriktlinjerna</link> när du designar innehållet i fönster.</p></item>
<item><p>Använd <link xref="view-switchers">vyväxlare</link> eller <link xref="tabs">flikar</link> för att dela upp komponenter och information.</p></item>
<item><p>Undvik att stapla dialogfönster ovanpå varandra. Endast ett dialogfönster bör visas åt gången.</p></item>
<item><p>När en dialog öppnas, tillhandahåll inledande tangentbordsfokus på den komponent som du förväntar dig att användaren ska interagera med först. Detta fokus är speciellt viktigt för användare som måste använda ett tangentbord för att navigera i ditt program.</p></item>
</list>

</section>

<section id="api-reference">
<title>API-referens</title>
<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkAboutDialog.html">GtkAboutDialog</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkDialog.html">GtkDialog</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkMessageDialog.html">GtkMessageDialog</link></p></item>
</list>
</section>

</page>
