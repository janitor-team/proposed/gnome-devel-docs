<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="writing-style" xml:lang="sv">

  <info>
    <revision pkgversion="3.15" date="2015-01-14" status="review"/>

    <credit type="author">
      <name>Allan Day</name>
      <email its:translate="no">aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Skriva text för ditt användargränssnitt, inklusive versaliseringsregler.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2015, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

<title>Skrivregler (för engelska)</title>

<p>Text spelar en viktig roll i användargränssnitt. Ta dig tid att säkerställa att text du använder är tydligt uttryckt och enkel att förstå.</p>

<section id="guidelines">
<title>Riktlinjer</title>

<p>Ditt huvudmål bör vara att säkerställa att texten är enkelt att förstå och snabbläst.</p>

<list>
<item><p>Håll text kort och rakt på sak. Detta förbättrar förståelsehastigheten för användaren. Det reducerar också textens expansion när den översätts (kom ihåg att översatt engelsk text kan expanderas upp till 30% i vissa språk).</p></item>
<item><p>Förkorta inte din text förbi punkten där den börjar förlora sin mening. En treords-etikett som ger klar och tydlig information är bättre än en enords-etikett som är otydlig eller vag. Försök hitta det minsta antalet ord som på ett tillfredsställande sätt förmedlar etikettens mening.</p></item>
<item><p>Använd ord, fraser och koncept som är bekanta för personerna som ska använda ditt program, snarare än termer från det underliggande systemet. Detta kan innebära att du använder termer som associeras med uppgifter ditt program har stöd för. I medicin, till exempel, kallas pappersmappen som patientinformationen ligger i för ”chart”. Därför skulle ett medicinskt program kunna referera till en patientpost som en ”chart” snarare än en ”patient database record”.</p></item>
<item><p>Text bör använda en neutral ton och tala från produktens synvinkel. Pronomen som ”you” eller ”my” bör därför undvikas där det är möjligt. Om det är oundvikligt är dock ”your” att föredra framför ”my”.</p></item>
<item><p>Använd standardtermer från GNOME när referenser till delar av användargränssnittet används, så som ”pointer” och ”window”. Riktlinjerna för mänskliga gränssnitt kan användas som en referens i detta fallet.</p></item>
<item><p>Undvik repetition där det är möjligt.</p></item>
<item><p>Meningar bör inte konstrueras utifrån text från flera komponenter utan varje etikett bör behandlas som om den vore fristående. Meningar som löper från en komponent till en annan kommer ofta inte att ge någon mening när de översätts till andra språk.</p></item>
<item><p>Latinska förkortningar så som ”i.e.” eller ”e.g.” bör undvikas då de inte alltid enkelt kan översättas och dessutom kan vara obegripliga för skärmläsare. Använd istället fullständiga ord så som ”for example”.</p></item>
</list>

</section>

<section id="capitalization">
<title>Versalisering</title>

<p>Det finns två sorters versalisering som används i GNOME:s användargränssnitt: rubrikversalisering och meningsversalisering.</p>

<section id="header-capitalization">
<title>Rubrikversalisering</title>

<p>Rubrikversalisering bör endast användas för rubriker, inklusive rubrikradstitlar samt sid-, flik- och menytitlar. Den bör också användas för korta komponentetiketter som normalt inte bildar fullständiga meningar, så som knappetiketter, brytaretiketter och menyobjekt.</p>

<p>Versalisera den första bokstaven i:</p>

<list>
<item><p>Alla ord med fyra eller fler bokstäver.</p></item>
<item><p>Verb av godtycklig längd så som ”Be”, ”Are”, ”Is”, ”See” och ”Add”.</p></item>
<item><p>Första och sista ordet.</p></item>
<item><p>Avstavade ord; till exempel ”Self-Test” eller ”Post-Install”.</p></item>
</list>

<p>Till exempel: ”Create a Document”, ”Find and Replace”, ”Document Cannot Be Found”.</p>

</section>

<section id="sentence-capitalisation">
<title>Meningsversalisering</title>

<p>Meningsversalisering bör användas för etiketter som bildar meningar som löper över annan text, inklusive etiketter för kryssrutor, radioknappar, skjutreglage, textinmatningsfält, fältetiketter och kombinationsruteetiketter. Den bör också användas för förklarande text eller huvudtext så som i dialoger eller aviseringar.</p>

<p>Versalisera den första bokstaven i:</p>

<list>
<item><p>Det första ordet.</p></item>
<item><p>Ord som normalt versaliseras i meningar, så som egennamn.</p></item>
</list>

<p>Till exempel: ”The document cannot be found in this location.” ”Finding results for London.”</p>

</section>
</section>

<section id="ellipses">
<title>Ellipser (…)</title>

  <p>Använd en ellips (…) i slutet på en etikett om vidare inmatning eller bekräftelse krävs av användaren innan åtgärden kan utföras. Till exempel <gui>Save As…</gui>, <gui>Find…</gui> och <gui>Delete…</gui>.</p>

  <p>Lägg inte till en ellips i etiketter så som <gui>Properties</gui> eller <gui>Preferences</gui>. Även om dessa kommandon öppnar fönster som kan innehålla vidare funktionalitet så anger etiketten inte en åtgärd och behöver därför inte kommunicera att vidare inmatning eller bekräftelse krävs.</p>

</section>

</page>
