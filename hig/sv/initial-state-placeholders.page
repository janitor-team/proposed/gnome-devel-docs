<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="initial-state-placeholder" xml:lang="sv">

  <info>
    <link type="guide" xref="patterns#secondary"/>
    <desc>Bild och text som visas när ett program initialt är tomt</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2015, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

<title>Platshållare för initialt tillstånd</title>

<media type="image" mime="image/svg" src="figures/patterns/initial-state-placeholder.svg"/>

<p>En platshållare för initialt tillstånd är en bild och text som fyller utrymmet i ett program som aldrig har haft något innehåll.</p>

<section id="when-to-use">
<title>När ska de användas</title>

<p>I vissa fall kan ett program vara tomt tills en användare lägger till något innehåll i det. I dessa fall kan ett initialt tillstånd användas för att tillhandahålla en innehållsrik och inbjudande användarupplevelse. Detta hjälper till att undvika att den första användningen av programmet känns nedstämd och ovälkomnande.</p>

<p>En platshållare för initialt tillstånd bör endast användas när ett program oundvikligt kommer att vara tomt. I många fall är det bättre att i förväg fylla programmet med innehåll.</p>

</section>

<section id="guidelines">
<title>Riktlinjer</title>

<list>
<item><p>Följ standardlayouten för storlek och placering av bilder och etiketter så att ditt program är konsekvent med andra GNOME 3-program.</p></item>
<item><p>Bilderna som används bör vara innehållsrika och färgglada.</p></item>
<item><p>Texten som följer med bilden bör vara positiv och upplyftande. Detta är ett tillfälle där du kan sälja in ditt program och etablera en positiv identitet för det. Det kan också vara en möjlighet för att skapa en relation med användaren genom att tilltala dem direkt.</p></item>
<item><p>Om det finns andra komponenter som tillåter att objekt läggs till så kan det vara lämpligt att markera dem med en <link xref="buttons#suggested-and-destructive">föreslagen stil</link> då listan/rutnätet är tomt.</p></item>
<item><p>När ett program initialt är tomt tjänar vissa komponenter inget syfte (så som de för att bläddra i innehåll, ändra vy eller söka). Att göra dessa komponenter okänsliga kommer att hjälpa till att undvika att användaren blir besviken, eller försöker använda funktioner som inte kommer att fungera.</p></item>
<item><p>Ett initialt tillstånd bör bestå tills innehåll har lagts till i programmet, efter vilket det inte bör ses igen. Om programmet sedan åter blir tomt kan ett <link xref="empty-placeholder">tomt tillstånd användas</link>.</p></item>
</list>

</section>

</page>
