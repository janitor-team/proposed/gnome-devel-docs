<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="toolbars" xml:lang="sv">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/toolbar.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2015, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

<title>Verktygsfält</title>
<p>Ett verktygsfält är en remsa med komponenter som möjliggör bekväm åtkomst till ofta använda funktioner. De flesta verktygsfält innehåller bara grafiska knappar, men i mer komplexa program kan även andra komponenter så som rullgardingslistor vara användbara.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/toolbar.svg"/>

<section id="when-to-use">
<title>När ska de användas</title>

<p>Använd ett verktygsfält för att tillhandahålla åtkomst till vanliga åtgärder, verktyg eller alternativ i primära fönster. <link xref="header-bars">Rubrikrader</link> fyller också denna funktion, och ett verktygsfält krävs som regel inte om du använder rubrikrader.</p>

<p>Även om verktygsfält är ett vanligt designval så finns det fall då det inte är det mest effektiva. Gränssnitt som fokuserar på direkt manipulering, eller vilka använder sig mycket av stegvis avslöjande kan utgöra ett bättre alternativ. Vart och ett av dessa tillvägagångssätt kräver mer tid och ansträngning i designfasen, och bör endast eftersträvas av de som känner sig säkra i att söka mer originella designlösningar.</p>

</section>

<section id="general-guidelines">
<title>Allmänna riktlinjer</title>

<list>
<item><p>Inkludera endast komponenter för de viktigaste funktionerna. Att ha för många verktygsfältskomponenter minskar deras effektivitet genom att göra dem svårare att hitta, och för många rader av verktygsfält minskar det tillgängliga skärmutrymmet för resten av programmet.</p></item>
<item><p>Använd konventioner för verktygsfält för att öka igenkänningsfaktorn. Exempelvis kommer huvudverktygsfältet i ett kontorsprogram nästan alltid att ha nytt, öppna och spara som sina tre första verktygsfältsknappar. På liknande sätt bör de första knapparna i ett webbläsarprogram vara bakåt och framåt.</p></item>
<item><p>Placera endast de mest använda programfunktionerna i dina verktygsfält. Lägg inte bara till knappar för varje menyobjekt.</p></item>
<item><p>Om du använder en <link xref="menu-bars">menyrad</link>, säkerställ att den innehåller alla funktioner som finns i ditt verktygsfält, antingen direkt (d.v.s. ett ekvivalent menyobjekt) eller indirekt (t.ex. i dialogrutan <guiseq><gui>Alternativ</gui><gui>Inställningar</gui></guiseq>).</p></item>
<item><p>Verktygsfält bör inte inkludera knappar för <gui>Hjälp</gui>, <gui>Stäng</gui> eller <gui>Avsluta</gui>, då dessa sällan används och utrymmet kan hellre användas för mer användbara komponenter. Tillhandahåll på samma sätt endast knappar för <gui>Ångra</gui>, <gui>Gör om</gui> och standardfunktionerna för urklipp om det finns plats på verktygsfältet för att göra detta utan att ge upp mer användbara, programspecifika komponenter.</p></item>
<item><p><link xref="buttons">Knappar</link> i verktygsfält bör ha en relief, och ikonknappar bör använda <link xref="icons-and-artwork#color-vs-symbolic">symboliska ikoner</link>.</p></item>
</list>

</section>

<section id="api-reference">
<title>API-referens</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkToolbar.html">GtkToolbar</link></p></item>
</list>

</section>

</page>
