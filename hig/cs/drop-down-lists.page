<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="drop-down-lists" xml:lang="cs">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/drop-down-list.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Rozbalovací seznamy</title>

<p>Rozbalovací seznam je prvek uživatelského rozhraní, který umožňuje uživateli vybírat ze seznamu navzájem se vylučujících voleb. Zobrazuje se jako tlačítko, které po kliknutí odhalí seznam.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/drop-down-list.svg"/>

<p>Často je před rozbalovacími seznamy dávána přednost skupinovým přepínačům nebo seznamům, protože zobrazují dostupné volby ihned viditelně naráz, bez potřeby nějakých dalších úkonů od uživatele. I v takových situacích může být rozbalovací seznam lepší volbou, když:</p>

<list>
<item><p>Je počet voleb rozsáhlý.</p></item>
<item><p>Je zde málo dostupného místa.</p></item>
<item><p>Seznam voleb se může s časem měnit.</p></item>
<item><p>Obsah skryté části nabídky je jasný z popisku a jedné právě vybrané položky. Například, když máte nabídku voleb s popiskem „Month:“ („Měsíc:“) s vybranou položkou „January“ („leden“), může uživatel rozumně předpokládat, že nabídka obsahuje dvanáct měsíců roku, aniž by musel nabídku rozbalovat.</p></item>
</list>

<section id="general-guidelines">
<title>Obecné zásady</title>

<list>
<item><p>Přestože GTK poskytuje přímo widget rozbalovacího seznamu, doporučuje se jej realizovat pomocí kombinace tlačítka a rozbalovacího dialogu. Umožňuje to pohodlnější posuv v dlouhých seznamech a integrovat do seznamu vyhledávání. Použití tohoto přístupu zároveň zajistí, že aktuálně vybraná položka bude vždy zobrazená.</p></item>
<item><p>Když je počet položek velmi rozsáhlý, poskytněte funkci hledání, která seznam vyfiltruje.</p></item>
<item><p>Opatřete rozbalovací seznam popiskem umístěným nad ním nebo vlevo od něj a držte se při tom pravidel pro <link xref="writing-style#capitalization">psaní velkých písmen ve větě</link>. V popisku dejte k dispozici horkou klávesu, která uživateli umožní rozbalovací seznam přímo zaměřit.</p></item>
<item><p>Pro položky v rozbalovacím seznamu použijte pravidla pro <link xref="writing-style#capitalization">psaní velkých písmen v nadpisech</link>, například <gui>Switched movement</gui> (<gui>Přesunout prohozením</gui>).</p></item>
</list>

</section>

<section id="custom-values">
<title>Vlastní hodnoty</title>

<media type="image" mime="image/svg" src="figures/ui-elements/drop-down-list-custom-values.svg"/>

<p>Rozbalovací seznam může dovolit přidávání vlastních hodnot k těm, co jsou přednastavené, nebo se můžou zadávat a používat jen vlastní hodnoty.</p>

<list>
<item><p>Zajistěte, aby se vlastní hodnoty řadily způsobem, který je pro uživatele nejužitečnější. Běžné je řazení podle abecedy nebo posledního použití.</p></item>
<item><p>Pokud rozbalovací seznam zahrnuje jak přednastavené, tak vlastní hodnoty, oddělte je v seznamu do zvláštních skupin.</p></item>
<item><p>Umožněte odstranit vlastní hodnoty ze seznamu.</p></item>
<item><p>Ověřujte platnost vložených vlastních hodnot, abyste předešli případným chybám.</p></item>
<item><p>Když rozbalovací seznam přijímá pouze vlastní hodnoty a zatím žádná hodnota nebyla zadána, zobrazte při otevření seznamu vstupní pole pro vlastní hodnotu, místo abyste zobrazovali prázdný seznam.</p></item>
</list>

</section>

</page>
