<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="selection-mode" xml:lang="cs">

  <info>
    <link type="guide" xref="patterns#primary"/>
    <desc>Návrhový vzor pro výběr položek obsahu</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Režim výběru</title>

<media type="image" mime="image/svg" src="figures/patterns/selection-mode.svg"/>

<p>Režim výběru je návrhový vzor, který umožňuje provádět činnosti na položkách obsahu. Obvykle se používá ve spojení se seznamy a mřížkami.</p>

<p>Když je aktivní režim výběru, umožňují zaškrtávací políčka vybrat položky a ve spodní části zobrazení je akční lišta. Ta obsahuje různé činnosti, které lze provést na vybraných položkách obsahu.</p>

<section id="when-to-use">
<title>Kdy použít</title>

<p>Režim výběru je vhodný když:</p>

<list>
<item><p>Jsou běžně prováděny činnosti naráz na více položkách obsahu.</p></item>
<item><p>Je dostupných více činností k provedení na položkách obsahu.</p></item>
<item><p>Pro uživatele je přínosné mít možnost provést činnosti na položkách obsahu, aniž by je musel otevírat.</p></item>
</list>

<p>Když je pro uživatele obvyklé provádět činnosti na jednotlivých položkách obsahu, můžete zvážit jiný návrhový vzor, jako třeba kontextovou nabídku. Obdobně, když existuje jen jedna činnosti, kterou lze na položkách obsahu provádět, můžete použít nějakou variantu k režimu výběru (například překrývající tlačítka mohou umožnit provést činnosti na položkách přímo, aniž by se musely dopředu vybírat).</p>

</section>

<section id="activating-selection-mode">
<title>Aktivace režimu výběru</title>

<p>Hlavní způsob, jak aktivovat režim výběru, je přes tlačítko režimu výběru, které je umístěné na hlavičkové liště a které se pozná podle ikony v podobě zaškrtnutí. Režim výběru se může aktivovat také výběrem pomocí tažení přes více položek při zmáčknutém hlavním tlačítku, nebo klikáním/mačkáním položek obsahu za současného držení <key>Ctrl</key> nebo <key>Shift</key>.</p>

</section>

<section id="action-bar">
<title>Akční lišta</title>

<list>
<item><p>Ovládací prvky na nástrojové liště by neměly být přístupné, když není vybrána žádná položka. Někdy mohou být činnosti použitelné jen pro více položek obsahu naráz. V takovém případě by příslušné ovládací prvky měly být přístupné, jen když je vybráno více položek.</p></item>
<item><p>Ovládací prvky v akční liště mohou mít ikony nebo textové popisky.</p></item>
<item><p>Skupiny činností lze odlišit jejich umístěním na opačné konce nástrojové lišty. Destruktivní činnosti, jako je mazání, by měly být umístěny mimo ostatní ovládací prvky.</p></item>
</list>

</section>
</page>
