<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="overlaid-controls" xml:lang="cs">

  <info>
    <link type="guide" xref="patterns#secondary"/>
    <desc>Plovoucí ovládací prvky, často používané pro práci s obrázky a videi</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Ovládací prvky překrývající obsah</title>

<media type="image" mime="image/svg" src="figures/patterns/overlaid-controls.svg"/>

<p>Dočasné ovládací prvky, kterou „plavou“ na obsahem jsou běžným návrhovým vzorem pro aplikace, které zobrazují obrázky nebo videa.</p>

<section id="when-to-use">
<title>Kdy použít</title>

<p>Díky tomu, že se překrývající ovládací prvky skrývají, když nejsou používány, pomáhají poskytnou přehledné zobrazení. Jsou vhodné, když je žádoucí prezentovat čisté a ničím nerušené zobrazení položky obsahu. Což se konkrétně týká (ale nejen) obrázků a videí.</p>

<p>Překrývající ovládací prvky mohou být nevhodné, když zakrývají důležitou část obsahu pod sebou. Například u obrázků editační ovládací prvky mohou překážet při sledování projevu jejich funkce. V takovém případě by ovládací prvky neměly obsah překrývat.</p>

</section>

<section id="guidelines">
<title>Zásady</title>

<list>
<item><p>Držte se zaběhnutých zvyklostí u typů ovládacích prvků, jako jsou tlačítka pro procházení vlevo/vpravo u prohlížeče obrázků a ovládací prvky přehrávání v dolní části pro video.</p></item>
<item><p>Ovládací prvky by se měly zobrazit, když ukazatel najede nad obsah nebo při poklepání na něj (na dotykových zařízeních).</p></item>
<item><p>Překrývající ovládací prvky mohou být ukotvené k hraně obsahu/okna nebo mohou být volně plovoucí. Za překrývající ovládací prvek lze svým způsobem považovat i <link xref="action-bars">akční lištu</link>.</p></item>
<item><p>Pro překrývající ovládací prvky použijte standardní motiv „OSD“.</p></item>
</list>

</section>

</page>
