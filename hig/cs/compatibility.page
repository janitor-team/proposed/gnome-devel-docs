<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="compatibility" xml:lang="cs">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <desc>Použití této příručky pro multiplatformní aplikace nebo aplikace ve stylu GNOME 2.</desc>
  </info>

<title>Kompatibilita</title>

<p>Pokyny k rozhraní pro člověka se zaměřuje hlavně na nové aplikace GTK a GNOME, s důrazem na integraci do GNOME 3. Jejím cílem je ale i to, aby byla užitečná pro multiplatformní aplikace a starší aplikace, které původně byly navržené a implementované podle HIG pro GNOME 2.</p>

<section id="cross-platform-compatibility">
<title>Kompatibilita napříč platformami</title>

<p>Obecně vzato, návrhové vzory, které najdete v této příručce, se dají uplatnit na kterékoliv platformě pro stolní počítače. Většina konvencí, jako jsou nabídky, nabídková tlačítka, zobrazení a přepínače zobrazení a rozbalovací dialogy jsou běžné a obecně pochopitelné.</p>

<p>Hlavním příkladem návrhového vzoru, který se liší napříč různými platformami, je <link xref="menu-bars">nabídková lišta</link>, které má na různých platformách různou formu. Více rad k úvahám nad návrhem pro více platforem najdete na stránce Principy návrhu v této části.</p>

</section>

<section id="gnome-2-compatibility">
<title>Kompatibilita s GNOME 2</title>

<p>Tato příručka je evolučním počinem z příručky HIG pro GNOME 2. Aplikace, které se řídí novou verzí příručky budou využívat efektivněji místo, více se zaměří na uživatelskou přívětivost a použijí moderní zpodobnění klíčové funkcionality, jako je vyhledávání, integrace s on-line prostředky nebo oznámení.</p>

<p>Nasazení některých návrhových vzorů z této příručky může znamenat rozsáhlé změny v aplikacích původně navržených pro GNOME 2, zvláště pokud jsou tyto aplikace složité. Velkou změnu znamená hlavně záměna lišty záhlaví a <link xref="menu-bars">nabídkové lišty</link> za <link xref="header-bars">hlavičkovou lištu</link>.</p>

<p>Přesto však můžete hodně rad v této příručce skloubit s aplikacemi ve stylu GNOME 2 s jen minimálním narušení původního návrhu a přitom to povede k lepší uživatelské přivětivosti. Patří k nim:</p>

<list>
<item><p>Využití nových prvků uživatelského rozhraní, jako jsou rozbalovací dialogy.</p></item>
<item><p>Poučení o nových schopnostech GTK, jako jsou animace.</p></item>
<item><p>Modernizované pokyny pro věci, jako je vizuální rozvržení, typografie nebo použití ikon.</p></item>
</list>

<p>Začlenění těchto prvků z příručky může vaši aplikaci vylepšit, aniž by to vyžadovalo její kompletní předělání a budou z toho mít prospěch všechny aplikace v GTK a GNOME.</p>

<p>Tato příručka byla navržena tak, aby vám pomohla při výběru nejlepšího návrhu pro vaši aplikaci a nenabízí proto jen jednu šablonu, kterou byste měli použít ve všech aplikacích. Takže i když jako taková upřednostňuje <link xref="header-bars">hlavičkové lišty</link> před <link xref="menu-bars">nabídkovými lištami</link>, poskytuje pokyny pro použití obou přístupů.</p>

</section>

</page>
