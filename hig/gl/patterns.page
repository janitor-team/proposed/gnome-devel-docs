<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="patterns" xml:lang="gl">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Dieguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2019.</mal:years>
    </mal:credit>
  </info>

<title>Design patterns</title>

<p>Patterns are the primary elements that make up an application design. Some patterns, like application menus or header bars, are highly recommended. Others are optional, and deciding on which patterns you want to use forms a core part of the design process. To help with this, each pattern page details the appropriate uses of each pattern, and suggests alternatives that might be applicable.</p>

<section id="anatomy">
<title>Anatomy of a GNOME 3 application</title>
<p>A visual reference for some of the most common GNOME 3 design patterns.</p>
<media type="image" mime="image/svg" src="figures/patterns/patterns.svg"/>
</section>

<section id="primary">
<title>Core design patterns</title>
<links type="topic" style="2column"/>

</section>

<section id="secondary">
<title>Supplementary design patterns</title>
<links type="topic" style="2column"/>
</section>

</page>
