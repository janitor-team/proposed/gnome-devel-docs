<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="menus" xml:lang="gl">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/menu.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Dieguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2019.</mal:years>
    </mal:credit>
  </info>

<title>Menus</title>

<p>A menu is a list of actions and/or options which is revealed by pressing a heading or button. In the case of context menus, the menu is opened through a secondary action (such as secondary click with a mouse, or long press with a touch screen) on an item of content.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/menu.svg"/>

<section id="when-to-use">
<title>Cando usala</title>

<p>Menus can appear as a part of a <link xref="menu-bars">menu bar</link>, as context menus (see <link xref="selection-mode">selection mode</link>), or as part of a <link xref="button-menus">button menu</link>. Refer to the relevant pages for advice on when to use these elements.</p>

</section>

<section id="size-and-structure">
<title>Size and structure</title>

<p>Menus should contain between three and 12 top-level items. If a menu contains more than 12 items, evaluate whether all the items are necessary and belong in the menu. If you are unable to reduce the size, submenus can be used. However, they should be avoided if at all possible, as they are physically difficult to use.</p>

<p>Submenus should contain between three and six items, and should never contain other submenus.</p>

<p>Organize similar menu items into groups using dividers — this will make them easier to understand and quicker to use. When creating groups:</p>

<list>
<item><p>Order groups and group items logically, either by importance, task order, or expected frequency of use. Items at the top and bottom of the menu are more noticeable and easily targeted, so reserve these locations for particularly important or interesting functionality.</p></item>
<item><p>Place single-item groups at the top or bottom of the menu, or group them together with other single items.</p></item>
<item><p>Do not mix different types of menu item within each group — actions, check box and radio button items should be kept separate.</p></item>
</list>

</section>

<section id="general-guidelines">
<title>Liñas de guía xerais</title>

<list>
<item><p>Provide an <link xref="keyboard-input#access-keys">access key</link> for every menu item. You may use the same access key on different menus in your application, but avoid duplicating access keys on the same menu. Note that unlike other controls, once a menu is displayed, its access keys may be used by just typing the letter; it is not necessary to press the Alt key at the same time.</p></item>
<item><p>Label menu items with verbs for commands and adjectives for settings, using <link xref="writing-style#capitalization">header capitalization</link>.</p></item>
<item><p>Use <link xref="writing-style#ellipses">ellipses</link> when a menu item requires further input from the user to complete an action.</p></item>
<item><p>Two linked actions can be combined into a single menu item, by changing the label when the item is selected. For example, a <gui>Play</gui> item may change to <gui>Pause</gui>. However, only use this type of item when actions are logical opposites which are obvious to users. Likewise, do not use this technique for settings — use check boxes or radio buttons instead.</p></item>
</list>

</section>

<section id="api-reference">
<title>Referencia da API</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkMenu.html">GtkMenu</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkMenuBar.html">GtkMenuBar</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkMenuButton.html">GtkMenuButton</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkMenuItem.html">GtkMenuItem</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkRadioMenuItem.html">GtkRadioMenuItem</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkCheckMenuItem.html">GtkCheckMenuItem</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkSeparatorMenuItem.html">GtkSeparatorMenuItem</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkPopoverMenu.html">GtkPopoverMenu</link></p></item>
</list>
</section>

</page>
