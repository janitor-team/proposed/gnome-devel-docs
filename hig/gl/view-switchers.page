<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="view-switchers" xml:lang="gl">

  <info>
    <link type="guide" xref="patterns#secondary"/>
    <desc>Toggle buttons that change the view, like tabs</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Dieguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2019.</mal:years>
    </mal:credit>
  </info>

<title>View switchers</title>

<media type="image" mime="image/svg" src="figures/patterns/view-switcher.svg"/>

<p>A view switcher is a control that allows switching between a number of predefined views. It appears as a set of toggle buttons that are placed in the center of a header bar.</p>

<section id="when-to-use">
<title>Cando usala</title>

<p>There are two primary cases when a view switcher is appropriate:</p>

<list>
<item><p>When presenting content, and it is useful to be able to view different sets or sub-sets of content. For example, a music application could show different views for artists, albums and playlists.</p></item>
<item><p>If your application provides discrete groups of functionality which are typically used independently.</p></item>
</list>

<p>As a rule of thumb, a view switcher should contain between three and five views. If you have more views, a <link xref="sidebar-lists">sidebar list</link> might be a more appropriate view switching control.</p>

</section>

<section id="additional-guidelines">
<title>Additional guidelines</title>

<list>
<item><p>Each view should have a short and clear title.</p></item>
<item><p>Buttons in the view switcher widget can indicate when there is activity in a view.</p></item>
</list>

</section>

<section id="api-reference">
<title>Referencia da API</title>
<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkStack.html">GtkStack</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkStackSwitcher.html">GtkStackSwitcher</link></p></item>
</list>
</section>

</page>
