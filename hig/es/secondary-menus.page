<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="secondary-menus" xml:lang="es">

  <info>
    <link type="guide" xref="patterns#primary"/>
    <desc>El menú secundario de la aplicación, denotado por por tres puntos en horizonal</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alex Puchades</mal:name>
      <mal:email>alex94puchades@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamín Valero Espinosa</mal:name>
      <mal:email>benjavalero@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rosa Elena Pérez</mal:name>
      <mal:email>rozydicemiau@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oscar Rafael Arachi</mal:name>
      <mal:email>arachi910@gmail.com</mal:email>
      <mal:years>2014 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miguel Ajuech</mal:name>
      <mal:email>miguel.ann.28@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adrián Pérez Domínguez</mal:name>
      <mal:email>adrian@aztli.org</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Celina Osorio Ochoa</mal:name>
      <mal:email>chelinoska@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Dario Amigon Espinoza</mal:name>
      <mal:email>daramigon@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Yenisei Ramírez Pérez</mal:name>
      <mal:email>yeniseirape@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Roberto Ramíres Mendoza</mal:name>
      <mal:email>aczella@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bernardo Sánchez Romero</mal:name>
      <mal:email>b.sanchez.rom@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Emmanuel Márquez Juárez</mal:name>
      <mal:email>resident3333@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Romero Serrano</mal:name>
      <mal:email>lemi136@hotmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Omar Garcia Guzman</mal:name>
      <mal:email>garciag.omar91@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Edilberto Huerta Niño</mal:name>
      <mal:email>edilberto.huerta0290@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Menús secundarios</title>

<media type="image" mime="image/svg" src="figures/patterns/secondary-menu.svg"/>

<p>Los menús secundarios se encuentran en la barra de encabezado e incluyen elementos de menú para la vista actual o el elemento de contenido. Esto los diferencia de <link xref="primary-menus">menús primarios</link>, que incluyen elementos de menú que se relacionan con una aplicación completa (como <gui>Preferencias</gui> y <gui>Acerca de «Nombre»</gui>).</p>

<section id="when-to-use">
<title>Cuándo usarlas</title>

<p>Los menús secundarios se pueden usar para alojar controles relacionados con un elemento de contenido particular que se muestra en la ventana de la aplicación (como un documento, contacto, conversación o foto). Por lo general, se usan en combinación con la navegación en la ventana o <link xref="sidebar-lists">listas en la barra lateral</link>, ya que ambos arreglos cuentan con vistas o áreas separadas para mostrar elementos de contenido.</p>

<p>Los menús secundarios son opcionales y sólo se necesitan si hay suficientes controles que requieran un menú.</p>

</section>

<section id="guidance">
<title>Dirección</title>

<list>
<item><p>Los menús secundarios no deberían reproducir elementos de menú incluidos en <link xref="primary-menus">menús primarios</link>, tales como <gui>Preferencias</gui> o <gui>Acerca de</gui>. Sin embargo, a veces puede ser útil mostrar la <gui>Ayuda</gui>.</p></item>
<item><p>Las <link xref="menus">directrices estándar para menús</link> se pueden usar para diseñar menús secundarios.</p></item>
<item><p>Un menú de la barra de cabecera está dentro de una <link xref="popovers">ventana emergente</link>. Como tal, un menú de la barra de cabecera puede incluir una variedad de controles, como un grupo de botones.</p></item>
<item><p>Los menús secundarios deben incluir elementos de menú para salir o cerrar.</p></item>
</list>

</section>

<section id="api-reference">
<title>Referencia de la API</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkMenuButton.html">GtkMenuButton</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkPopoverMenu.html">GtkPopoverMenu</link></p></item>
</list>

</section>

</page>
