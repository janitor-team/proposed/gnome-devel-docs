<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="progress-bars" xml:lang="es">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/progress-bars.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alex Puchades</mal:name>
      <mal:email>alex94puchades@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamín Valero Espinosa</mal:name>
      <mal:email>benjavalero@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rosa Elena Pérez</mal:name>
      <mal:email>rozydicemiau@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oscar Rafael Arachi</mal:name>
      <mal:email>arachi910@gmail.com</mal:email>
      <mal:years>2014 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miguel Ajuech</mal:name>
      <mal:email>miguel.ann.28@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adrián Pérez Domínguez</mal:name>
      <mal:email>adrian@aztli.org</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Celina Osorio Ochoa</mal:name>
      <mal:email>chelinoska@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Dario Amigon Espinoza</mal:name>
      <mal:email>daramigon@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Yenisei Ramírez Pérez</mal:name>
      <mal:email>yeniseirape@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Roberto Ramíres Mendoza</mal:name>
      <mal:email>aczella@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bernardo Sánchez Romero</mal:name>
      <mal:email>b.sanchez.rom@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Emmanuel Márquez Juárez</mal:name>
      <mal:email>resident3333@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Romero Serrano</mal:name>
      <mal:email>lemi136@hotmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Omar Garcia Guzman</mal:name>
      <mal:email>garciag.omar91@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Edilberto Huerta Niño</mal:name>
      <mal:email>edilberto.huerta0290@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Barras de progreso</title>

<p>Las barras de progreso indican que se está realizando una tarea, junto con el progreso que se ha hecho en la misma.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/progress-bars.svg"/>

<section id="when-to-use">
<title>Cuándo usarlas</title>

<p>Normalmente es necesario indicar que el progreso se está realizando cuando una operación tarda más de tres segundos. Esto asegura que el usuario entiende que tiene que esperar, y que no ha ocurrido ningún error.</p>

<p>Al indicar el progreso, la elección principal es entre una barra de progreso o un <link xref="progress-spinners">indicador de progreso incremental</link>. Las barras de progreso indica que cantidad de la tarea se ha completado. Son por lo tanto útiles para tareas que tardan bastante tiempo. Como regla general, use sólo las barras de progreso para tareas que tardan más de 30 segundos. Para tareas más cortas, los <link xref="progress-spinners">indicadores de progreso incrementales</link> son una mejor elección.</p>

</section>

<section id="types">
<title>Tipos</title>

<p>Hay tres tipos de barras de progreso</p>

<list>
<item><p>Tiempo restante: indica cuánto tiempo le queda a una operación.</p></item>
<item><p>Tiempo habitual: indica cuánto tiempo queda, basado en una estimación de la duración esperada.</p></item>
<item><p>Indeterminado: sólo indica que la operación está en progreso, pero no cuánto tiempo tardará.</p></item>
</list>

<p>En las barras de progreso es preferible la precisión. Cuando sea posible, use una barra de progreso de tiempo restante, seguida de un tiempo habitual. Trate de evitar las barras de progreso indeterminadas.</p>

</section>

<section id="progress-text">
<title>Texto de progreso</title>

<p>Cada barra de progreso incluye un texto descriptivo. Este texto debe proporcionar una idea sobre cuánto queda para terminar la tarea. Al elegir el texto de la barra de progreso:</p>

<list>
<item><p>Considere siempre qué es más relevante e interesante para el usuario.</p></item>
<item><p>A menudo es mejor proporcionar información específica que porcentajes. Por ejemplo, <gui>rotadas 13 de 19 imágenes</gui> o <gui>descargados 12.1 de 30 MB</gui> en lugar de <gui>13% completado</gui>.</p></item>
<item><p>Para tareas muy largas, puede ser interesante mostrar una estimación del tiempo restante en el texto de la barra de progreso. Si no hay disponibles otra información, se puede mostrar esto únicamente. Como alternativa, puede aparecer junto al texto acerca de la tarea; sin embargo, tenga cuidado de no sobrecargar al usuario con demasiada información al hacer esto, y use los <link xref="typography">convenios tipográficos</link> para diferenciar la información más relevante.</p></item>
<item><p>Si el tiempo restante es estimado, use la palabra <gui>about</gui>. Por ejemplo: <gui>About 3 minutes left</gui>.</p></item>
</list>

</section>

<section id="task-stages">
<title>Estados de las tareas</title>

<p>Algunas tareas pueden componerse de una serie de fases, cada una de las cuales tiene diferentes opciones para estimar el tiempo. Puede ser posible estimar el tiempo restante para una parte de la tarea, pero no para otra parte, por ejemplo. En estas situaciones:</p>

<list>
<item><p>Comunique sólo las diferentes fases de una tarea cuando sean relevantes para el usuario. En general, eso no es necesario, y tampoco es deseable informar de las diferentes fases de una tareas.</p></item>
<item><p>Si una tarea incluye tiempo restante y fases de tiempo estimado, intente crear una composición de tiempo habitual y barra de progreso.</p></item>
<item><p>Si una tarea incluye una fase de progreso indeterminado, la barra de progreso puede mostrar una parte indeterminada para parte de la tarea. Sin embargo, debe evitar mostrar barras de progreso indeterminadas durante largos períodos de tiempo, y debe intentar mantener el número de cambios de modo de las barras de progreso en un mínimo absoluto. Evite las barras de progreso indeterminadas siempre que sea posible.</p></item>
</list>

</section>

<section id="sub-tasks">
<title>Subtareas</title>

<p>Si una tarea se compone de varias subtareas (como descargar varios archivos a la vez), generalmente es recomendable mostrar una una única barra de progreso que indica el progreso total de todas las tareas. Sin embargo, ha algunas situaciones en las que este puede no ser el caso:</p>

<list>
<item><p>Si es especialmente útil para el usuario conocer el progreso individual de cada subtarea. (Como alternativa, el completado de cada subtarea se puede indicar con un texto de progreso).</p></item>
<item><p>Si puede ser necesario pausar o detener una subtarea (consulte la sección <link xref="#general-guidelines">guía general</link> sobre esto, más abajo).</p></item>
<item><p>Si las subtareas ya están indicadas en la interfaz de usuario de su aplicación. En este caso, puede ser menos invasivo mostrar una barra de progreso para cada elemento en línea.</p></item>
</list>

<p>Al mostrar las barras de progreso para las subtareas:</p>

<list>
<item><p>Cada subtarea debe cumplir las guís de uso de barras de progreso (consulte la sección <link xref="#when-to-use">cuándo usar</link>, más arriba).</p></item>
<item><p>En general, no es necesario mostrar una barra de progreso para el progreso global a través de todas las subtareas.</p></item>
</list>

</section>

<section id="progress-windows">
<title>Ventanas de progreso</title>

<p>Antiguamente, las ventanas de progreso eran una manera habitual de mostrar las barras de progreso. Estas ventanas secundarias mostraban la duración de la tarea y contenían una o más barras de progreso. En general, las ventanas de progreso no son recomendables, ya que cerrar la ventana puede no ser muy claro y puede ocultar controles útiles o contenido.</p>

<p>Cuando sea posible, las barras de progreso se deben mostrar en línea, y deben tener una relación visual cercana con los elementos de contenido o los controles que representan la tarea en curso.</p>

</section>

<section id="general-guidelines">
<title>Guías Generales</title>

<list>
<item><p>Si la operación en curso es potencialmente destructiva o usa muchos recursos, considere el poner un botón de pausa y/o cancelar junto a la barra de progreso.</p></item>
<item><p>Asegúrese de que las barras de tiempo restante y de tiempo habitual miden el tiempo o el trabajo total, no sólo uno de los pasos individuales.</p></item>
<item><p>Actualice el tiempo restante de las barras de progreso cuando haya cambios que hacen que la operación vaya más rápida o más lenta.</p></item>
<item><p>Al usar una barra de progreso de tiempo habitual, si su aplicación sobrestima la cantidad de trabajo terminado, la longitud de la barra puede indicar <gui>casi completado</gui> hasta que la operación se complete. Si su aplicación infraestima cuando trabajo se ha completado, rellene la parte restante de la barra cuando la operación se complete.</p></item>
</list>

</section>

<section id="api-reference">
<title>Referencia de la API</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkProgressBar.html">GtkProgressBar</link></p></item>
</list>
</section>

</page>
