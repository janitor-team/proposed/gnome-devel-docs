<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:itst="http://itstool.org/extensions/" type="topic" id="keyboard-input" xml:lang="es">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <desc>Navegación con el teclado, acceso y atajos del teclado.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alex Puchades</mal:name>
      <mal:email>alex94puchades@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamín Valero Espinosa</mal:name>
      <mal:email>benjavalero@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rosa Elena Pérez</mal:name>
      <mal:email>rozydicemiau@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oscar Rafael Arachi</mal:name>
      <mal:email>arachi910@gmail.com</mal:email>
      <mal:years>2014 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miguel Ajuech</mal:name>
      <mal:email>miguel.ann.28@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adrián Pérez Domínguez</mal:name>
      <mal:email>adrian@aztli.org</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Celina Osorio Ochoa</mal:name>
      <mal:email>chelinoska@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Dario Amigon Espinoza</mal:name>
      <mal:email>daramigon@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Yenisei Ramírez Pérez</mal:name>
      <mal:email>yeniseirape@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Roberto Ramíres Mendoza</mal:name>
      <mal:email>aczella@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bernardo Sánchez Romero</mal:name>
      <mal:email>b.sanchez.rom@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Emmanuel Márquez Juárez</mal:name>
      <mal:email>resident3333@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Romero Serrano</mal:name>
      <mal:email>lemi136@hotmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Omar Garcia Guzman</mal:name>
      <mal:email>garciag.omar91@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Edilberto Huerta Niño</mal:name>
      <mal:email>edilberto.huerta0290@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Entrada de teclado</title>

<p>Los teclados son una manera habitual de interactuar con las interfaces de usuario. Proporcionan una manera adecuada y efectiva de usar aplicaciones en una amplia variedad de situaciones, y pueden ser más rápido y eficientes que otros métodos de entrada. Los teclados también son vitales para personas con deficiencias visuales o de movilidad.</p>

<p>Debe asegurarse de que toda la funcionalidad ofrecida por su aplicación es accesible usando un teclado. Probar a usar su aplicación sólo con el teclado es una buena manera de probarlo.</p>

<p>La integración del teclado tiene tres aspectos en GNOME y GTK: navegación, teclas de acceso y atajos del teclado. La <link xref="search">Búsqueda</link> es otro aspecto adicional.</p>

<section id="keyboard-navigation">
<title>Navegación con el teclado</title>

<p>Asegúrese de que es posible moverse e interactuar con todas las partes de su interfaz de usuario usando el teclado, siguiendo esta guía.</p>

<list>
<item><p>Siga las teclas estándar de GNOME para la navegación. <key>Tab</key> es la tecla estándar para moverse por la interfaz en GTK y GNOME.</p></item>
<item><p>Use un orden de navegación lógica con el teclado. Al navegar por una ventana con <key>Tab</key>, el foco del teclado se debe mover entre los controles con un orden predecible. En configuraciones regionales del oeste, lo normal es de derecha a izquierda y de arriba a abajo.</p></item>
<item><p>Además de la navegación usando la tecla <key>Tab</key>, haga un esfuerzo para permitir el movimiento usando las teclas de flechas, tanto en los elementos de la interfaz del usuario (tales como listas, rejillas de iconos o barras laterales) como entre ellos.</p></item>
</list>

<note><p>Si activar un control hace que se activen otros controles, no dé el foco automáticamente al primer control dependiente cuando se active; deje el foco donde está.</p></note>

<section id="navigation-keys">
<title>Teclas de navegación estándar</title>

<table>
<thead>
<tr>
<td><p>Combinación de teclas</p></td>
<td><p>Función</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p><key>Tab</key> y <keyseq><key>Mayús</key><key>Tab</key></keyseq></p></td>
<td><p>Mueve el foto del teclado al siguiente/anterior control</p></td>
</tr>
<tr>
<td><p><keyseq><key>Ctrl</key><key>Tab</key></keyseq> y <keyseq><key>Mayús</key><key>Ctrl</key><key>Tab</key></keyseq></p></td>
<td><p>Mueve el foco del teclado fuera del widget hasta el control siguiente/anterior, en aquellas situaciones en las que Tab tiene otra función.</p></td>
</tr>
<tr>
<td><p><keyseq><key>Ctrl</key><key>Tab</key></keyseq> y <keyseq><key>Mayús</key><key>Ctrl</key><key>Tab</key></keyseq></p></td>
<td><p>Mueve el foto del teclado al siguiente/anterior grupo de controles</p></td>
</tr>
<tr>
<td><p><keyseq><key>Ctrl</key><key>F1</key></keyseq></p></td>
<td><p>Muestra un consejo para la ventana o control actualmente con el foco</p></td>
</tr>
<tr>
<td><p><keyseq><key>Mayús</key><key>F1</key></keyseq></p></td>
<td><p>Mostrar la ayuda sensible a contexto para la ventana o control actualmente con el foco</p></td>
</tr>
<tr>
<td><p><key>F6</key> y <keyseq><key>Mayús</key><key>F6</key></keyseq></p></td>
<td><p>Dar el foco al siguiente/anterior panel en una ventana GtkPaned</p></td>
</tr>
<tr>
<td><p><key>F8</key></p></td>
<td><p>Dar el foco a la barra agrupadora en la ventana con paneles</p></td>
</tr>
<tr>
<td><p><key>F10</key></p></td>
<td><p>Dar el foco a la barra de menu o a la barra de menú de la cabecera abierta</p></td>
</tr>
<tr>
<td><p><key>Espacio</key></p></td>
<td><p>Conmutar el estado de la casilla de verificación, botón de radio o botón de conmutación con el foco</p></td>
</tr>
<tr>
<td><p><key>Intro</key></p></td>
<td><p>Activar el botón, el elemento del menú, etc. con el foco</p></td>
</tr>
<tr>
<td><p><key>Intro</key> y <key>Fin</key></p></td>
<td><p>Seleccionar/moverse al primer elemento en un widget seleccionado</p></td>
</tr>
<tr>
<td><p><key>Re Pág</key>, <keyseq><key>Ctrl</key><key>Re Pág</key></keyseq>, <key>Av Pág</key> y <keyseq><key>Ctrl</key><key>Av Pág</key></keyseq></p></td>
<td><p>Desplazar una página la vista seleccionada arriba/izquierda/abajo/derecha</p></td>
</tr>
<tr>
<td><p><key>Esc</key></p></td>
<td><p>Cierra o sale del contexto actual, si es transitorio. Esto se debe usar de manera consistente en menús, ventanas emergentes o <link xref="dialogs#default-action-and-escape">diálogos</link>, o cualquier otro elemento o modo temporal de la IU.</p></td>
</tr>
</tbody>
</table>

</section>
</section>

<section id="access-keys">
<title>Teclas de acceso</title>

<p>Las teclas de acceso permiten operar en los controles etiquetados usando <key>Alt</key>. Están señalados con una letra subrayada en cada etiqueta de control (que se muestra cuando se pulsa <key>Alt</key>).</p>

<list>
<item><p>Cuando sea posible, todos los elementos etiquetados deben tener una tecla de acceso.</p></item>
<item><p>Elija teclas de acceso fáciles de recordar. Normalmente esto significa usar la primera letra de la etiqueta. si la etiqueta tiene más de una palabra, también se puede usar la primera letra de alguna de las otras palabras. Además, si otra letra proporciona una mejor asociación (por ejemplo, «x» en «extra grande», considere usar esa letra en su lugar.</p></item>
<item><p>Evite asociar teclas de acceso a letras «delgadas» (como la «i» o la «l» minúsuclas) o a letras descendentes (como la «g» o la «y» minúsculas) a menos que sea imprescindible. A veces, el subrayado no es del todo claro con estos caracteres.</p></item>
<item><p>Si la elección de las teclas de acceso es difícil, asigne primero teclas de acceso a los controles usados más frecuentemente. Si la primera letra no está disponible, elija una consonante de la etiqueta fácil de recordar. Por ejemplo, «p» en «Reemplazar». Asigne vocales sólo cuando no haya consonantes disponibles.</p></item>
<item><p>Tenga en cuenta que las teclas de acceso se deben traducir junto con el resto de la cadena a la que pertenecen, por lo que aunque no haya conflictos en el idioma original, puede haberlos en las traducciones.</p></item>
</list>

</section>

<section id="shortcut-keys">
<title>Atajos de teclado</title>

<p>Los atajos del teclado proporcionan una manera adecuada de acceder a las operaciones habituales. Pueden ser teclas únicas o combinaciones de varias pulsaciones de teclas (normalmente una tecla modificadora con una tecla normal).</p>

<list>
<item><p>No asigne atajos del teclado del sistema a su aplicación. Consulte más abajo los detalles sobre esto.</p></item>
<item><p>Use los atajos estándar de GNOME (ver más abajo) si su aplicación soporta estas funciones. Esto asegura la consistencia entre aplicaciones de GNOME y ayuda a descubrirlos.</p></item>
<item><p>Asigne atajos de teclado a las acciones más comúnmente usadas en su aplicación. Sin embargo, no intente asignar un atajo de teclado a cada cosa.</p></item>
<item><p>Pruebe a usar <key>Ctrl</key>junto con otra letra para sus propios atajos. <keyseq><key>Mayús</key><key>Ctrl</key></keyseq> y una letra es el patrón recomendado para atajos que revierten o extienden otra función. Por ejemplo, <keyseq><key>Ctrl</key><key>Z</key></keyseq> y <keyseq><key>Mayús</key><key>Ctrl</key><key>Z</key></keyseq> para <gui>Deshacer</gui> y <gui>Rehacer</gui>.</p></item>
<item><p>Los nuevos atajos deben ser tan nemónicas como sea posible, ya que así serán más fáciles de recordar <keyseq><key>Ctrl</key><key>E</key></keyseq> podría ser un buen atajo para un elemento de menú llamado <gui>Editar página</gui>.</p></item>
<item><p>Los atajos que se pueden usar fácilmente con una sola mano son preferibles para las operaciones habituales.</p></item>
<item><p>No use la tecla <key>Alt</key> para los atajos, ya que puede crear conflictos con las teclas de acceso.</p></item>
</list>

</section>

<section id="system-shortcuts">
<title>Atajos reservados para el sistema</title>

<p>Los siguientes atajos de teclado no deberían sobrescribirlos las aplicaciones.</p>

<p>GNOME 3 hace un uso exclusivo de la tecla <key>Super</key>, comúnmente conocida como tecla Windows, para los atajos del sistema. Por lo tanto, la tecla <key>Super</key> no se debe usar en las aplicaciones.</p>

<table>
<thead>
<tr>
<td><p>Función</p></td>
<td><p>Combinación de teclas</p></td>
<td><p>Atajo heredado</p></td>
<td><p>Descripción</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p>Vista de actividades</p></td>
<td><p><key>Súper</key></p></td>
<td><p>Ninguno</p></td>
<td><p>Abre y cierra la vista de actividades</p></td>
</tr>
<tr>
<td><p>Vista de aplicaciones</p></td>
<td><p><keyseq><key>Súper</key><key>A</key></keyseq></p></td>
<td><p>Ninguno</p></td>
<td><p>Abre y cierra la vista de aplicaciones de la vista de actividades</p></td>
</tr>
<tr>
<td><p>Bandeja de mensajes</p></td>
<td><p><keyseq><key>Súper</key><key>M</key></keyseq></p></td>
<td><p>Ninguno</p></td>
<td><p>Cambia la visibilidad de la bandeja de mensajes.</p></td>
</tr>
<tr>
<td><p>Bloquear</p></td>
<td><p><keyseq><key>Super</key><key>L</key></keyseq></p></td>
<td><p>Ninguno</p></td>
<td><p>Bloquea el sistema, vaciando la pantalla y solicitando una contraseña para desbloquear, si se ha establecido alguna.</p></td>
</tr>
<tr>
<td><p>Cambiar entre aplicaciones</p></td>
<td><p><keyseq><key>Súper</key><key>Tab</key></keyseq> y <keyseq><key>Mayús</key><key>Súper</key><key>Tab</key></keyseq></p></td>
<td><p><keyseq><key>Alt</key><key>Tab</key></keyseq> y <keyseq><key>Mayús</key><key>Alt</key><key>Tab</key></keyseq></p></td>
<td><p>Cambia el foco la siguiente o anterior aplicación</p></td>
</tr>
<tr>
<td><p>Cambiar entre ventanas</p></td>
<td><p><keyseq><key>Súper</key><key>`</key></keyseq> y <keyseq><key>Mayús</key><key>Súper</key><key>`</key></keyseq></p></td>
<td><p><keyseq><key>Alt</key><key>F6</key></keyseq> y <keyseq><key>Mayús</key><key>Alt</key><key>F6</key></keyseq></p></td>
<td><p>Cambiar el foco al siguiente o anterior ventana secundaria asociada con la aplicación</p></td>
</tr>
<tr>
<td><p><gui>Maximizar</gui></p></td>
<td><p><keyseq><key>Súper</key><key>↑</key></keyseq></p></td>
<td><p><keyseq><key>Alt</key><key>F10</key></keyseq></p></td>
<td><p>Maximizar la ventana que tenga el foco</p></td>
</tr>
<tr>
<td><p><gui>Restaurar</gui></p></td>
<td><p><keyseq><key>Súper</key><key>↓</key></keyseq></p></td>
<td><p><keyseq><key>Alt</key><key>F5</key></keyseq></p></td>
<td><p>Restaurar la ventana que tiene el foco a su estado anterior</p></td>
</tr>
<tr>
<td><p><gui>Ocultar</gui></p></td>
<td><p><keyseq><key>Súper</key><key>H</key></keyseq></p></td>
<td><p><keyseq><key>Alt</key><key>F9</key></keyseq></p></td>
<td><p>Ocultar la ventana con el foco</p></td>
</tr>

<tr>
<td><p>Cambiar el área del sistema</p></td>
<td><p>Ninguno</p></td>
<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq> y <keyseq><key>Mayús</key><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq></p></td>
<td><p>Cambia el foco a las áreas primarias del sistema: ventanas, barra superior, bandeja de mensajes</p></td>
</tr>
<tr>
<td><p>Apagar</p></td>
<td><p>Ninguno</p></td>
<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Supr</key></keyseq></p></td>
<td><p>Pregunta al usuario si apagar el sistema. Normalmente, este atajo está desactivado de manera predeterminada.</p></td>
</tr>
<tr>
<td><p>Menú de ventana</p></td>
<td><p><keyseq><key>Alt</key><key>Espacio</key></keyseq></p></td>
<td><p>Ninguno</p></td>
<td><p>Abre el menú de ventana para la ventana actual</p></td>
</tr>
<tr>
<td><p><gui>Cerrar</gui></p></td>
<td><p>Ninguno</p></td>
<td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
<td><p>Cierra la ventana con el foco</p></td>
</tr>
<tr>
<td><p><gui>Mover</gui></p></td>
<td><p>Ninguno</p></td>
<td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
<td><p>Mover la ventana con el foco</p></td>
</tr>
<tr>
<td><p><gui>Redimensionar</gui></p></td>
<td><p>Ninguno</p></td>
<td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
<td><p>Redimensionar la ventana con el foco</p></td>
</tr>
</tbody>
</table>

<p>Además, los atajos de entrada de caracteres Unicode se deben evitar. Esto incluye <keyseq><key>Mayús</key><key>Ctrl</key><key>A</key></keyseq> a <keyseq><key>Mayús</key><key>Ctrl</key><key>F</key></keyseq>, o <keyseq><key>Mayús</key><key>Ctrl</key><key>0</key></keyseq> a <keyseq><key>Mayús</key><key>Ctrl</key><key>9</key></keyseq>.</p>

</section>

<section id="application-shortcuts">
<title>Atajos estándar de aplicaciones</title>

<p>Esta sección detalla los atajos del teclado más comunes en aplicaciones. Con la excepción de los atajos de aplicaciones, estos atajos sólo se deben usar cuando la correspondiente acción esté incluída en su aplicación. Los atajos estándar se pueden asignar a otras acciones si la acción estándar no está disponible.</p>

<p>Esta sección también proporciona ayuda sobre los elementos de menú estándar que se deben usar en una <link xref="menu-bars">barra de menú</link>.</p>

<section id="application">
<title>Aplicación</title>

<p>Atajos del teclado de aplicaciones estándar y elementos de menú. Estos atajos de aplicaciones no se deben reasignar a otras acciones, aunque la correspondiente acción no esté dentro de la aplicación.</p>

<table>
<thead>
<tr>
<td><p>Etiqueta</p></td>
<td><p>Combinación de teclas</p></td>
<td><p>Descripción</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p><gui>Ayuda</gui></p></td>
<td><p><key>F1</key></p></td>
<td><p>Abre el explorador de ayuda predeterminado en la página de contenido de la aplicación.</p></td>
</tr>
<tr>
<td><p><gui>Acerca de</gui></p></td>
<td><p>Ninguno</p></td>
<td><p>Abre el diálogo Acerca de para la aplicación. Use el diálogo estándar de GNOME 3 para esto.</p></td>
</tr>
<tr>
<td><p><gui>Salir</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>Q</key></keyseq></p></td>
<td><p>Cierra la aplicación, incluyendo todas sus ventanas.</p></td>
</tr>
</tbody>
</table>

</section>

<section id="file">
<title>Archivo</title>

<p>Atajos de teclado de archivo y elementos de menú estándar.</p>

<table>
<thead>
<tr>
<td><p>Etiqueta</p></td>
<td><p>Combinación de teclas</p></td>
<td><p>Descripción</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p><gui>Nuevo</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>N</key></keyseq></p></td>
<td><p>Crear un elemento de contenido nuevo, a menudo (pero no siempre) en una ventana primaria o pestaña nuevas. Si su aplicación puede crear varios tipos de documentos, puede hacer que el elemento <gui>Nuevo</gui> sea un submenú que contenga un elemtno de menú para cada tipo.. Etiquete estos elementos <gui>Nuevo</gui> tipo de documento, haga la primera entrada en el submenú el tipo de documento más utilizado, y asígnele el atajo <keyseq><key>Ctrl</key><key>N</key></keyseq>.</p></td>
</tr>
<tr>
<td><p><gui>Abrir…</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>O</key></keyseq></p></td>
<td><p>Abre un elemento de contenido existente, a menudo presentando al usuario un diálogo <gui>Abrir archivo</gui>. Si el archivo elegido ya está abierto en la aplicación, levante su ventana en lugar de abrir una nueva.</p></td>
</tr>
<tr>
<td><p><gui>Abrir recientes</gui></p></td>
<td><p>Ninguno</p></td>
<td><p>Un submenú que contiene una lista de no más de seis archivos usado recientemente, ordenados por los usados más recientemente.</p></td>
</tr>
<tr>
<td><p><gui>Guardar</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>S</key></keyseq></p></td>
<td><p>Guarda el elemento de contenido actual. Si el documento ya tiene un nombre de archivo asociado, lo guarda inmediatamente sin más interacción con el usuario. Si hay opciones adicionales involucradas al guardar, pregunte la primera vez que guarda el documento, pero a continuación use los mismo valores hasta que el usuario los cambie. Si el documento no tiene un nombre de archivo o si es de solo lectura, seleccionar este elemento debería ser lo mismo que seleccionar <gui>Guardar como</gui>.</p></td>
</tr>
<tr>
<td><p><gui>Guardar como…</gui></p></td>
<td><p><keyseq><key>Mayús</key><key>Ctrl</key><key>S</key></keyseq></p></td>
<td><p>Guarda el elemento de contenido con un nombre de archivo nuevo. Preséntele al usuario un diálogo <gui>Guardar como</gui> estándar, y guarde el archivo con el nombre elegido.</p></td>
</tr>
<tr>
<td><p><gui>Guardar una copia…</gui></p></td>
<td><p>Ninguno</p></td>
<td><p>Le pide al usuario un nombre de archivo, con el que se guarda una copia del documento. No altere la vista o el nombre de archivo en el documento original. A continuación, todos los cambios se seguirán haciendo sobre el documento original hasta que el usuario especifique lo contrario, por ejemplo eligiendo el comando <gui>Guardar como</gui>.</p>
<p>Al igual que el diálogo <gui>Guardar como</gui>, el diálogo <gui>Guardar una copia</gui> puede presentar distintas maneras de guardar los datos. Por ejemplo, una imagen puede guardarse en un formato nativo o como PNG.</p></td>
</tr>
<tr>
<td><p><gui>Configuración de página</gui></p></td>
<td><p>Ninguno</p></td>
<td><p>Permite al usuario controlar las opciones relacionadas con la impresión. Preséntele al usuario un diálogo que le permita configurar opciones como formato vertical o apaisado, márgenes, etc.</p></td>
</tr>
<tr>
<td><p><gui>Vista previa de impresión</gui></p></td>
<td><p><keyseq><key>Mayús</key><key>Ctrl</key><key>P</key></keyseq></p></td>
<td><p>Muestra al usuario cómo se verá el documento impreso. Presente una ventana nueva que contenga una representación precisa de la apariencia del documento como si se hubiera impreso.</p></td>
</tr>
<tr>
<td><p><gui>Imprimir…</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>P</key></keyseq></p></td>
<td><p>Imprime el documento actual. Preséntele al usuario un diálogo permitiéndole establecer opciones como el rango de páginas que imprimir, la impresora que usar, etc. El diálogo debe contener un botón etiquetado <guibutton>Imprimir</guibutton> que comience la impresión y lo cierre.</p></td>
</tr>
<tr>
<td><p><gui>Enviar a…</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>M</key></keyseq></p></td>
<td><p>Proporciona al usuario una manera de adjuntar o enviar el documento actual como un correo o un adjunto de un correo, dependiendo de su formato. Puede proporcionar más de un elemento <gui>Enviar</gui>, dependiendo de las opciones disponibles. Si hay más de dos, muévalos a un submenú. Por ejemplo, si solo están disponibles <gui>Enviar por correo-e</gui> y <gui>Enviar por Bluetooth</gui>, déjelas en el menú superior. Si hay una tercera opción, como <gui>Enviar por FTP</gui>, póngalas en un submenú «Enviar».</p></td>
</tr>
<tr>
<td><p><gui>Propiedades…</gui></p></td>
<td><p><keyseq><key>Alt</key><key>Intro</key></keyseq></p></td>
<td><p>Abre la ventana de <gui>Propiedades</gui> del documento. Puede contener información editable, como el nombre del autor del documento, o de solo lectura, como el número de palabras en el documento, o una combinación de ambas. El atajo <keyseq><key>Alt</key><key>Intro</key></keyseq> no debe proporcionarse donde <key>Intro</key> se usa más frecuentemente para insertar una línea nueva.</p></td>
</tr>
<tr>
<td><p><gui>Cerrar</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>W</key></keyseq></p></td>
<td><p>Cierra la ventana o la pestaña actual. Si la ventana usa pestañas, y sólo hay una abierta, el atajo debe cerrar la ventana.</p></td>
</tr>
</tbody>
</table>

</section>

<section id="edit">
<title itst:context="menu">Editar</title>

<p>Atajos de teclado de edición y elementos de menú estándar</p>

<table>
<thead>
<tr>
<td><p>Etiqueta</p></td>
<td><p>Combinación de teclas</p></td>
<td><p>Descripción</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p><gui>Deshacer <em>acción</em></gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>Z</key></keyseq></p></td>
<td><p>Revierte el efecto de la acción anterior.</p></td>
</tr>
<tr>
<td><p><gui>Rehacer <em>acción</em></gui></p></td>
<td><p><keyseq><key>Mayús</key><key>Ctrl</key><key>Z</key></keyseq></p></td>
<td><p>Realiza la acción siguiente en el listado histórico de deshacer, después de que el usuario se haya movido hacia atrás por la lista con el comando <gui>Deshacer</gui>.</p></td>
</tr>
<tr>
<td><p><gui>Cortar</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>X</key></keyseq></p></td>
<td><p>Elimina el contenido seleccionado y lo pone en el portapapeles. Visualmente, quita el contenido del documento igual que <gui>Supr</gui>.</p></td>
</tr>
<tr>
<td><p><gui>Copiar</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>C</key></keyseq></p></td>
<td><p>Copia el contenido seleccionado al portapapeles.</p></td>
</tr>
<tr>
<td><p><gui>Pegar</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>V</key></keyseq></p></td>
<td><p>Inserta el contenido del cortapapeles en el elemento de contenido. Al editar texto, si no hay una selección actual, use el signo de intercalación  como punto de inserción. Si hay una selección actual, reemplácela por el contenido del portapapeles.</p></td>
</tr>
<tr>
<td><p><gui>Pegado especial…</gui></p></td>
<td><p><keyseq><key>Mayús</key><key>Ctrl</key><key>V</key></keyseq></p></td>
<td><p>Inserta una representación no predeterminada del contenido del portapapeles. Abre un diálogo presentando una lista de los formatos disponibles que el usuario puede seleccionar. Por ejemplo, si el portapapeles contiene un archivo PNG copiado de un gestor de archivos, la imagen podría empotrarse o enlazarse al documento, para que siempre refleje los cambios sufridos en el disco.</p></td>
</tr>
<tr>
<td><p><gui>Duplicar</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>U</key></keyseq></p></td>
<td><p>Crea una copia duplicada del objeto seleccionado.</p></td>
</tr>
<tr>
<td><p><gui>Eliminar</gui></p></td>
<td><p><key>Supr</key></p></td>
<td><p>Quita el contenido seleccionado sin colocarlo en el portapapeles.</p></td>
</tr>
<tr>
<td><p><gui>Seleccionar todo</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>A</key></keyseq></p></td>
<td><p>Selecciona todo el contenido del documento actual.</p></td>
</tr>
<tr>
<td><p><gui>Deseleccionar todo</gui></p></td>
<td><p><keyseq><key>Mayús</key><key>Ctrl</key><key>A</key></keyseq></p></td>
<td><p>Deselecciona todo el contenido en el documento actual. Proporcione este elemento solo en situaciones en las que otro método de deshacer la selección no sea posible o visible al usuario. Por ejemplo, en aplicaciones de gráficos complejas donde la selección y la deselección no es siempre posible solo con las teclas de dirección. Nota: no proporcione una opción <gui>Deseleccionar todo</gui> en los campos de texto, ya que <keyseq><key>Mayús</key><key>Ctrl</key><key>hex</key></keyseq> se usa para introducir caracteres Unicode, por lo que el atajo no funcionará.</p></td>
</tr>
<tr>
<td><p><gui>Buscar…</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>F</key></keyseq></p></td>
<td><p>Muestra una interfaz de usuario que le permite buscar un contenido específico en el elemento de contenido o página actual.</p></td>
</tr>
<tr>
<td><p><gui>Buscar siguiente</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>G</key></keyseq></p></td>
<td><p>Selecciona la siguiente instancia del último término de búsqueda en el documento actual.</p></td>
</tr>
<tr>
<td><p><gui>Buscar anterior</gui></p></td>
<td><p><keyseq><key>Mayús</key><key>Ctrl</key><key>G</key></keyseq></p></td>
<td><p>Selecciona la instancia previa del último término de <gui>Búsqueda</gui> en el documento actual.</p></td>
</tr>
<tr>
<td><p><gui>Reemplazar…</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>H</key></keyseq></p></td>
<td><p>Muestra una interfaz de usuario que le permite buscar un contenido específico y reemplazar cada ocurrencia.</p></td>
</tr>
</tbody>
</table>

</section>

<section id="view">
<title>Ver</title>

<p>Ver los atajos del teclado y los elementos del menú.</p>

<table>
<thead>
<tr>
<td><p>Etiqueta</p></td>
<td><p>Combinación de teclas</p></td>
<td><p>Descripción</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p><gui>Iconos</gui></p></td>
<td><p>Ninguno</p></td>
<td><p>Muestra el contenido como una cuadrícula de iconos. Esto es un elemento de menú de botón de radio.</p></td>
</tr>
<tr>
<td><p><gui>Lista</gui></p></td>
<td><p>Ninguno</p></td>
<td><p>Muestra el contenido como una lista. Esto es un elemento de menú de botón de radio.</p></td>
</tr>
<tr>
<td><p><gui>Ordenar por…</gui></p></td>
<td><p>Ninguno</p></td>
<td><p>Especifica los criterios por los que se debe ordenar el contenido. Puede abrir un diálogo de preferencias, una ventana emergente o un submenú.</p></td>
</tr>
<tr>
<td><p><gui>Filtrar…</gui></p></td>
<td><p>Ninguno</p></td>
<td><p>Permite filtrar el contenido, abriendo una ventana emergente, un desplegable o un diálogo.</p></td>
</tr>
<tr>
<td><p><gui>Ampliar</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>+</key></keyseq></p></td>
<td><p>Amplía, mostrando el contenido más grande.</p></td>
</tr>
<tr>
<td><p><gui>Reducir</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>-</key></keyseq></p></td>
<td><p>Reduce, mostrando el contenido más pequeño.</p></td>
</tr>
<tr>
<td><p><gui>Tamaño normal</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>0</key></keyseq></p></td>
<td><p>Restaurar el nivel de ampliación a su tamaño predeterminado.</p></td>
</tr>
<tr>
<td><p><gui>Mejor ajuste</gui></p></td>
<td><p>Ninguno</p></td>
<td><p>Hace que el documento ocupe toda la ventana.</p></td>
</tr>
<tr>
<td><p><gui>Recargar</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>R</key></keyseq></p></td>
<td><p>Redibuja la vista actual del documento comprobando primero si ha habido cambios en la fuente original. Por ejemplo, comprobaría si hay actualizaciones en el servidor web antes de redibujar la página.</p></td>
</tr>
</tbody>
</table>

</section>

<section id="format">
<title>Formato</title>

<p>Atajos del teclado de formato estándar y elementos del menú.</p>

<table>
<thead>
<tr>
<td><p>Etiqueta</p></td>
<td><p>Combinación de teclas</p></td>
<td><p>Descripción</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p><gui>Estilo…</gui></p></td>
<td><p>Ninguno</p></td>
<td><p>Establece los atributos de estilo del texto o los objetos seleccionados, individualmente o con un estilo con nombre, predefinido.</p></td>
</tr>
<tr>
<td><p><gui>Tipografía…</gui></p></td>
<td><p>Ninguno</p></td>
<td><p>Establece las propiedades de la tipografía del texto o los objetos seleccionados.</p></td>
</tr>
<tr>
<td><p><gui>Párrafo…</gui></p></td>
<td><p>Ninguno</p></td>
<td><p>Establece las propiedades del párrafo seleccionado.</p></td>
</tr>
<tr>
<td><p><gui>Negrita</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>B</key></keyseq></p></td>
<td><p>Activa o desactiva la negrita para la selección de texto actual. Si una parte del texto ya está en negrita y la otra no, este comando debe poner en negrita todo el texto seleccionado.</p></td>
</tr>
<tr>
<td><p><gui>Cursiva</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>I</key></keyseq></p></td>
<td><p>Activa o desactiva la cursiva para la selección de texto actual. Si una parte del texto ya está en negrita y la otra no, este comando debe poner en cursiva todo el texto seleccionado.</p></td>
</tr>
<tr>
<td><p><gui>Subrayado</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>U</key></keyseq></p></td>
<td><p>Activa o desactiva el subrayado para la selección de texto actual. Si una parte del texto ya está subrayada y la otra no, este comando debe subrayar todo el texto seleccionado.</p></td>
</tr>
<tr>
<td><p><gui>Celdas…</gui></p></td>
<td><p>Ninguno</p></td>
<td><p>Establece las propiedades de las celdas de la tabla seleccionada.</p></td>
</tr>
<tr>
<td><p><gui>Lista…</gui></p></td>
<td><p>Ninguno</p></td>
<td><p>Establece las propiedades de la lista seleccionada, o convierte los párrafos seleccionados en una lista si no están formateados como tal.</p></td>
</tr>
<tr>
<td><p><gui>Capa…</gui></p></td>
<td><p>Ninguno</p></td>
<td><p>Establece las propiedades de todas las capas seleccionadas de un documento con varias capas.</p></td>
</tr>
<tr>
<td><p><gui>Página…</gui></p></td>
<td><p>Ninguno</p></td>
<td><p>Establece las propiedades de todas las páginas del documento o de las seleccionadas.</p></td>
</tr>
</tbody>
</table>

</section>

<section id="bookmarks">
<title>Marcadores</title>

<p>Atajos del teclado para marcadores estándar y elementos del menú.</p>

<table>
<thead>
<tr>
<td><p>Etiqueta</p></td>
<td><p>Combinación de teclas</p></td>
<td><p>Descripción</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p><gui>Añadir marcador</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>D</key></keyseq></p></td>
<td><p>Añade un marcador para la ubicación actual. No muestre un diálogo preguntando un título o una ubicación para el marcador; en su lugar elija valores predeterminados relacionados (por ejemplo, el título del documento como el nombre del marcador) y permita que el usuario lo cambie posteriormente usando la característica de <gui>Editar marcadores</gui>.</p></td>
</tr>
<tr>
<td><p><gui>Editar marcadores</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>B</key></keyseq></p></td>
<td><p>Permite al usuario editar sus marcadores.</p></td>
</tr>
<tr>
<td><p>Lista de marcadores</p></td>
<td><p>Ninguno</p></td>
<td><p>Muestra los marcadores del usuario.</p></td>
</tr>
</tbody>
</table>

</section>

<section id="go">
<title>Ir</title>

<p>Atajos del teclado de navegación de elementos del menú <gui>Ir</gui> estándar.</p>

<table>
<thead>
<tr>
<td><p>Etiqueta</p></td>
<td><p>Combinación de teclas</p></td>
<td><p>Descripción</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p><gui>Atrás</gui></p></td>
<td><p><keyseq><key>Alt</key><key>Izquierda</key></keyseq></p></td>
<td><p>Navega a la ubicación anterior.</p></td>
</tr>
<tr>
<td><p><gui>Adelante</gui></p></td>
<td><p><keyseq><key>Alt</key><key>Derecha</key></keyseq></p></td>
<td><p>Navega a la siguiente ubicación en el histórico de navegación.</p></td>
</tr>
<tr>
<td><p><gui>Arriba</gui></p></td>
<td><p><keyseq><key>Alt</key><key>Arriba</key></keyseq></p></td>
<td><p>Navega al elemento padre del contenido, documento, página o sección.</p></td>
</tr>
<tr>
<td><p><gui>Inicio</gui></p></td>
<td><p><keyseq><key>Alt</key><key>Inicio</key></keyseq></p></td>
<td><p>Navega a la página inicial definida por el usuario o la aplicación.</p></td>
</tr>
<tr>
<td><p><gui>Ubicación</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>L</key></keyseq></p></td>
<td><p>Permite al usuario ir a un URL específico al que navegar.</p></td>
</tr>
<tr>
<td><p><gui>Página anterior</gui></p></td>
<td><p><key>RePág</key></p></td>
<td><p>Navega a la página anterior en el documento.</p></td>
</tr>
<tr>
<td><p><gui>Siguiente página</gui></p></td>
<td><p><key>AvPág</key></p></td>
<td><p>Navega a la siguiente página en el documento.</p></td>
</tr>
<tr>
<td><p><gui>Ir a la página…</gui></p></td>
<td><p>Ninguno</p></td>
<td><p>Permite al usuario especificar un número de página al que navegar. Las aplicaciones basadas en texto puede incluir también un elemento de menú <gui>Ir a la línea…</gui> que permita al usuario saltar al número de línea especificado.</p></td>
</tr>
<tr>
<td><p><gui>Primera página</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>Inicio</key></keyseq></p></td>
<td><p>Navega hasta la primera página del documento.</p></td>
</tr>
<tr>
<td><p><gui>Última página</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>Fin</key></keyseq></p></td>
<td><p>Navega hasta la última página del documento.</p></td>
</tr>
</tbody>
</table>

</section>

<section id="windows">
<title>Ventanas</title>

<p>Elementos del menú <gui>Ventana</gui> estándar.</p>

<table>
<thead>
<tr>
<td><p>Etiqueta</p></td>
<td><p>Combinación de teclas</p></td>
<td><p>Descripción</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p><gui>Guardar todo</gui></p></td>
<td><p>Ninguno</p></td>
<td><p>Guarda todos los documentos abiertos. Si actualmente algún documento no tiene nombre de archivo, solicite un nombre de archivo para cada uno de ellos usando el diálogo <gui>Guardar</gui> estándar.</p></td>
</tr>
<tr>
<td><p><gui>Cerrar todo</gui></p></td>
<td><p>Ninguno</p></td>
<td><p>Cierra todos los documentos abiertos. Si hay cambios sin guardar en algún documento muestre una alerta para cada uno de ellos.</p></td>
</tr>
<tr>
<td><p>Lista de ventanas</p></td>
<td><p>Ninguno</p></td>
<td><p>Cada elemento del menú muestra la correspondiente ventana en la parte superior de la pila de ventanas.</p></td>
</tr>
</tbody>
</table>

</section>
</section>

</page>
