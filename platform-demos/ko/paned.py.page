<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="paned.py" xml:lang="ko">
  <info>
    <title type="text">Paned (Python)</title>
    <link type="guide" xref="beginner.py#layout"/>
    <link type="next" xref="signals-callbacks.py"/>
    <revision version="0.1" date="2012-08-15" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>크기 조절 가능한 두 창을 가진 위젯입니다</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2017-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Paned</title>
  <media type="image" mime="image/png" src="media/paned.png"/>
  <p>크기 조절 가능한 두 창에 그림 둘을 두고 수평 방향으로 정렬합니다.</p>

  <links type="section"/>

  <section id="code">
    <title>예제 결과를 만드는 코드</title>
    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Paned Example", application=app)
        self.set_default_size(450, 350)

        # a new widget with two adjustable panes,
        # one on the left and one on the right
        paned = Gtk.Paned.new(Gtk.Orientation.HORIZONTAL)

        # two images
        image1 = Gtk.Image()
        image1.set_from_file("gnome-image.png")
        image2 = Gtk.Image()
        image2.set_from_file("tux.png")

        # add the first image to the left pane
        paned.add1(image1)
        # add the second image to the right pane
        paned.add2(image2)

        # add the panes to the window
        self.add(paned)


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
    <title>Paned 위젯에 쓸만한 메서드</title>
    <p>수직 처리 창을 두려면 <code>Gtk.Orientation.HORIZONTAL</code> 대신 <code>Gtk.Orientation.VERTICAL</code>를 사용하십시오. <code>add1(widget1)</code> 메서드는, 최상단 창에 <code>widget1</code>를 추가하며, <code>add2(widget2)</code> 메서드는 하단 창에 <code>widget2</code>를 추가합니다.</p>
  </section>

  <section id="references">
    <title>API 참고서</title>
    <p>이 예제는 다음 참고자료가 필요합니다:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkPaned.html">GtkPaned</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/gtk3-Standard-Enumerations.html#GtkOrientation">표준 에뮬레이션</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkImage.html">GtkImage</link></p></item>
    </list>
  </section>
</page>
