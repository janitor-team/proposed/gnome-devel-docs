<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="hello-world.py" xml:lang="ko">

  <info>
  <title type="text">Hello World(Python)</title>
    <link type="guide" xref="py#tutorial" group="#first"/>

    <revision version="0.1" date="2013-06-17" status="review"/>

    <credit type="author copyright">
      <name>Susanna Huhtanen</name>
      <email its:translate="no">ihmis.suski@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
    </credit>

    <desc>기본 "hello, world" 프로그램</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2017-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Hello World 프로그램의 <file>tar.xz</file> 파일을 빌드하고, 설치하고, 만들기</title>
    <media type="image" mime="image/png" style="floatend" src="media/hello-world.png"/>
    <synopsis>
      <p>이 지침서에서는 다음 방법을 보여줍니다:</p>
      <list style="numbered">
        <item><p>파이썬과 GTK+를 사용한 작은 "Hello, World" 프로그램을 만듭니다</p></item>
        <item><p><file>.desktop</file> 파일 만들기</p></item>
        <item><p>빌드 시스템 구성 방법</p></item>
      </list>
    </synopsis>



  <links type="section"/>

  <section id="HelloWorld"><title>프로그램 만들기</title>

    <links type="section"/>

    <section id="imports"><title>가져올 라이브러리</title>
      <code mime="text/x-python">from gi.repository import Gtk
import sys</code>
      <p>우리가 작성한 스크립트가 그놈에서 동작하게 하려면, 그놈 라이브러리를 GObject 인트로스펙션으로 가져와야합니다. 여기서 우리는 언어 바인딩과 그놈 프로그램을 만들 때 그래픽 위젯이 들어있는 GTK+ 라이브러리를 임포팅합니다.</p>
    </section>

    <section id="mainwindow"><title>프로그램 메인 창 만들기</title>
      <code mime="text/x-python">class MyWindow(Gtk.ApplicationWindow):

    # constructor for a Gtk.ApplicationWindow
    def __init__(self, app):
        Gtk.Window.__init__(self, title="Hello World!", application=app)
        self.set_default_size(200, 100)

class MyApplication(Gtk.Application):
    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)</code>

    <p>Gtk.Application은 GTK+를 초기화합니다. 또한 창을 만들 때 자동으로 붙인 <gui>x</gui> 단추를 "destroy" 시그널에 연결합니다.</p>
    <p>첫 창 만들기를 시작할 수 있습니다. <var>MyWindow</var> 클래스를 만들고 Gtk.ApplicationWindow를 할당하겠습니다.</p>
    <p>창에 <var>title</var> 속성을 설정해야합니다. 제목은 원하는대로 지을 수 있습니다. 안전한 방편으로, UTF-8 인코딩으로 작성하시는게 좋습니다.</p>
    <p>이제 제목과 동작하는 "닫기" 단추가 붙은 창을 만들었습니다. 이제 실제 "Hello World" 문구를 찍어보겠습니다.</p>
    </section>

    <section id="label"><title>창 레이블</title>
      <code mime="text/x-python"># Add a label widget to your window

        # create a label
        label = Gtk.Label()

        # set the text of the label
        label.set_text("Hello GNOME!")

        # add the label to the window
        self.add(label)</code>

      <p>텍스트 레이블은 가져온 GTK+ 라이브러리에 들어있어 우리가 활용할 수 있는 GTK+ 위젯 중 하나입니다. 이 위젯을 사용하려, <var>label</var> 변수를 만들고, 계속 표시해 둘 텍스트를 설정하겠습니다. 최종적으로, 프로그램을 만들고 실행합니다:</p>

      <code mime="text/x-python">#run the application

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)</code>

      <p>Gtk.ApplicationWindow는 한번에 위젯 하나만 가질 수 있습니다. 프로그램을 더 정교하게 만들려면 Gtk.Grid 같은 홀더 위젯을 창 안에 만들어 넣고 그 안에 위젯을 추가해야합니다.</p>
   </section>


    <section id="py"><title>hello-world.py</title>
      <p>완전한 파일 내용:</p>
      <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):
    # constructor for a Gtk.ApplicationWindow

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Welcome to GNOME", application=app)
        self.set_default_size(200, 100)

        # create a label
        label = Gtk.Label()
        # set the text of the label
        label.set_text("Hello GNOME!")
        # add the label to the window
        self.add(label)


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
    </section>

    <section id="terminal"><title>터미널에서 프로그램 실행</title>
      <p>이 프로그램을 실행하려면 우선 hello-world.py로 저장하십시오. 그 다음 터미널을 여시고 프로그램을 저장한 폴더로 이동하신 다음 실행하십시오:</p>
      <screen><output style="prompt">$ </output><input>python hello-world.py</input></screen>
    </section>
  </section>

  <section id="desktop.in"><title><file>.desktop.in</file> 파일</title>
      <p>터미널에서의 프로그램 실행은 프로그램을 처음 만드는 단계에서 상당히 유용합니다. 그놈 3에서 완벽하게 <link href="https://developer.gnome.org/integration-guide/stable/mime.html.en">프로그램 통합</link>하여 동작할 수 있게 하려면 데스크톱 실행 아이콘이 필요합니다. 이 아이콘을 만들려면 <file>.desktop</file> 파일을 만들어야합니다.  파일은 프로그램 이름, 사용 아이콘, 다양한 통합 부분을 서술합니다. <file>.desktop</file> 파일에 대한 더 자세한 관점은 <link href="http://developer.gnome.org/desktop-entry-spec/">여기</link>에 있습니다. <file>.desktop.in</file> 파일로 <file>.desktop</file> 파일을 만듭니다.</p>

    <p>예제에서는 <code>.desktop.in</code> 파일에서 최소한 필요한 내용을 보여줍니다.</p>
    <code mime="text/desktop" style="numbered">[Desktop Entry]
Version=1.0
Encoding=UTF-8
Name=Hello World
Comment=Say Hello
Exec=@prefix@/bin/hello-world
Icon=application-default-icon
Terminal=false
Type=Application
StartupNotify=true
Categories=GNOME;GTK;Utility;
</code>

    <p>이제 <code>.desktop.in</code> 파일 부분으로 들어가보겠습니다.</p>
    <terms>
      <item><title>Name</title><p>프로그램 이름입니다.</p></item>
      <item><title>Comment</title><p>프로그램의 간단한 설명입니다.</p></item>
      <item><title>Exec</title><p>메뉴에서 프로그램을 선택했을 때 실행할 명령을 지정합니다. 이 예제에서는 <file>hello-world</file> 파일을 어디서 찾는지 알려주며 파일을 다루는 방식은 나머지 부분에서 다룹니다.</p></item>
      <item><title>Terminal</title><p>Exec 키의 명령을 터미널 창에서 실행할 지 여부를 지정합니다.</p></item>
    </terms>

    <p>프로그램을 적당한 분류에 넣으려면 Categories 줄에 필요한 분류 이름을 추가해야합니다. 다른 분류에 대한 자세한 정보는 <link href="http://standards.freedesktop.org/menu-spec/latest/apa.html">메뉴 명세</link>에 있습니다.</p>
    <p>In this example we use an existing icon. For a custom icon you need to have a .svg file of your icon, stored in <file>/usr/share/icons/hicolor/scalable/apps</file>. Write the name of your icon file to the .desktop.in file, on line 7. More information on icons in: <link href="https://wiki.gnome.org/Initiatives/GnomeGoals/AppIcon">Installing Icons for Themes</link> and <link href="http://freedesktop.org/wiki/Specifications/icon-theme-spec">on freedesktop.org: Specifications/icon-theme-spec</link>.</p>
  </section>

  <section id="autotools"><title>빌드 시스템</title>
    <p>그놈 3의 일부 프로그램을 만들려면 autotools의 도움을 받아 설치해야합니다. autotools 빌드는 필요한 모든 파일을 모든 올바른 경로에 설치합니다.</p>
    <p>진행하려면 다음 파일이 필요합니다:</p>
    <links type="section"/>

      <section id="autogen"><title>autogen.sh</title>
        <code mime="application/x-shellscript" style="numbered">#!/bin/sh

set -e

test -n "$srcdir" || srcdir=`dirname "$0"`
test -n "$srcdir" || srcdir=.

olddir=`pwd`
cd "$srcdir"

# This will run autoconf, automake, etc. for us
autoreconf --force --install

cd "$olddir"

if test -z "$NOCONFIGURE"; then
  "$srcdir"/configure "$@"
fi
</code>

      <p><file>autogen.sh</file> 파일을 준비했고 저장하고 나면, 다음을 실행하십시오:</p>
      <screen><output style="prompt">$ </output><input>chmod +x autogen.sh</input></screen>
    </section>


    <section id="makefile"><title>Makefile.am</title>
      <code mime="application/x-shellscript" style="numbered"># The actual runnable program is set to the SCRIPTS primitive.
# # Prefix bin_ tells where to copy this
bin_SCRIPTS = hello-world.py
# # List of files to be distributed
EXTRA_DIST=  \
	$(bin_SCRIPTS)
#
#     # The desktop files
desktopdir = $(datadir)/applications
desktop_DATA = \
	hello-world.desktop
</code>
    </section>


    <section id="configure"><title>configure.ac</title>
      <code mime="application/x-shellscript" style="numbered"># This file is processed by autoconf to create a configure script
AC_INIT([Hello World], 1.0)
AM_INIT_AUTOMAKE([1.10 no-define foreign dist-xz no-dist-gzip])
AC_CONFIG_FILES([Makefile hello-world.desktop])
AC_OUTPUT
</code>
    </section>


    <section id="readme"><title>README</title>
       <p>사용자가 우선 읽어야 할 내용입니다. 이 파일은 비워둘 수 있습니다.</p>

       <p>올바른 내용을 넣고 권한을 설정한 <file>hello-world</file>, <file>hello-world.desktop.in</file>, <file>Makefile.am</file>, <file>configure.ac</file>, <file>autogen.sh</file> 파일을 갖췄다면, <file>README</file> 파일에 다음 내용을 넣을 수 있습니다:</p>
      <code mime="text/readme" style="numbered">To build and install this program:

./autogen.sh --prefix=/home/your_username/.local
make install

-------------
Running the first line above creates the following files:

aclocal.m4
autom4te.cache
config.log
config.status
configure
hello-world.desktop
install-sh
missing
Makefile.in
Makefile

Running "make install", installs the application in /home/your_username/.local/bin
and installs the hello-world.desktop file in /home/your_username/.local/share/applications

You can now run the application by typing "Hello World" in the Overview.

----------------
To uninstall, type:

make uninstall

----------------
To create a tarball type:

make distcheck

This will create hello-world-1.0.tar.xz
</code>
    </section>

    <!-- TODO: How to make a custom icon with autotools -->

  </section>
</page>
