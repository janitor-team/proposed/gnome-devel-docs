<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="buttonbox.py" xml:lang="ko">
  <info>
    <title type="text">ButtonBox (Python)</title>
    <link type="guide" xref="beginner.py#layout"/>
    <link type="seealso" xref="button.py"/>
    <link type="next" xref="statusbar.py"/>
    <revision version="0.2" date="2012-08-01" status="stub"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>단추를 정렬하는 컨테이너</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2017-2019.</mal:years>
    </mal:credit>
  </info>

  <title>ButtonBox</title>

  <media type="image" mime="image/png" src="media/buttonbox_calculator.png"/>
  <p>계산기 - 단추는 수평 ButtonBox에 둘러싸여있습니다.</p>

  <links type="section"/>

  <section id="code">
    <title>예제 결과를 만드는 코드</title>
    <code mime="text/python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Calculator", application=app)
        self.set_default_size(350, 200)
        self.set_border_width(10)

        # an entry
        self.entry = Gtk.Entry()
        # with an initial text
        self.entry.set_text('0')
        # text aligned on the right
        self.entry.set_alignment(1)
        # the text in the entry cannot be modified writing in it
        self.entry.set_can_focus(False)

        # a grid
        grid = Gtk.Grid()
        grid.set_row_spacing(5)

        # to attach the entry
        grid.attach(self.entry, 0, 0, 1, 1)

        # the labels for the buttons
        buttons = [7, 8, 9, '/',
                   4, 5, 6, '*',
                   1, 2, 3, '-',
                   'C', 0, '=', '+']

        # each row is a ButtonBox, attached to the grid
        for i in range(4):
            hbox = Gtk.ButtonBox.new(Gtk.Orientation.HORIZONTAL)
            hbox.set_spacing(5)
            grid.attach(hbox, 0, i + 1, 1, 1)
            # each ButtonBox has 4 buttons, connected to the callback function
            for j in range(4):
                button = Gtk.Button(label=buttons[i * 4 + j])
                button.set_can_focus(False)
                button.connect("clicked", self.button_clicked)
                hbox.add(button)

        # some variables for the calculations
        self.first_number = 0
        self.second_number = 0
        self.counter = 0
        self.operation = ""

        # add the grid to the window
        self.add(grid)

    # callback function for all the buttons
    def button_clicked(self, button):
        # for the operations
        if button.get_label() == '+':
            self.counter += 1
            if self.counter &gt; 1:
                self.do_operation()
            self.entry.set_text('0')
            self.operation = "plus"
        elif button.get_label() == '-':
            self.counter += 1
            if self.counter &gt; 1:
                self.do_operation()
            self.entry.set_text('0')
            self.operation = "minus"
        elif button.get_label() == '*':
            self.counter += 1
            if self.counter &gt; 1:
                self.do_operation()
            self.entry.set_text('0')
            self.operation = "multiplication"
        elif button.get_label() == '/':
            self.counter += 1
            if self.counter &gt; 1:
                self.do_operation()
            self.entry.set_text('0')
            self.operation = "division"
        # for =
        elif button.get_label() == '=':
            self.do_operation()
            self.entry.set_text(str(self.first_number))
            self.counter = 1
        # for Cancel
        elif button.get_label() == 'C':
            self.first_number = 0
            self.second_number = 0
            self.counter = 0
            self.entry.set_text('')
            self.operation = ""
        # for a digit button
        else:
            new_digit = int(button.get_label())
            if self.entry.get_text() == 'error':
                number = 0
            else:
                number = int(self.entry.get_text())
            number = number * 10 + new_digit
            if self.counter == 0:
                self.first_number = number
            else:
                self.second_number = number
            self.entry.set_text(str(number))

    def do_operation(self):
        if self.operation == "plus":
            self.first_number += self.second_number
        elif self.operation == "minus":
            self.first_number -= self.second_number
        elif self.operation == "multiplication":
            self.first_number *= self.second_number
        elif self.operation == "division":
            try:
                self.first_number /= self.second_number
            except ZeroDivisionError:
                self.first_number = 0
                self.second_number = 0
                self.counter = 0
                self.entry.set_text('error')
                self.operation = ""
                return
        else:
            self.first_number = 0
            self.second_number = 0
            self.counter = 0
            self.entry.set_text('error')


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
    <title>ButtonBox 위젯에 쓸만한 메서드</title>
    <list>
      <item><p>ButtonBox 배치는 <code>set_layout(layout)</code> 함수로 설정하며, <code>layout</code>에 들어갈 값은 <code>Gtk.ButtonBoxStyle.SPREAD</code>(단추가 상자 크기만큼 늘어남), <code>Gtk.ButtonBoxStyle.EDGE</code>(단추가 상자 가장자리에 붙음), <code>Gtk.ButtonBoxStyle.START</code>(단추가 상자 시작 부분에 모임), <code>Gtk.ButtonBoxStyle.END</code>(단추가 상자 끝부분에 모임), <code>Gtk.ButtonBoxStyle.CENTER</code>(단추가 상자 한가운데로 모임)가 될 수 있습니다.</p></item>
      <item><p><code>set_child_secondary(button, is_secondary)</code> 함수로 <code>button</code> 이 하위 두번째 모임으로 나타날지 여부를 설정합니다. 2차 하위요소는 대화상자의 도움말 단추가 될 수 있습니다. 이 모임은 모양새가 <code>START</code>, <code>SPREAD</code>, <code>EDGE</code> 중 하나고, 그 이전 하위 요소의 모양새가 <code>END</code>일 때, 다른 하위 요소의 전에 나타납니다. 모양새가 <code>START</code>, <code>END</code> 중 하나면, 주 하위 요소의 단추 상자 다른 끝부분에 두번째 하위 요소를 정렬합니다. 다른 모양새로 설정하면, 주 하위 요소 바로 다음에 나타냅니다.</p></item>
      <item><p><code>set_child_non_homogeneous(button, is_non_homogeneous)</code> 함수로 하위 요소에 대한 동일 크기 조절 방식 비적용 여부를 설정합니다. 기본 값은 <code>false</code>입니다.</p></item>
      <item><p><code>set_spacing(spacing)</code> 함수는 상자안의 단추 사이 간격을 픽셀 단위로 설정합니다.</p></item>
    </list>
  </section>

  <section id="references">
    <title>API 참고서</title>
    <p>이 예제는 다음 참고자료가 필요합니다:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkButtonBox.html">GtkButtonBox</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkBox.html">GtkBox</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkButton.html">GtkButton</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkEntry.html">GtkEntry</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkGrid.html">GtkGrid</link></p></item>
    </list>
  </section>
</page>
