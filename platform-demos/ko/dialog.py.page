<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="dialog.py" xml:lang="ko">
  <info>
    <title type="text">Dialog(Python)</title>
    <link type="guide" xref="beginner.py#windows"/>
    <link type="seealso" xref="signals-callbacks.py"/>
    <link type="next" xref="aboutdialog.py"/>
    <revision version="0.1" date="2012-06-11" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>뜨는 창</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2017-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Dialog</title>
  <media type="image" mime="image/png" src="media/dialog.png"/>
  <p>콜백 함수에 응답 시그널을 연결한 대화상자입니다.</p>

  <links type="section"/>

  <section id="code">
  <title>예제 결과를 만드는 코드</title>

  <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):
    # construct a window (the parent window)

    def __init__(self, app):
        Gtk.Window.__init__(self, title="GNOME Button", application=app)
        self.set_default_size(250, 50)

        # a button on the parent window
        button = Gtk.Button("Click me")
        # connect the signal "clicked" of the button with the function
        # on_button_click()
        button.connect("clicked", self.on_button_click)
        # add the button to the window
        self.add(button)

    # callback function for the signal "clicked" of the button in the parent
    # window
    def on_button_click(self, widget):
        # create a Gtk.Dialog
        dialog = Gtk.Dialog()
        dialog.set_title("A Gtk+ Dialog")
        # The window defined in the constructor (self) is the parent of the dialog.
        # Furthermore, the dialog is on top of the parent window
        dialog.set_transient_for(self)
        # set modal true: no interaction with other windows of the application
        dialog.set_modal(True)
        # add a button to the dialog window
        dialog.add_button(button_text="OK", response_id=Gtk.ResponseType.OK)
        # connect the "response" signal (the button has been clicked) to the
        # function on_response()
        dialog.connect("response", self.on_response)

        # get the content area of the dialog, add a label to it
        content_area = dialog.get_content_area()
        label = Gtk.Label("This demonstrates a dialog with a label")
        content_area.add(label)
        # show the dialog
        dialog.show_all()

    def on_response(self, widget, response_id):
        print("response_id is", response_id)
        # destroy the widget (the dialog) when the function on_response() is called
        # (that is, when the button of the dialog has been clicked)
        widget.destroy()


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>

  </section>

  <section id="methods">
  <title>Dialog 위젯에 쓸만한 메서드</title>
    <p>16번째 줄에서, <code><var>widget</var>.connect(<var>signal</var>, <var>callback function</var>)</code> 코드로  <code>"clicked"</code> 시그널을 <code>on_button_click()</code> 콜백 함수에 연결했습니다. 더 자세한 설명은 <link xref="signals-callbacks.py"/>를 참조하십시오.</p>
  <list>
    <item><p><code>set_modal(True)</code> 대신 <code>set_modal(False)</code>로 호출하고, 그 뒤에 <code>set_destroy_with_parent(True)</code> 함수를 호출해서 메인 창을 닫으면 대화상자 창을 닫을 수 있게 할 수 있습니다.</p></item>
    <item><p>임의의 정수 값 <code>42</code>를 갖는 <code>add_button(button_text="The Answer", response_id=42)</code> 대신, <code>RESPONSE</code> 자리에 <code>-5, -6,..., -11</code> 정수형 값에 해당하는 <code>OK, CANCEL, CLOSE, YES, NO, APPLY, HELP</code>중 하나가 오는 <code>add_button(button_text="text", response_id=Gtk.ResponseType.RESPONSE)</code> 호출 방식을 쓸 수 있습니다.</p></item>
  </list>
  </section>

  <section id="references">
  <title>API 참고서</title>
  <p>이 예제는 다음 참고자료가 필요합니다:</p>
  <list>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkDialog.html">GtkDialog</link></p></item>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkWindow.html">GtkWindow</link></p></item>
  </list>
  </section>
</page>
