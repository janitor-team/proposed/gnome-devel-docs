<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="linkbutton.py" xml:lang="ko">
  <info>
    <title type="text">LinkButton (Python)</title>
    <link type="guide" xref="beginner.py#buttons"/>
    <link type="next" xref="checkbutton.py"/>
    <revision version="0.1" date="2012-05-23" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>URL에 연결하는 단추</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2017-2019.</mal:years>
    </mal:credit>
  </info>

  <title>LinkButton</title>

  <media type="image" mime="image/png" src="media/linkbutton.png"/>
  <p>웹페이지로 연결한 단추입니다.</p>

  <links type="section"/>

  <section id="code">
    <title>예제 결과를 만드는 코드</title>

    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):
    # a window

    def __init__(self, app):
        Gtk.Window.__init__(self, title="GNOME LinkButton", application=app)
        self.set_default_size(250, 50)

        # a linkbutton pointing to the given URI
        button = Gtk.LinkButton(uri="http://live.gnome.org")
        # with given text
        button.set_label("Link to GNOME live!")

        # add the button to the window
        self.add(button)


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>

  </section>
  <section id="methods">
    <title>LinkButton 위젯에 쓸만한 메서드</title>
    <list>
      <item><p><code>get_visited()</code> 함수는 LinkButton이 가리키는 URI의 'visited' 상태(<code>True</code> 또는 <code>False</code>)를 반환합니다. 단추를 누르면 visited 상태가 됩니다.</p></item>
      <item><p><code>set_visited(True)</code> 함수는 LinkButton을 <code>True</code>(<code>False</code> 도 비슷하게).로 설정했을 경우 URI의 'visited' 상태를 설정합니다</p></item>
      <item><p>단추를 누를 때마다 <code>"activate-link"</code> 시그널을 내보냅니다. 시그널과 콜백 함수를 설명하는 내용을 보려면 <link xref="signals-callbacks.py"/>를 참고하십시오.</p></item>
    </list>
  </section>
  <section id="references">
    <title>API 참고서</title>
    <p>이 예제는 다음 참고자료가 필요합니다:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkWindow.html">GtkWindow</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkLinkButton.html">GtkLinkButton</link></p></item>
    </list>
  </section>
</page>
