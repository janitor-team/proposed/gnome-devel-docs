<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="image-viewer.cpp" xml:lang="ko">

  <info>
    <link type="guide" xref="cpp#examples"/>

    <desc>간단한 "Hello world" Gtkmm 프로그램에 약간의 무언가가 더 들어갑니다.</desc>

    <revision pkgversion="0.1" version="0.1" date="2011-03-18" status="review"/>
    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Johannes Schmid</name>
      <email its:translate="no">jhs@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2013</years>
    </credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2017-2019.</mal:years>
    </mal:credit>
  </info>

<title>그림 보기</title>

<synopsis>
  <p>이 지침서를 통해 다음을 배웁니다:</p>
  <list>
    <item><p>C++/GObject 프로그래밍 일부 기본 개념</p></item>
    <item><p>C++ 프로그래밍 언어로 Gtk 프로그램을 작성하는 방법</p></item>
  </list>
</synopsis>

<media type="image" mime="image/png" src="media/image-viewer.png"/>

<section id="anjuta">
  <title>안주타에서 프로젝트 만들기</title>
  <p>코딩을 시작하기 전에 안주타에서 새 프로젝트를 설정해야합니다. 이 프로그램은 빌드에 필요한 모든 파일을 만들고 그 다음 코드를 실행합니다. 또한 이 모든 상태를 유지 관리하는데 쓸만합니다.</p>
  <steps>
    <item>
    <p>안주타를 시작하고 <guiseq><gui>파일</gui><gui>새로 만들기</gui><gui>프로젝트</gui></guiseq> 를 눌러 프로젝트 마법사를 여십시오.</p>
    </item>
    <item>
    <p><gui>C++</gui> 탭에서 <gui>GTKmm (단순)</gui>을 선택하고, <gui>계속</gui>을 누른 다음, 나타난 페이지에서 몇가지 자세한 내용을 입력하십시오. 프로젝트 이름과 디렉터리에 <file>image-viewer</file>를 입력하십시오.</p>
   	</item>
    <item>
    <p>앞서 따라하기 지침을 통해 사용자 인터페이스를 직접 만들 예정이므로 <gui>사용자 인터페이스에 GtkBuilder 사용</gui> 설정을 껐는지 확인하십시오. 인터페이스 빌더 사용법을 알아보려면 <link xref="guitar-tuner.cpp">기타 조율 프로그램</link> 따라하기 지침서를 확인하십시오.</p>
    </item>
    <item>
    <p><gui>적용</gui>을 누르면 프로젝트를 만들어줍니다. <gui>프로젝트</gui>나 <gui>파일</gui>탭에서 <file>src/main.cc</file> 파일을 여십시오. 다음 줄로 시작하는 일부 코드를 볼 수 있습니다:</p>
    <code mime="text/x-csrc">
#include &lt;gtkmm.h&gt;
#include &lt;iostream&gt;

#include "config.h"&gt;</code>
    </item>
  </steps>
</section>

<section id="build">
  <title>첫 코드 작성</title>
  <p>이 부분은 GTKmm을 구성한 매우 간단한 C++ 코드입니다. 더 자세한 내용은 아래를 보십시오. 기본을 이해하고 있다면 이 부분을 건너 뛰십시오:</p>
  <list>
  <item>
    <p>상단의 <code>#include</code> 세 줄은 <code>config</code> (쓸만한 autoconf 빌드 정의), <code>gtkmm</code> (사용자 인터페이스), <code>iostream</code> (C++-STL) 라이브러리입니다. 이 라이브러리로부터 온 함수는 코드 나머지 부분에서 활용합니다.</p>
   </item>
   <item>
    <p><code>main</code> 함수는 (비어있는) 새 창을 만들고 창 제목을 설정합니다.</p>
   </item>
   <item>
    <p><code>kit::run()</code> 호출은 GTKmm 메인 루프를 시작하며, 메인 루프에서는 사용자 인터페이스를 실행하고 이벤트(마우스 단추 누름 키보드 키 누름) 대기를 시작합니다. 창을 해당 함수에 인자로 전달하여, 창을 닫을 때 프로그램에서 자동으로 빠져나갑니다.</p>
   </item>
  </list>

  <p>이 코드를 사용할 준비가 됐으니 <guiseq><gui>빌드</gui><gui>프로젝트 빌드</gui></guiseq>(또는 <keyseq><key>Shift</key><key>F7</key></keyseq> 키 누름)를 눌러 코드를 컴파일할 수 있습니다.</p>
  <p>디버깅 빌드 설정이 나타나는 다음 창에서 <gui>실행</gui> 을 누르십시오. 처음 빌드할 때 한번만 하면 됩니다.</p>
</section>

<section id="ui">
<title>사용자 인터페이스 만들기</title>
<p>이제 비어있는 창에 숨결을 불어넣겠습니다. GTKmm에는 다른 위젯을 넣을 수 있고 다른 컨테이너도 넣을 수 있는 <code>Gtk::Container</code>로  사용자 인터페이스를 모아둡니다. 여기서는 여러가지 컨테이너 중 가장 간단한 <code>Gtk::Box</code>를 사용하겠습니다:</p>
<code mime="text/x-csrc">
int
main (int argc, char *argv[])
{
	Gtk::Main kit(argc, argv);

	Gtk::Window main_win;
	main_win.set_title ("image-viewer-cpp");

	Gtk::Box* box = Gtk::manage(new Gtk::Box());
	box-&gt;set_orientation (Gtk::ORIENTATION_VERTICAL);
	box-&gt;set_spacing(6);
	main_win.add(*box);

	image = Gtk::manage(new Gtk::Image());
	box-&gt;pack_start (*image, true, true);

	Gtk::Button* button = Gtk::manage(new Gtk::Button("Open Image…"));
	button-&gt;signal_clicked().connect (
		sigc::ptr_fun(&amp;on_open_image));
	box-&gt;pack_start (*button, false, false);

	main_win.show_all_children();
	kit.run(main_win);

	return 0;
}
</code>
  <steps>
    <item>
    <p>첫번째 줄에서는 우리가 쓰려는 위젯을 만듭니다. 그림을 여는 단추, 그림 보기 위젯 자체, 컨테이너로 쓸 상자 위젯이 있습니다.</p>
    </item>
    <item>
    <p><code>pack_start</code> 호출로 박스에 두 위젯을 추가하고 동작을 정의합니다. 그림은 활용 가능한 공간만큼 충분히 확장하고, 단추는 필요한 만큼만 커집니다. 위젯의 크기를 분명하게 설정하지 않음을 알아채셨을 겁니다. GTKmm에서는 창 크기가 달라져도 보기 좋은 배치를 쉽게하기에 직접 설정할 필요가 없습니다. 이 과정이 끝나면 박스를 창에 추가합니다.</p>
    </item>
    <item>
    <p>단추를 사용자가 눌렀을 때 어떤 일이 일어날 지 정해야합니다. GTKmm에서는 <em>시그널</em> 개념을 활용합니다. 단추를 누르면, 어떤 동작과 연결할 수 있는 <em>clicked</em> 시그널을 방출합니다. 사용자가 단추를 누르면 <code>on_image_open</code> 함수를 호출하여 그림을 이 함수의 인자로 전달하라고 GTK에 지시할 때 <code>signal_clicked().connect</code> 메서드를 사용하면 됩니다. 다음 섹션에서 <em>콜백</em>을 정의하겠습니디.</p>
    </item>
    <item>
    <p>마지막 단계에서는 <code>show_all_children()</code> 함수로 창의 모든 위젯을 나타냅니다. 이는 모든 하우 위젯을 나타내는 <code>show()</code> 메서드의 활용법과 같습니다.</p>
    </item>
  </steps>
</section>

<section id="show">
<title>그림 표시</title>
<p>이제 <em>clicked</em> 시그널이나 위에서 언급한 단추에 대한 시그널 핸들러를 정했습니다. 이 코드는 <code>main</code> 메서드 전에 추가하십시오.</p>
<code mime="text/x-csrc">
Gtk::Image* image = 0;

static void
on_open_image ()
{
	Gtk::FileChooserDialog dialog("Open image",
	                              Gtk::FILE_CHOOSER_ACTION_OPEN);
	dialog.add_button (Gtk::Stock::OPEN,
	                   Gtk::RESPONSE_ACCEPT);
	dialog.add_button (Gtk::Stock::CANCEL,
	                   Gtk::RESPONSE_CANCEL);

	Glib::RefPtr&lt;Gtk::FileFilter&gt; filter =
		Gtk::FileFilter::create();
	filter-&gt;add_pixbuf_formats();
	filter-&gt;set_name("Images");
	dialog.add_filter (filter);

	const int response = dialog.run();
	dialog.hide();

	switch (response)
	{
		case Gtk::RESPONSE_ACCEPT:
			image-&gt;set(dialog.get_filename());
			break;
		default:
			break;
	}
}
</code>
  <p>지금까지 우리가 다루어왔던 어떤 코드보다 조금 복잡하니 하나씩 뜯어보도록 하겠습니다:</p>
  <list>
      <item>
      <p>파일을 선택하는 대화상자를 <code>Gtk::FileChooserDialog</code> 생성자로 만들었습니다. 이 생성자는 제목과 대화상자 형식을 취합니다. 지금 같은 경우는 <em>열기</em> 대화상자입니다.</p>
    </item>
    <item>
    <p>다음 두 줄은 대화상자의 <em>열기</em>, <em>닫기</em> 단추를 추가합니다.</p>
    <p>참고로 "취소", "열기"를 직접 입력하는 대신, Gtk에서 <em>stock</em> 단추 이름을 활용합니다  스톡 이름은 사용자가 쓰는 언어로 미리 번역해둔 단추 레이블을 활용한다는 점입니다.</p>
    <p><code>add_button()</code> 메서드의 두번째 인자는 누른 단추를 식별하는 값입니다. GTKmm에서 주는 기 정의값을 사용했습니다.</p>
    </item>
    <item>
    <p>다음 두 줄은 <gui>열기</gui> 대화 상자에서 <code>Gtk::Image</code>로만 열 수 있는 파일을 표시하도록 제한합니다. 필터 객체를 우선 만듭니다. 그 다음 <code>Gdk::Pixbuf</code>에서 지원하는 파일 종류(PNG와 JPEG 같은 대부분의 그림 형식)를 필터에 추가합니다. 마지막으로 이 필터를 <gui>열기</gui> 대화 상자의 필터로 설정합니다.</p>
    <p><code>Glib::RefPtr</code>는 여기서 사용한 지능형 포인터로, 더 이상 참조할 코드가 없을 때, 필터를 해체했는지 확인합니다.</p>
    </item>
    <item>
    <p><code>dialog.run</code>은 <gui>열기</gui> 대화 상자를 표시합니다. 대화 상자는 사용자의 그림 선택을 기다립니다. 사용자가 그림을 선택하면 <code>dialog.run</code> 에서 <code>Gtk::RESPONSE_ACCEPT</code> 값을 반환합니다(<gui>취소</gui>를 누르면 <code>Gtk::RESPONSE_CANCEL</code> 값을 반환합니다). <code>switch</code> 구문에서는 어떤 값을 반환했는지 확인합니다.</p>
    </item>
    <item>
    <p>이 메서드의 마지막 줄에서는 <gui>열기</gui> 대화상자는 더 이상 필요 없으니 해체합니다.  지역 변수로서 실행 범위가 끝나면 나중에 해체하므로(숨김) 대화 상자는 여하튼 나중에 숨길 수도 있습니다.</p>
    </item>
    <item><p>사용자가 <gui>열기</gui>를 눌렀다고 한 상황에서, 다음 줄에서 파일을 <code>Gtk::Image</code>에 불러와서 화면에 표시합니다.</p>
    </item>
  </list>
</section>

<section id="build2">
  <title>프로그램 빌드 및 실행</title>
  <p>모든 코드를 실행할 준비가 됐습니다. <guiseq><gui>빌드</gui><gui>프로젝트 빌드</gui></guiseq>를 눌러 프로젝트 전체를 다시 빌드하고, <guiseq><gui>실행</gui><gui>실행</gui></guiseq>을 눌러 프로그램을 시작하십시오.</p>
  <p>아직 끝나지 않았다면, 화면에 나타나는 대화상자에서 <file>Debug/src/image-viewer</file> 프로그램을 선택하십시오. 마지막으로 <gui>실행</gui>을 누르고 즐기세요!</p>
</section>

<section id="impl">
 <title>참조 구현체</title>
 <p>지침서를 따라하는 실행하는 과정에 문제가 있다면, <link href="image-viewer/image-viewer.cc">참조 코드</link>와 여러분의 코드를 비교해보십시오.</p>
</section>

<section id="next">
  <title>다음 단계</title>
  <p>여기 간단한 시험 프로그램에 여러분이 추가로 넣을 수 있는 몇가지 아이디어가 있습니다:</p>
  <list>
   <item>
   <p>파일을 선택하기 보단 디렉터리를 선택하게 하고, 디렉터리의 모든 그림을 보여줄 수 있는 컨트롤을 제어 기능을 제공합니다.</p>
   </item>
   <item>
   <p>사용자가 그림을 불러오고 수정한 그림을 저장할 때 임의 필터와 효과를 그림에 적용하십시오.</p>
   <p><link href="http://www.gegl.org/api.html">GEGL</link>에서는 강력한 그림 편집 기능을 제공합니다.</p>
   </item>
   <item>
   <p>네트워크 공유, 스캐너, 다른 복잡한 공급원에서 그림을 불러올 수 있습니다.</p>
   <p>네트워크 파일 전송 같은 동작을 처리할 때 <link href="http://library.gnome.org/devel/gio/unstable/">GIO</link>를 사용할 수 있습니다. 그리고 스캐너를 다룰 때는 <link href="http://library.gnome.org/devel/gnome-scan/unstable/">그놈 스캔</link>을 활용하시면 됩니다.</p>
   </item>
  </list>
</section>


</page>
