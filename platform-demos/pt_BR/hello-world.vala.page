<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="hello-world.vala" xml:lang="pt-BR">

  <info>
  <title type="text">Hello World (Vala)</title>
    <link type="guide" xref="beginner.vala#tutorials" group="#first"/>

    <revision version="0.1" date="2013-06-17" status="review"/>

    <credit type="author copyright">
      <name>Susanna Huhtanen</name>
      <email its:translate="no">ihmis.suski@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
    </credit>

    <desc>A basic "hello, world" application</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2013, 2018</mal:years>
    </mal:credit>
  </info>

  <title>How to build, install and create a <file>tar.xz</file> of a Hello World program</title>
    <media type="image" mime="image/png" style="floatend" src="media/hello-world.png"/>
    <synopsis>
      <p>This tutorial will demonstrate how to:</p>
      <list style="numbered">
        <item><p>create a small "Hello, World" application using GTK+</p></item>
        <item><p>make the <file>.desktop</file> file</p></item>
        <item><p>how to set up the build system</p></item>
      </list>
    </synopsis>

  <links type="section"/>

  <section id="hello-world"><title>Create the program</title>

    <links type="section"/>

    <section id="application"><title>Creating the main window for the application</title>
      <code mime="text/x-csharp"><![CDATA[class MyApplication : Gtk.Application {
        protected override void activate () {
                var window = new Gtk.ApplicationWindow (this);
                window.set_title ("Welcome to GNOME");
                window.set_default_size (200, 100);
                window.show_all ();
        }
}]]></code>

    <p>Gtk.Application initializes GTK+. It also connects the <gui>x</gui> button that's automatically generated along with the window to the "destroy" signal.</p>
    <p>We can start building our first window. We do this by creating a variable called <var>window</var> and assigning it a new Gtk.ApplicationWindow.</p>
    <p>We give the window a title using <code>set_title</code>. The title can be any string you want it to be. To be on the safe side, it's best to stick to UTF-8 encoding.</p>
    <p>Now we have a window which has a title and a working "close" button. Let's add the actual "Hello World" text.</p>
    </section>

    <section id="label"><title>Label for the window</title>
      <code mime="text/x-csharp"><![CDATA[var label = new Gtk.Label ("Hello GNOME!");
                window.add (label);
]]></code>

      <p>Finally, we create and run the application:</p>

      <code mime="text/x-csharp"><![CDATA[int main (string[] args) {
        return new MyApplication ().run (args);
}]]></code>

      <p>Gtk.ApplicationWindow can only hold one widget at a time. To construct more elaborate programs you need to create a holder widget like Gtk.Grid inside the window, and then add all the other widgets to it.</p>
   </section>


    <section id="vala"><title>hello-world.vala</title>
      <p>The complete file:</p>
      <code mime="text/x-csharp" style="numbered">public class MyApplication : Gtk.Application {
	protected override void activate () {
		var window = new Gtk.ApplicationWindow (this);
		var label = new Gtk.Label ("Hello GNOME!");
		window.add (label);
		window.set_title ("Welcome to GNOME");
		window.set_default_size (200, 100);
		window.show_all ();
	}
}

public int main (string[] args) {
	return new MyApplication ().run (args);
}
</code>
    </section>

    <section id="terminal"><title>Running the application from terminal</title>
      <p>To run this application, first save it as hello-world.vala. Then open Terminal, go to the folder where your application is stored.</p>
      <p>Compile the program:</p>
           <screen>valac --pkg gtk+-3.0 <file>hello-world.vala</file></screen>
      <p>Run the program:</p>
           <screen>./<var>hello-world</var></screen>
    </section>
  </section>

  <section id="desktop.in"><title>The <file>.desktop.in</file> file</title>
      <p>Running applications from the Terminal is useful at the beginning of the application making process. To have fully working <link href="https://developer.gnome.org/integration-guide/stable/mime.html.en">application integration</link> in GNOME 3 requires a desktop launcher. For this you need to create a  <file>.desktop</file> file. The <file>.desktop</file> file describes the application name, the used icon and various integration bits. A deeper insight into the <file>.desktop</file> file can be found <link href="http://developer.gnome.org/desktop-entry-spec/">here</link>. The <file>.desktop.in</file> file will create the <file>.desktop</file>.</p>

    <p>The example shows you the minimum requirements for a <code>.desktop.in</code> file.</p>
    <code mime="text/desktop" style="numbered">[Desktop Entry]
Version=1.0
Encoding=UTF-8
Name=Hello World
Comment=Say Hello
Exec=@prefix@/bin/hello-world
Icon=application-default-icon
Terminal=false
Type=Application
StartupNotify=true
Categories=GNOME;GTK;Utility;
</code>

    <p>Now let's go through some parts of the <code>.desktop.in</code> file.</p>
    <terms>
      <item><title>Name</title><p>The application name.</p></item>
      <item><title>Comment</title><p>A short description of the application.</p></item>
      <item><title>Exec</title><p>Specifies a command to execute when you choose the application from the menu. In this example exec just tells where to find the <file>hello-world</file> file and the file takes care of the rest.</p></item>
      <item><title>Terminal</title><p>Specifies whether the command in the Exec key runs in a terminal window.</p></item>
    </terms>

    <p>To put your application into the appropriate category, you need to add the necessary categories to the Categories line. More information on the different categories can be found in the <link href="http://standards.freedesktop.org/menu-spec/latest/apa.html">menu specification</link>.</p>
    <p>In this example we use an existing icon. For a custom icon you need to have a .svg file of your icon, stored in <file>/usr/share/icons/hicolor/scalable/apps</file>. Write the name of your icon file to the .desktop.in file, on line 7. More information on icons in: <link href="https://wiki.gnome.org/Initiatives/GnomeGoals/AppIcon">Installing Icons for Themes</link> and <link href="http://freedesktop.org/wiki/Specifications/icon-theme-spec">on freedesktop.org: Specifications/icon-theme-spec</link>.</p>
  </section>

  <section id="autotools"><title>The build system</title>
    <p>To make your application truly a part of the GNOME 3 system you need to install it with the help of autotools. The autotools build will install all the necessary files to all the right places. </p>
    <p>For this you need to have the following files:</p>
    <links type="section"/>

      <section id="autogen"><title>autogen.sh</title>
        <code mime="application/x-shellscript" style="numbered">#!/bin/sh

set -e

test -n "$srcdir" || srcdir=`dirname "$0"`
test -n "$srcdir" || srcdir=.

olddir=`pwd`
cd "$srcdir"

# This will run autoconf, automake, etc. for us
autoreconf --force --install

cd "$olddir"

if test -z "$NOCONFIGURE"; then
  "$srcdir"/configure "$@"
fi
</code>

      <p>After the <file>autogen.sh</file> file is ready and saved, run:</p>
      <screen><output style="prompt">$ </output><input>chmod +x autogen.sh</input></screen>
    </section>


    <section id="makefile"><title>Makefile.am</title>
      <code mime="application/x-shellscript" style="numbered"># The actual runnable program is set to the SCRIPTS primitive.
# # Prefix bin_ tells where to copy this
bin_PROGRAMS = hello-world
hello_world_CFLAGS = $(gtk_CFLAGS)
hello_world_LDADD = $(gtk_LIBS)
hello_world_VALAFLAGS = --pkg gtk+-3.0
hello_world_SOURCES = hello-world.vala

desktopdir = $(datadir)/applications
desktop_DATA = \
	hello-world.desktop
</code>
    </section>


    <section id="configure"><title>configure.ac</title>
      <code mime="application/x-shellscript" style="numbered"># This file is processed by autoconf to create a configure script
AC_INIT([Hello World], 1.0)
AM_INIT_AUTOMAKE([1.10 no-define foreign dist-xz no-dist-gzip])
AC_PROG_CC
AM_PROG_VALAC([0.16])
PKG_CHECK_MODULES(gtk, gtk+-3.0)
AC_CONFIG_FILES([Makefile hello-world.desktop])

AC_OUTPUT
</code>
    </section>


    <section id="readme"><title>README</title>
       <p>Information users should read first. This file can be blank.</p>

       <p>When you have the <file>hello-world.c</file>, <file>hello-world.desktop.in</file>, <file>Makefile.am</file>, <file>configure.ac</file> and <file>autogen.sh</file> files with correct information and rights, the <file>README</file> file can include the following instructions:</p>
      <code mime="text/readme" style="numbered">To build and install this program:

./autogen.sh --prefix=/home/your_username/.local
make
make install

-------------
Running the first line above creates the following files:

aclocal.m4
autom4te.cache
config.log
config.status
configure
depcomp
hello-world
hello-world.c
hello-world.desktop
hello_world-hello-world.o
hello_world_vala.stamp
install-sh
missing
Makefile.in
Makefile

Running "make" links all the appropriate libraries.

Running "make install", installs the application in /home/your_username/.local/bin
and installs the hello-world.desktop file in /home/your_username/.local/share/applications

You can now run the application by typing "Hello World" in the Overview.

----------------
To uninstall, type:

make uninstall

----------------
To create a tarball type:

make distcheck

This will create hello-world-1.0.tar.xz
</code>
    </section>

    <!-- TODO: How to make a custom icon with autotools -->

  </section>
</page>
