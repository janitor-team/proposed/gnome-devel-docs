<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:e="http://projectmallard.org/experimental/" type="guide" style="task" id="signals-callbacks.py" xml:lang="cs">

<info>
  <title type="text">Signály a zpětná volání (Python)</title>
  <link type="guide" xref="beginner.py#theory"/>
  <link type="next" xref="button.py"/>
  <revision version="0.1" date="2012-06-16" status="draft"/>

  <desc>Vysvětlení signálů a zpětných volání v GTK+.</desc>
  <credit type="author copyright">
    <name>Sebastian Pölsterl</name>
    <email its:translate="no">sebp@k-d-w.org</email>
    <years>2011</years>
  </credit>
  <credit type="editor">
    <name>Marta Maria Casetti</name>
    <email its:translate="no">mmcasetti@gmail.com</email>
    <years>2012</years>
  </credit>
</info>

<title>Signály a zpětná volání</title>

<links type="section"/>

<section id="overview">
<title>Přehled</title>

<p>Podobně jako jiné nástrojové sady GUI, i GTK+ používá událostmi řízený model programování. Když uživatel nic nedělá, hoví si GTK+ v hlavní smyčce a čeká na vstup. Když uživatel provede nějakou činnost – řekněme, že klikne myší – tak se hlavní smyčka „probudí“ a doručí událost do GTK+.</p>

<p>Když událost obdrží widgety, nejčastěji vyšlou jeden nebo více signálů. Signály upozorní váš program, že „se něco zajímavého stalo“ tím, že vyvolají funkci, kterou jste na signál napojili. Tyto funkce jsou obecně známé jako zpětná volání. Jakmile je vaše zpětné volání vyvoláno, měli byste běžně něco udělat. Po dokončení zpětného volání se GTK+ vrátí do zpětné smyčky a čeká na další uživatelův vstup.</p>

<p>Obecný příklad: <code>handler_id = widget.connect("událost", zpětné_volání, data)</code>. <code>widget</code> je instance widgetu, který jsme vytvořili dříve. Následuje <code>událost</code>, která nás zajímá. Každý widget má své vlastní konkrétní události, které u něj mohou nastat. Například, když máte <code>Gtk.Button</code>, obvykle chcete napojit událost <code>"clicked"</code>: to znamená, že když je na tlačítko kliknuto, vyšle se signál. Jiným příkladem je signál <code>notify::property</code>: kdykoliv je u <code>GObject</code> změněna <link xref="properties.py">vlastnost</link>, tak místo prostého vyslání signálu <code>notify</code>, přiřadí <code>GObject</code> tomuto signálu doplňující údaj v podobě názvu změněné vlastnosti. To umožňuje klientovi, který si přeje být upozorňován na změny jen u jedné vlastnosti, odfiltrovat většinu událostí dříve, než k němu dorazí. Zatřetí, argument <code>zpětné_volání</code> je název funkce zpětného volání, která obsahuje kód, co se má spustit při vyslání signálu zadaného typu. A nakonec, volitelný argument <code>data</code> obsahuje libovolná data, která by měla být předána při vyslání signálu.</p>

<p>Funkce vrací číslo (<code>handler_id</code>), které identifikuje tento konkrétní pár signál/zpětné volání. Toto číslo je požadováno při odpojení signálu v podobě <code>widget.disconnect(handler_id)</code>. Způsobí to, že takováto funkce zpětného volání již nebude nadále volána v budoucnu nebo při právě probíhajícím vysílání signálu, který byl na ni napojen.</p>

</section>

<section id="references">

<title>Odkazy</title>
<p><link href="http://developer.gnome.org/gobject/stable/signal.html">Signály</link> v dokumentaci k GObject</p>
<p><link href="http://python-gtk-3-tutorial.readthedocs.org/en/latest/basics.html">Základy – Hlavní smyčka a signály</link> ve výuce GTK+ 3 v jazyce Python</p>
</section>


</page>
