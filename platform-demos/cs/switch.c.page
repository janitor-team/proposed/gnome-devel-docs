<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="switch.c" xml:lang="cs">
  <info>
    <title type="text">Switch (C)</title>
    <link type="guide" xref="c#buttons"/>
    <revision version="0.1" date="2012-06-01" status="draft"/>

    <credit type="author copyright">
      <name>Monica Kochofar</name>
      <email its:translate="no">monicakochofar@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Přepínač ve stylu „posuvného vypínače“</desc>
  </info>

  <title>Switch</title>

  <media type="image" mime="image/png" style="floatend" src="media/switch_off.png"/>
  <media type="image" mime="image/png" src="media/switch_on.png"/>
  <p>Tento vypínač přepíná název okna.</p>

      <code mime="text/x-csrc" style="numbered">
#include &lt;gtk/gtk.h&gt;



/* Zpracuje signál "active" od widgetu Switch */
static void
activate_cb (GObject    *switcher,
             GParamSpec *pspec,
             gpointer    user_data)
{
  GtkWindow *window = user_data;

  if (gtk_switch_get_active (GTK_SWITCH (switcher)))
    gtk_window_set_title (GTK_WINDOW (window), "Switch Example");
  else
    gtk_window_set_title (GTK_WINDOW (window), "");
}



static void
activate (GtkApplication *app,
          gpointer        user_data)
{
  GtkWidget *grid;
  GtkWidget *window;
  GtkWidget *label;
  GtkWidget *switcher;

  /* Vytvoří okno s nasteveným názvem a výchozí velikostí.
     Rovněž nastaví šířku okraje, aby uvnitř okna bylo ponecháno
     více místa */
  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "Switch Example");
  gtk_window_set_default_size (GTK_WINDOW (window), 300, 100);
  gtk_container_set_border_width (GTK_CONTAINER (window), 10);

  /* Vytvoří popisek */
  label = gtk_label_new ("Title");

  /* Vytvoří vypínač s výchozím stavem */
  switcher = gtk_switch_new ();
  gtk_switch_set_active (GTK_SWITCH (switcher), TRUE);

  /* Vytvoří mřížku a nastaví rozestup sloupců, připojí popisek
     a přepínače do mřížky a správné je umístí */
  grid = gtk_grid_new();
  gtk_grid_set_column_spacing (GTK_GRID (grid), 10);
  gtk_grid_attach (GTK_GRID (grid), label, 0, 0, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), switcher, 1, 0, 1, 1);

  /* Přípojí signál kliknutí na funkci zpětného volání */
  g_signal_connect (GTK_SWITCH (switcher), 
                    "notify::active", 
                    G_CALLBACK (activate_cb), 
                    window);

  gtk_container_add (GTK_CONTAINER (window), GTK_WIDGET (grid));

  gtk_widget_show_all (window);
}



int
main (int argc, char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}
</code>
<p>V této ukázce se používá následující:</p>
<list>
  <item><p><link href="http://developer.gnome.org/gtk3/3.4/GtkApplication.html">GtkApplication</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/3.4/GtkWindow.html">GtkWindow</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkLabel.html">GtkLabel</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkSwitch.html">GtkSwitch</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkGrid.html">GtkGrid</link></p></item>
</list>
</page>
