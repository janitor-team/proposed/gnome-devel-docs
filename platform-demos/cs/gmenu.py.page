<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="gmenu.py" xml:lang="cs">
  <info>
    <title type="text">GMenu (Python)</title>
    <link type="guide" xref="beginner.py#menu-combo-toolbar"/>
    <link type="seealso" xref="signals-callbacks.py"/>
    <link type="next" xref="menubutton.py"/>
    <revision version="0.1" date="2012-04-28" status="draft"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Jednoduchá implementace GMenu.</desc>
  </info>

  <title>GMenu</title>
  <media type="image" mime="image/png" src="media/gmenu.py.png"/>
  <p>GtkApplication s jednoduchým widgetem GMenu a objekty SimpleAction</p>

  <links type="section"/>

  <section id="code">
    <title>Kód použitý k vygenerování tohoto příkladu</title>
    <code mime="text/x-python" style="numbered">
    from gi.repository import Gtk
from gi.repository import Gio
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="GMenu Example", application=app)


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        # Spustí aplikaci
        Gtk.Application.do_startup(self)

        # Vytvoří nabídku
        menu = Gio.Menu()
        # Připojí do nabídky tři volby
        menu.append("New", "app.new")
        menu.append("About", "app.about")
        menu.append("Quit", "app.quit")
        # Nastaví nabídku jako aplikační nabídku
        self.set_app_menu(menu)

        # Vytvoří akci pro volbu "new" v nabídce
        new_action = Gio.SimpleAction.new("new", None)
        # Napojí ji na funkci zpětného volání new_cb
        new_action.connect("activate", self.new_cb)
        # Přidá akci do aplikace
        self.add_action(new_action)

        # Volba "about"
        about_action = Gio.SimpleAction.new("about", None)
        about_action.connect("activate", self.about_cb)
        self.add_action(about_action)

        # Volba "quit"
        quit_action = Gio.SimpleAction.new("quit", None)
        quit_action.connect("activate", self.quit_cb)
        self.add_action(quit_action)

    # Funkce zpětného volání pro "new"
    def new_cb(self, action, parameter):
        print("This does nothing. It is only a demonstration.")

    # Funkce zpětného volání pro "about"
    def about_cb(self, action, parameter):
        print("No AboutDialog for you. This is only a demonstration.")

    # Funkce zpětného volání pro "quit"
    def quit_cb(self, action, parameter):
        print("You have quit.")
        self.quit()

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
    <title>Užitečné metody pro GSimpleAction a GMenu</title>

    <p>Na řádku 33 je signál <code>"activate"</code> od činnosti <code>new_action</code> (což není nabídka!) napojen na funkci zpětného volání <code>new_cb()</code> pomocí <code><var>action</var>.connect(<var>signál</var>, <var>funkce zpětného volání</var>)</code>. Podrobnější vysvětlení viz <link xref="signals-callbacks.py"/>.</p>

    <p>Užitečné metody pro GSimpleAction:</p>
    <list>
      <item><p>K vytvoření nové akce, která je <em>bezstavová</em>, což znamená, že akce neuchovává stav nebo nezávisí na stavu daném akcí samotnou, použijte:</p>
      <code>
action = Gio.SimpleAction.new("název", typ_parametru)</code>
      <p>kde <code>"název"</code> je název akce a <code>typ_parametru</code> je typ parametrů, která akce přijímá při aktivaci. Může to být <code>None</code> nebo <code>GLib.VariantType.new('s')</code>, když je parametr typu <code>string</code>. Místo <code>'s'</code> můžete použít i jiné znaky popsané <link href="http://developer.gnome.org/glib/unstable/glib-GVariantType.html">zde</link>. K vytvoření <em>stavové</em> akce použijte:</p>
      <code>
action = Gio.SimpleAction.new_stateful("název", typ_parametru, počáteční_stav)</code>
      <p>kde <code>počáteční_stav</code> je definován jako GVariant, například <code>Glib.Variant.new_string("start")</code>. Seznam možných stavů najdete <link href="http://developer.gnome.org/glib/unstable/glib-GVariant.html">zde</link>.</p></item>
      <item><p><code>set_enabled(True)</code> nastavuje akci jako povolenou. Akce musí být povolená, aby šla aktivovat nebo aby šel její stav změnit voláním zvenku. Mělo by být voláno jen implementátorem akce. Uživatel akce by neměl zkoušel měnit tento její příznak.</p></item>
      <item><p><code>set_state(stav)</code>, kde <code>stav</code> je GVariant, nastavuje stav akce a tím aktualizuje vlastnost <code>state</code> na zadanou hodnotu. Mělo by být voláno jen implementátorem akce. Uživatel akce by měl při požadavku na změnu místo toho volat <code>change_state(stav)</code> (kde <code>stav</code> byl zmíně v předchozím).</p></item>
    </list>

    <p>Užitečné metody pro GMenu:</p>
    <list>
      <item><p>K vložení položky do nabídky na pozici <code>pozice</code> použijte <code>insert(pozice, popisek, upřesnění_akce)</code>, kde <code>popisek</code> je text, který se v nabídce objeví a <code>upřesnění_akce</code> je složený řetězec v podobě názvu akce, před který se připojí předpona <code>app.</code>. Podrobnější rozebrání této záležitosti můžete najít v <link xref="menubar.py#win-app"/>.</p>
      <p>Pro připojení položky do nabídky na konec respektive na začátek použijte <code>append(popisek, upřesnění_akce)</code> respektive <code>prepend(popisek, upřesnění_akce)</code>.</p></item>
      <item><p>Jiným způsobem, jak přidat položky do nabídky, je vytvořit je jako widgety <code>GMenuItem</code> a použít <code>insert_item(pozice, položka)</code>, <code>append_item(položka)</code> nebo <code>prepend_item(položka)</code>. Takže například můžeme mít:</p>
      <code>
about = Gio.MenuItem.new("About", "app.about")
menu.append_item(about)</code>
      </item>
      <item><p>Do nabídky můžeme také přidat celý pododdíl pomocí <code>insert_section(pozice, popisek, oddíl)</code>, <code>append_section(popisek, oddíl)</code> nebo <code>prepend_section(popisek, oddíl)</code>, kde <code>popisek</code> je název pododdílu.</p></item>
      <item><p>Přidáme také celou podnabídku, která se bude rozbalovat a sbalovat, pomocí <code>insert_submenu(pozice, popisek, oddíl)</code>, <code>append_submenu(popisek, oddíl)</code> nebo <code>prepend_submenu(popisek, oddíl)</code>, kde <code>popisek</code> jen název pododdílu.</p></item>
      <item><p>K odstranění položky z nabídky použijte <code>remove(pozice)</code>.</p></item>
      <item><p>K nastavení popisku pro nabídku použijte <code>set_label(popisek)</code>.</p></item>
    </list>

  </section>

  <section id="references">
    <title>Odkazy k API</title>
    <p>V této ukázce se používá následující:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gio/unstable/GMenu.html">GMenu</link></p></item>
      <item><p><link href="http://developer.gnome.org/gio/stable/GSimpleAction.html">GSimpleAction</link></p></item>
      <item><p><link href="http://developer.gnome.org/glib/unstable/glib-GVariantType.html">GVariantType</link></p></item>
      <item><p><link href="http://developer.gnome.org/glib/unstable/glib-GVariant.html">GVariant</link></p></item>
    </list>
  </section>
</page>
