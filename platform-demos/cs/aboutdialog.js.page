<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="aboutdialog.js" xml:lang="cs">
  <info>
  <title type="text">AboutDialog (JavaScript)</title>
    <link type="guide" xref="beginner.js#windows"/>
    <revision version="0.1" date="2012-05-30" status="draft"/>

    <credit type="author copyright">
      <name>Taryn Fox</name>
      <email its:translate="no">jewelfox@fursona.net</email>
      <years>2012</years>
    </credit>

    <desc>Zobrazuje informace o aplikaci</desc>
  </info>

  <title>AboutDialog</title>
  <media type="image" mime="image/png" src="media/aboutdialog_GMenu.png"/>
  <p>Modální dialogové okno, které zobrazuje informace o aplikaci a jejích autorech. Spouští se kliknutím na <gui>About</gui> v aplikační nabídce, což je pro ni normálně to správné místo.</p>

<code mime="application/javascript" style="numbered">#!/usr/bin/gjs

imports.gi.versions.Gtk = '3.0';

const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;

class AboutDialogExample {

    // Vytvoří vlastní aplikaci
    constructor() {
        this.application = new Gtk.Application({
            application_id: 'org.example.jsaboutdialog',
            flags: Gio.ApplicationFlags.FLAGS_NONE
        });

    // Napojí signály "activate" a "startup" k funkcím zpětného volání
        this.application.connect('activate', this._onActivate.bind(this));
        this.application.connect('startup', this._onStartup.bind(this));
    }

    // Funkce zpětného volání pro signál "activate" zobrazujicí okno při aktivaci
    _onActivate() {
        this._window.present();
    }

    // Funkce zpětného volání pro signál "startup" sestavující uživatelské rozhraní
    _onStartup() {
        this._initMenus();
        this._buildUI();
    }

    // Sestaví uživatelské rozhraní aplikace
    _buildUI() {
        // Vytvoří okno aplikace
        this._window = new Gtk.ApplicationWindow({ application: this.application,
                                                   window_position: Gtk.WindowPosition.CENTER,
                                                   title: "AboutDialog Example",
                                                   default_height: 250,
                                                   default_width: 350 });

        // Zobrazí okno a všechny jeho synovské widgety
        this._window.show_all();
    }

    // Vytvoří aplikační nabídku
    _initMenus() {
        let menu = new Gio.Menu();
        menu.append("About", 'app.about');
        menu.append("Quit",'app.quit');
        this.application.set_app_menu(menu);

        // Vytvoří položku nabídky „About“ a dá ji za úkol volat funkci _showAbout()
        let aboutAction = new Gio.SimpleAction({ name: 'about' });
        aboutAction.connect('activate', () =&gt; { this._showAbout(); });
        this.application.add_action(aboutAction);

        // Vytvoří položku nabídky „Quit“ a dá ji za úkol zavřít okno
        let quitAction = new Gio.SimpleAction ({ name: 'quit' });
        quitAction.connect('activate', () =&gt; { this._window.destroy(); });
        this.application.add_action(quitAction);
    }

    _showAbout() {

        // Pole řetězců se jmény lidí, kteří se podílejí na projektu
        var authors = ["GNOME Documentation Team"];
        var documenters = ["GNOME Documentation Team"];

        // Vytvoří dialogové okno „O aplikaci“
        let aboutDialog = new Gtk.AboutDialog({ title: "AboutDialog Example",
                                                program_name: "GtkApplication Example",
                                                copyright: "Copyright \xa9 2012 GNOME Documentation Team",
                                                authors: authors,
                                                documenters: documenters,
                                                website: "http://developer.gnome.org",
                                                website_label: "GNOME Developer Website" });

        // Připojí dialogové okno „O aplikaci“ k oknu
        aboutDialog.modal = true;
        aboutDialog.transient_for = this._window;

        // Zobrazí dialogové okno „O aplikaci“
        aboutDialog.show();

        // Napojí tlačítko Close na signál "destroy" pro dialogové okno
        aboutDialog.connect('response', function() {
            aboutDialog.destroy();
        });
    }
};

// Spustí aplikaci
let app = new AboutDialogExample();
app.application.run(ARGV);
</code>
<p>V této ukázce se používá následující:</p>
<list>
  <item><p><link href="http://developer.gnome.org/gio/unstable/GMenu.html">GMenu</link></p></item>
  <item><p><link href="http://developer.gnome.org/gio/stable/GSimpleAction.html">GSimpleAction</link></p></item>
  <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.AboutDialog.html">Gtk.AboutDialog</link></p></item>
  <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Application.html">Gtk.Application</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkApplicationWindow.html">Gtk.ApplicationWindow</link></p></item>
</list>
</page>
