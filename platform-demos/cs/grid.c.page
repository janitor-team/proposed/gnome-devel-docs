<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="grid.c" xml:lang="cs">
  <info>
    <title type="text">Grid (C)</title>
    <link type="guide" xref="c#layout"/>
    <revision version="0.1" date="2012-06-04" status="draft"/>

    <credit type="author copyright">
      <name>Monica Kochofar</name>
      <email its:translate="no">monicakochofar@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Balí widgety do řádků a sloupců</desc>
  </info>

  <title>Grid</title>

  <media type="image" mime="image/png" src="media/grid.png"/>
  <p>Widget tlačítka napojený na ukazatel průběhu.</p>

      <code mime="text/x-csrc" style="numbered">
#include &lt;gtk/gtk.h&gt;



/* Obsluha signálu pro signál "clicked" tlačítka. Každé 
   kliknutí vygeneruje zapulzování ukazatele průběhu */
static void
on_button_click (GtkButton *button,
                 gpointer   user_data)
{
  GtkProgressBar *progress_bar = user_data;
  gtk_progress_bar_pulse (progress_bar);
}



static void
activate (GtkApplication *app,
          gpointer        user_data)
{
  GtkWidget *grid;
  GtkWidget *window;
  GtkWidget *button;
  GtkWidget *progress_bar;

  /* Vytvoří okno a nastaví mu název */
  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "Grid Example");

  /* Vytvoří tlačítko s popiskem */
  button = gtk_button_new_with_label ("Button");

  /* Vytvoří ukazatel průběhu */
  progress_bar = gtk_progress_bar_new ();

  /* Vytvoří mřížku a správně do ní připojí tlačítko a ukazatel
     průběhu */
  grid = gtk_grid_new ();
  gtk_grid_attach (GTK_GRID (grid), button, 1, 1, 1, 1);
  gtk_grid_attach_next_to (GTK_GRID (grid), 
                           progress_bar, 
                           button, 
                           GTK_POS_BOTTOM, 1, 1);

  /* Napojí signál "clicked" na funkci zpětného volání */
  g_signal_connect (GTK_BUTTON (button), "clicked", 
                    G_CALLBACK (on_button_click), progress_bar);

  gtk_container_add (GTK_CONTAINER (window), GTK_WIDGET (grid));

  gtk_widget_show_all (window);
}



int
main (int argc, char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}
</code>
<p>V této ukázce se používá následující:</p>
<list>
  <item><p><link href="http://developer.gnome.org/gtk3/3.4/GtkApplication.html">GtkApplication</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/3.4/GtkWindow.html">GtkWindow</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkProgressBar.html">GtkProgressBar</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkButton.html">GtkButton</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkGrid.html">GtkGrid</link></p></item>
</list>
</page>
