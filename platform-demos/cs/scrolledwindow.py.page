<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="scrolledwindow.py" xml:lang="cs">
  <info>
    <title type="text">ScrolledWindow (Python)</title>
    <link type="guide" xref="beginner.py#scrolling"/>
    <link type="next" xref="paned.py"/>
    <revision version="0.1" date="2012-05-26" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Přidává posuvníky pro svůj synovský widget</desc>
  </info>

  <title>ScrolledWindow</title>
  <media type="image" mime="image/png" src="media/scrolledwindow.png"/>
  <p>Obrázek v okně s posuvníky.</p>

  <links type="section"/>

  <section id="code">
    <title>Kód použitý k vygenerování tohoto příkladu</title>
    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(
            self, title="ScrolledWindow Example", application=app)
        self.set_default_size(200, 200)

        # Okno s posuvníky
        scrolled_window = Gtk.ScrolledWindow()
        scrolled_window.set_border_width(10)
        # Zde jsou posuvníky vždy (jinak: AUTOMATIC - podle potřeby, 
        # nebo NEVER - nikdy)
        scrolled_window.set_policy(
            Gtk.PolicyType.ALWAYS, Gtk.PolicyType.ALWAYS)

        # Obrázek - lehce větší než okno
        image = Gtk.Image()
        image.set_from_file("gnome-image.png")

        # Přidá obrázek do okna s posuvníky
        scrolled_window.add_with_viewport(image)

        # Přidá okno s posuvníky do okna
        self.add(scrolled_window)


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>
  <section id="methods">
    <title>Užitečné metody pro widget ScrolledWindow</title>
    <list>
      <item><p><code>set_policy(pravidlo_vodorovného_posuvníku, pravidlo_svislého posuvníku)</code>, kde oba parametry jsou něco z <code>Gtk.Policy.AUTOMATIC, Gtk.Policy.ALWAYS, Gtk.Policy.NEVER</code> reguluje, kdy by se měl vodorovný a svislý posuvník zobrazit: s <code>AUTOMATIC</code> se zobrazí v případě potřeby, s <code>ALWAYS</code> vždy a s <code>NEVER</code> nikdy.</p></item>
      <item><p><code>add_with_viewport(widget)</code> se používá k přidání <code>widgetu</code> (potomka <code>Gtk.Widget</code>) bez přirozené schopnosti posouvání svého obsahu.</p></item>
      <item><p><code>set_placement(umisteni_okna)</code> nastavuje umístění obsahu vůči posuvníkům v okně s posuvníky. Možnosti argumentu jsou <code>Gtk.CornerType.TOP_LEFT</code> (výchozí: posuvníky jsou u spodního a pravého okraje okna), <code>Gtk.CornerType.TOP_RIGHT</code>, <code>Gtk.CornerType.BOTTOM_LEFT</code>, <code>Gtk.CornerType.BOTTOM_RIGHT</code>.</p></item>
      <item><p><code>set_hadjustment(přizpůsobení)</code> a <code>set_vadjustment(přizpůsobení)</code> nastaví <code>přizpůsobení</code> typu <code>Gtk.Adjustment</code>. Jedná se o reprezentaci hodnoty se spodní a horní mezí, spolu s přírůstkem kroku a stránky a s velikostí stránky. Vytvoří se jako <code>Gtk.Adjustment(hodnota, spodní_mez, horní_mez, přírůstek_kroku, přírůstek_stránky, velikost_stránky)</code>, kde všechny údaje jsou typu <code>float</code>. (Poznamenejme, že v tomto případě není <code>přírůstek_kroku</code> použit, takže může být nastaven na <code>0</code>.)</p></item>
    </list>
  </section>
  <section id="references">
    <title>Odkazy k API</title>
    <p>V této ukázce se používá následující:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkScrolledWindow.html">GtkScrolledWindow</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/gtk3-Standard-Enumerations.html">Standardní výčtové konstanty</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkImage.html">GtkImage</link></p></item>
    </list>
  </section>
</page>
