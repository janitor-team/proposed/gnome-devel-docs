<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="GtkApplicationWindow.js" xml:lang="sv">
  <info>
  <title type="text">ApplicationWindow (Javascript)</title>
    <link type="guide" xref="beginner.js#windows"/>
    <revision version="0.1" date="2012-04-07" status="draft"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>GtkWindow-underklass med GtkApplication-stöd</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>ApplicationWindow</title>
  <media type="image" mime="image/png" src="media/window.png"/>
  <p>A simple GtkApplicationWindow which can support Menus.</p>

<code mime="application/javascript" style="numbered">
#!/usr/bin/gjs

imports.gi.versions.Gtk = '3.0';

const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;

class Application {

    //skapa programmet
    constructor() {
        this.application = new Gtk.Application ({
            application_id: 'org.example.myapp',
            flags: Gio.ApplicationFlags.FLAGS_NONE
        });

       //anslut ”activate”- och ”startup”-signaler till återanropsfunktionerna
       this.application.connect('activate', this._onActivate.bind(this));
       this.application.connect('startup', this._onStartup.bind(this));
    }

    //skapa användargränssnittet (i detta fall bara vårt ApplicationWindow
    _buildUI() {
        this._window = new Gtk.ApplicationWindow({ application: this.application,
                                                   window_position: Gtk.WindowPosition.CENTER,
                                                   title: "Välkommen till GNOME" });

        //att avkommentera raden nedan kommer ändra fönsterstorleken
        //this._window.set_default_size(600, 400);

        //visa fönstret och alla barnkomponenter (ingen i detta fall)
        this._window.show_all();
    }

    //återanropsfunktion för ”activate”-signal
    _onActivate() {
        this._window.present();
    }

    //återanropsfunktion för ”startup”-signal
    _onStartup() {
        this._buildUI();
    }
};

//kör programmet
let app = new Application ();
app.application.run (ARGV);
</code>
<p>I detta exempel använde vi följande:</p>
<list>
  <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Application.html">Gtk.Application</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkApplicationWindow.html">Gtk.ApplicationWindow</link></p></item>
</list>
</page>
