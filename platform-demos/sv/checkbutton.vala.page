<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="checkbutton.vala" xml:lang="sv">
  <info>
  <title type="text">CheckButton (Vala)</title>
    <link type="guide" xref="beginner.vala#buttons"/>
    <revision version="0.1" date="2012-05-09" status="draft"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Skapa komponenter med en diskret växlingsknapp</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>CheckButton</title>
  <media type="image" mime="image/png" src="media/checkbutton.png"/>
  <p>Denna CheckButton visar/döljer titeln.</p>

<code mime="text/x-csharp" style="numbered">/* Ett fönster i programmet */
class MyWindow : Gtk.ApplicationWindow {

	/* Konstruktorn */
	internal MyWindow (MyApplication app) {
		Object (application: app, title: "CheckButton-exempel");

		this.set_default_size (300, 100);
		this.border_width = 10;

		var checkbutton = new Gtk.CheckButton.with_label ("Visa titel");

		/* Anslut kryssrutan till
		 * återanropsfunktionen (även kallad signalhanterare).
		 */
		checkbutton.toggled.connect (this.toggled_cb);

		/* Lägger till knappen till detta fönster */
		this.add (checkbutton);

		checkbutton.set_active (true);
		checkbutton.show ();
	}

	/* Signalhanteraren för kryssrutans ”toggled”-signal. */
	void toggled_cb (Gtk.ToggleButton checkbutton) {
		if (checkbutton.get_active())
			this.set_title ("CheckButton-exempel");
		else
			this.set_title ("");
	}
}

/* Det här är programmet */
class MyApplication : Gtk.Application {

	/* Konstruktorn */
	internal MyApplication () {
		Object (application_id: "org.example.checkbutton");
	}

	/* Åsidosätt ”activate”-signalen för GLib.Application */
	protected override void activate () {
		new MyWindow (this).show ();
	}

}

/* main skapar och kör programmet */
int main (string[] args) {
	return new MyApplication ().run (args);
}
</code>
<p>I detta exempel använde vi följande:</p>
<list>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.CheckButton.html">Gtk.CheckButton</link></p></item>
</list>
</page>
