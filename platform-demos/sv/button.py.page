<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="button.py" xml:lang="sv">
  <info>
    <title type="text">Button (Python)</title>
    <link type="guide" xref="beginner.py#buttons"/>
    <link type="seealso" xref="signals-callbacks.py"/>
    <link type="next" xref="linkbutton.py"/>
    <revision version="0.2" date="2012-05-05" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>En knappkomponent vilken sänder ut en signal när den klickas på</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Button</title>

  <media type="image" mime="image/png" src="media/button.png"/>
  <p>En knappkomponent ansluten till en enkel återanropsfunktion.</p>

  <links type="section"/>

  <section id="code">
    <title>Kod som använts för att generera detta exempel</title>
    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):
    # ett fönster

    def __init__(self, app):
        Gtk.Window.__init__(self, title="GNOME Button", application=app)
        self.set_default_size(250, 50)

        # en knapp
        button = Gtk.Button()
        # med en etikett
        button.set_label("Klicka på mig")
        # anslut ”clicked”-signalen som utges av knappen
        # till återanropsfunktionen do_clicked
        button.connect("clicked", self.do_clicked)
        # lägg till knappen till fönstret
        self.add(button)

    # återanropsfunktion ansluten till knappens signal ”clicked”
    def do_clicked(self, button):
        print("Du klickade på mig!")


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>
  
  <section id="methods">
    <title>Användbara metoder för en Button-komponent</title>
    <p>På rad 16 ansluts signalen <code>"clicked"</code> från knappen till återanropsfunktionen <code>do_clicked()</code> med <code><var>komponent</var>.connect(<var>signal</var>, <var>återanropsfunktion</var>)</code>. Se <link xref="signals-callbacks.py"/> för en utförligare förklaring.</p>
    <list>
      <item><p><code>set_relief(Gtk.ReliefStyle.NONE)</code> ställer reliefstilen på kanterna för vår Gtk.Button till ingen - till skillnad från <code>Gtk.ReliefStyle.NORMAL</code>.</p></item>
      <item><p>Om etiketten för knappen är en <link href="http://developer.gnome.org/gtk3/unstable/gtk3-Stock-Items.html">standardikon</link> så ställer <code>set_use_stock(True)</code> in etiketten som namnet på motsvarande standardikon.</p></item>
      <item><p>För att ställa in en bild (t.ex. en standardbild) för knappen <code>button</code>:</p>
        <code>
image = Gtk.Image()
image.set_from_stock(Gtk.STOCK_ABOUT, Gtk.IconSize.BUTTON)
button.set_image(image)</code>
      <p>Du bör inte ställa in en etikett för knappen efter detta, då kommer den visa etiketten och inte bilden.</p></item>
      <item><p>Om vi använder <code>set_focus_on_click(False)</code> kommer knappen inte fånga fokus när den klickas på med musen. Detta kan vara användbart i platser som verktygsfält, så att tangentbordsfokus inte tas bort från programmets huvudområde.</p></item>
    </list>
  </section>
  
  <section id="references">
    <title>API-referenser</title>
    <p>I detta exempel använde vi följande:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkButton.html">GtkButton</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkWindow.html">GtkWindow</link></p></item>
    </list>
  </section>
</page>
