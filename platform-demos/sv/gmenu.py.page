<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="gmenu.py" xml:lang="sv">
  <info>
    <title type="text">GMenu (Python)</title>
    <link type="guide" xref="beginner.py#menu-combo-toolbar"/>
    <link type="seealso" xref="signals-callbacks.py"/>
    <link type="next" xref="menubutton.py"/>
    <revision version="0.1" date="2012-04-28" status="draft"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>En enkel implementation av GMenu</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>GMenu</title>
  <media type="image" mime="image/png" src="media/gmenu.py.png"/>
  <p>En GtkApplication med en enkel GMenu och SimpleActions</p>

  <links type="section"/>

  <section id="code">
    <title>Kod som använts för att generera detta exempel</title>
    <code mime="text/x-python" style="numbered">
    from gi.repository import Gtk
from gi.repository import Gio
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="GMenu Example", application=app)


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        # start the application
        Gtk.Application.do_startup(self)

        # create a menu
        menu = Gio.Menu()
        # append to the menu three options
        menu.append("New", "app.new")
        menu.append("About", "app.about")
        menu.append("Quit", "app.quit")
        # set the menu as menu of the application
        self.set_app_menu(menu)

        # create an action for the option "new" of the menu
        new_action = Gio.SimpleAction.new("new", None)
        # connect it to the callback function new_cb
        new_action.connect("activate", self.new_cb)
        # add the action to the application
        self.add_action(new_action)

        # option "about"
        about_action = Gio.SimpleAction.new("about", None)
        about_action.connect("activate", self.about_cb)
        self.add_action(about_action)

        # option "quit"
        quit_action = Gio.SimpleAction.new("quit", None)
        quit_action.connect("activate", self.quit_cb)
        self.add_action(quit_action)

    # callback function for "new"
    def new_cb(self, action, parameter):
        print("This does nothing. It is only a demonstration.")

    # callback function for "about"
    def about_cb(self, action, parameter):
        print("No AboutDialog for you. This is only a demonstration.")

    # callback function for "quit"
    def quit_cb(self, action, parameter):
        print("You have quit.")
        self.quit()

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
    <title>Användbara metoder för en GSimpleAction och en GMenu</title>

    <p>På rad 33 ansluts signalen <code>"activate"</code> från åtgärden <code>new_action</code> (inte menyn!) till återanropsfunktionen <code>new_cb()</code> med <code><var>åtgärd</var>.connect(<var>signal</var>, <var>återanropsfunktion</var>)</code>. Se <link xref="signals-callbacks.py"/> för en utförligare förklaring.</p>

    <p>Användbara metoder för en GSimpleAction:</p>
    <list>
      <item><p>To create a new action that is <em>stateless</em>, that is, an action that do not retain or depend on a state given by the action itself, use</p>
      <code>
action = Gio.SimpleAction.new("name", parameter_type)</code>
      <p>where <code>"name"</code> is the name of the action and <code>parameter_type</code> is the type of the parameters that the action receives when being activated. This can be <code>None</code>, or <code>GLib.VariantType.new('s')</code> if the parameter is of type <code>str</code>, or instead of <code>'s'</code> a character as described <link href="http://developer.gnome.org/glib/unstable/glib-GVariantType.html">here</link>. To create a new <em>stateful</em> (i.e. not stateless) action, use</p>
      <code>
action = Gio.SimpleAction.new_stateful("name", parameter_type, initial_state)</code>
      <p>where <code>initial_state</code> is defined as a GVariant - for instance <code>Glib.Variant.new_string('start')</code>; for a list of possibilities see <link href="http://developer.gnome.org/glib/unstable/glib-GVariant.html">here</link>.</p></item>
      <item><p><code>set_enabled(True)</code>  sets the action as enabled; an action must be enabled in order to be activated or in order to have its state changed from outside callers. This should only be called by the implementor of the action. Users of the action should not attempt to modify its enabled flag.</p></item>
      <item><p><code>set_state(state)</code>, where <code>state</code> is a GVariant, sets the state of the action, updating the 'state' property to the given value. This should only be called by the implementor of the action; users of the action should instead call <code>change_state(state)</code> (where <code>state</code> is as above) to request the change.</p></item>
    </list>

    <p>Användbara metoder för en GMenu:</p>
    <list>
      <item><p>To insert an item in the menu in position <code>position</code>, use <code>insert(position, label, detailed_action)</code>, where <code>label</code> is the label that will appear in the menu and <code>detailed_action</code> is a string composed of the name of the action to which we prepend the prefix <code>app.</code>. A more detailed discussion of this can be found in <link xref="menubar.py#win-app"/>.</p>
      <p>To append or prepend an item in the menu use respectively <code>append(label, detailed_action)</code> and <code>prepend(label, detailed_action)</code>.</p></item>
      <item><p>Another way of adding items to the menu is to create them as <code>GMenuItem</code>s and use <code>insert_item(position, item)</code>, <code>append_item(item)</code>, or <code>prepend_item(item)</code>; so for instance we might have:</p>
      <code>
about = Gio.MenuItem.new("About", "app.about")
menu.append_item(about)</code>
      </item>
      <item><p>We can also add a whole subsection in a menu using <code>insert_section(position, label, section)</code>, <code>append_section(label, section)</code>, or <code>prepend_section(label, section)</code>, where <code>label</code> is the title of the subsection.</p></item>
      <item><p>To add a submenu that will expand and collapse, use <code>insert_submenu(position, label, section)</code>, <code>append_submenu(label, section)</code>, or <code>prepend_submenu(label, section)</code>, where <code>label</code> is the title of the subsection.</p></item>
      <item><p>To remove an item from the menu, use <code>remove(position)</code>.</p></item>
      <item><p>To set a label for the menu, use <code>set_label(label)</code>.</p></item>
    </list>

  </section>

  <section id="references">
    <title>API-referenser</title>
    <p>I detta exempel använde vi följande:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gio/unstable/GMenu.html">GMenu</link></p></item>
      <item><p><link href="http://developer.gnome.org/gio/stable/GSimpleAction.html">GSimpleAction</link></p></item>
      <item><p><link href="http://developer.gnome.org/glib/unstable/glib-GVariantType.html">GVariantType</link></p></item>
      <item><p><link href="http://developer.gnome.org/glib/unstable/glib-GVariant.html">GVariant</link></p></item>
    </list>
  </section>
</page>
