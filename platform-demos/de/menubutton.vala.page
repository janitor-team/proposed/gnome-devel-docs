<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="menubutton.vala" xml:lang="de">
  <info>
  <title type="text">MenuButton (Vala)</title>
    <link type="guide" xref="beginner.vala#buttons"/>
    <revision version="0.1" date="2012-07-18" status="draft"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>A widget that shows a menu when clicked on</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2013, 2016, 2018, 2021</mal:years>
    </mal:credit>
  </info>

  <title>MenuButton</title>
  <media type="image" mime="image/png" src="media/menubutton.png"/>
  <p>The GtkMenuButton widget is used to display a menu when clicked on. This menu can be provided either as a GtkMenu, or an abstract GMenuModel.

The GtkMenuButton widget can hold any valid child widget. That is, it can hold almost any other standard GtkWidget. The most commonly used child is the provided GtkArrow.</p>

<note><p>Sie müssen GNOME 3.6 ausführen, damit der MenuButton funktioniert.</p></note>
<code mime="text/x-csharp" style="numbered">public class MyWindow : Gtk.ApplicationWindow {

	internal MyWindow (MyApplication app) {
		Object (application: app, title: "MenuButton Example");
		this.set_default_size (600, 400);
		var grid = new Gtk.Grid ();

		var menubutton = new Gtk.MenuButton();
		menubutton.set_size_request (80, 35);

		var menumodel = new Menu ();
		menumodel.append ("New", "app.new");
		menumodel.append ("About", "win.about");

		/* We create the last item as a MenuItem, so that
		 * a submenu can be appended to this menu item.
		 */
		var submenu = new Menu ();
		menumodel.append_submenu ("Other", submenu);
		submenu.append ("Quit", "app.quit");
		menubutton.set_menu_model (menumodel);

		var about_action = new SimpleAction ("about", null);
		about_action.activate.connect (this.about_cb);
		this.add_action (about_action);

		this.add(grid);
		grid.attach(menubutton, 0, 0, 1, 1);
	}

	void about_cb (SimpleAction simple, Variant? parameter) {
		print ("You clicked \"About\"\n");
	}
}

public class MyApplication : Gtk.Application {
	protected override void activate () {
		new MyWindow (this).show_all ();
	}

	internal MyApplication () {
		Object (application_id: "org.example.MyApplication");
	}

	/* Override the 'startup' signal of GLib.Application. */
	protected override void startup () {
		base.startup ();

		var new_action = new SimpleAction ("new", null);
		new_action.activate.connect (this.new_cb);
		this.add_action (new_action);

		var quit_action = new SimpleAction ("quit", null);
		quit_action.activate.connect (this.quit);
		this.add_action (quit_action);
	}

	void new_cb (SimpleAction simple, Variant? parameter) {
		print ("You clicked \"New\"\n");
	}
}

public int main (string[] args) {
	return new MyApplication ().run (args);
}
</code>
<p>In diesem Beispiel haben wir Folgendes verwendet:</p>
<list>
  <item><p><link href="https://developer.gnome.org/gtk3/unstable/GtkMenuButton.html">MenuButton</link></p></item>
</list>
</page>
