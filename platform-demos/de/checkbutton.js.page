<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="checkbutton.js" xml:lang="de">
  <info>
  <title type="text">CheckButton (JavaScript)</title>
    <link type="guide" xref="beginner.js#buttons"/>
    <revision version="0.1" date="2012-06-12" status="draft"/>

    <credit type="author copyright">
      <name>Taryn Fox</name>
      <email its:translate="no">jewelfox@fursona.net</email>
      <years>2012</years>
    </credit>

    <desc>Ein Kästchen, das aktiviert oder deaktiviert werden kann</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2013, 2016, 2018, 2021</mal:years>
    </mal:credit>
  </info>

  <title>CheckButton</title>
  <media type="image" mime="image/png" src="media/checkbutton.png"/>
  <p>This application has a CheckButton. Whether the box is checked dictates whether the window's title bar shows anything.</p>
  <p>A CheckButton sends the "toggled" signal when it's checked or unchecked. While it's checked, the "active" property is true. While it's not, "active" tests as false.</p>
    <links type="section"/>

  <section id="imports">
    <title>Zu importierende Bibliotheken</title>
    <code mime="application/javascript">
#!/usr/bin/gjs

imports.gi.versions.Gtk = '3.0';

const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;
</code>
    <p>These are the libraries we need to import for this application to run. Remember that the line which tells GNOME that we're using Gjs always needs to go at the start.</p>
    </section>

  <section id="applicationwindow">
    <title>Entwurf des Anwendungsfensters</title>
    <code mime="application/javascript">
class CheckButtonExample {
    // Create the application itself
    constructor() {
        this.application = new Gtk.Application({
            application_id: 'org.example.jscheckbutton',
            flags: Gio.ApplicationFlags.FLAGS_NONE
        });

    // Connect 'activate' and 'startup' signals to the callback functions
    this.application.connect('activate', this._onActivate.bind(this));
    this.application.connect('startup', this._onStartup.bind(this));
    }

    // Callback function for 'activate' signal presents window when active
    _onActivate() {
        this._window.present();
    }

    // Callback function for 'startup' signal builds the UI
    _onStartup() {
        this._buildUI ();
    }
</code>
    <p>All the code for this sample goes in the CheckButtonExample class. The above code creates a <link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Application.html">Gtk.Application</link> for our widgets and window to go in.</p>
    <code mime="application/javascript">
    // Build the application's UI
    _buildUI() {

        // Create the application window
        this._window = new Gtk.ApplicationWindow({
            application: this.application,
            window_position: Gtk.WindowPosition.CENTER,
            default_height: 100,
            default_width: 300,
            border_width: 10,
            title: "CheckButton Example"});
</code>
    <p>The _buildUI function is where we put all the code to create the application's user interface. The first step is creating a new <link href="GtkApplicationWindow.js.page">Gtk.ApplicationWindow</link> to put all our widgets into.</p>
  </section>

  <section id="button">
    <title>Creating the checkbutton</title>
    <code mime="application/javascript">
        // Create the check button
        this._button = new Gtk.CheckButton ({label: "Show Title"});
        this._window.add (this._button);

        // Have the check button be checked by default
        this._button.set_active (true);

        // Connect the button to a function that does something when it's toggled
        this._button.connect ("toggled", this._toggledCB.bind(this));
</code>
    <p>This code creates the checkbutton itself. The label next to the checkbutton is created by giving the checkbutton the "label" property and assigning a string value to it. Since this checkbutton toggles whether the window title is on or off, and the window title will be on to start with, we want the box to be checked by default. Whenever the user checks or unchecks the box, we call the _toggledCB function.</p>
    <code mime="application/javascript">
        // Show the window and all child widgets
        this._window.show_all();
    }
</code>
    <p>This code finishes up creating the UI, by telling the window to show itself and all child widgets (which is just the checkbutton in this case).</p>
  </section>

  <section id="function">
    <title>Funktion, die das Umschalten des Checkbuttons handhabt</title>
    <code mime="application/javascript">
    _toggledCB() {

        // Make the window title appear or disappear when the checkbox is toggled
        if (this._button.get_active() == true)
            this._window.set_title ("CheckButton Example");
        else
            this._window.set_title ("");

    }

};
</code>
    <p>If the checkbutton is toggled from on to off, we want the window title to disappear. If it's toggled from off to on, we want it to reappear. We can tell which way it was toggled by testing to see whether it's active (checked) or not afterwards. A simple if / else statement which calls the checkbutton's get_active() method will work for this.</p>
    <code mime="application/javascript">
// Run the application
let app = new CheckButtonExample ();
app.application.run (ARGV);
</code>
    <p>Schließlich erstellen wir eine neue Instanz der fertigen Klasse CheckButtonExample und legen fest, dass die Anwendung ausgeführt wird.</p>
  </section>

  <section id="complete">
    <title>Vollständiges Codebeispiel</title>
<code mime="application/javascript" style="numbered">#!/usr/bin/gjs

imports.gi.versions.Gtk = '3.0';

const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;

class CheckButtonExample {

    // Create the application itself
    constructor() {
        this.application = new Gtk.Application({
            application_id: 'org.example.jscheckbutton',
            flags: Gio.ApplicationFlags.FLAGS_NONE
        });

        // Connect 'activate' and 'startup' signals to the callback functions
        this.application.connect('activate', this._onActivate.bind(this));
        this.application.connect('startup', this._onStartup.bind(this));
    }

    // Callback function for 'activate' signal presents window when active
    _onActivate() {
        this._window.present();
    }

    // Callback function for 'startup' signal builds the UI
    _onStartup() {
        this._buildUI();
    }

    // Build the application's UI
    _buildUI() {

        // Create the application window
        this._window = new Gtk.ApplicationWindow({
            application: this.application,
            window_position: Gtk.WindowPosition.CENTER,
            default_height: 100,
            default_width: 300,
            border_width: 10,
            title: "CheckButton Example"});

        // Create the check button
        this._button = new Gtk.CheckButton ({label: "Show Title"});
        this._window.add (this._button);

        // Have the check button be checked by default
        this._button.set_active (true);

        // Connect the button to a function that does something when it's toggled
        this._button.connect ("toggled", this._toggledCB.bind(this));

        // Show the window and all child widgets
        this._window.show_all();
    }

    _toggledCB() {

        // Make the window title appear or disappear when the checkbox is toggled
        if (this._button.get_active() == true)
            this._window.set_title ("CheckButton Example");
        else
            this._window.set_title ("");

    }

};

// Run the application
let app = new CheckButtonExample ();
app.application.run (ARGV);
</code>
  </section>

  <section id="in-depth">
    <title>Weiterführende Dokumentation</title>
<list>
  <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Application.html">Gtk.Application</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkApplicationWindow.html">Gtk.ApplicationWindow</link></p></item>
  <item><p><link href="http://www.roojs.org/seed/gir-1.2-gtk-3.0/gjs/Gtk.CheckButton.html">Gtk.CheckButton</link></p></item>
</list>
  </section>
</page>
