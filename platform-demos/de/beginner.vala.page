<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:e="http://projectmallard.org/experimental/" type="guide" style="task" id="beginner.vala" xml:lang="de">

<info>
  <title type="text">Tutorial für Anfänger (Vala)</title>
  <link type="guide" xref="vala#code-samples"/>
  <revision version="0.1" date="2012-02-19" status="stub"/>

  <desc>Ein Leitfaden für Anfänger zur GUI-Programmierung mithilfe von GTK+ inklusive Codebeispielen und praktischen Übungen.</desc>
  <credit type="author">
    <name>Tiffany Antopolski</name>
    <email its:translate="no">tiffany.antopolski@gmail.com</email>
  </credit>
    <credit type="editor">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasettii@gmail.com</email>
      <years>2013</years>
    </credit>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2013, 2016, 2018, 2021</mal:years>
    </mal:credit>
  </info>

<title>Tutorial für Anfänger mit Codebeispielen</title>
<synopsis>
 <p>Although these tutorials are designed for beginners, we can't cover all the basics.  Before attempting to follow these tutorials, you are expected to be familiar with the following concepts:</p>
<list type="numbered">
  <item><p>Objektorientierte Programmierung</p></item>
  <item><p>Die Programmiersprache Vala:</p>
    <list>
     <item><p><link href="https://live.gnome.org/Vala/Tutorial">The Vala Tutorial</link></p></item>
     <item><p><link href="https://live.gnome.org/Vala/Documentation#Sample_Code">Vala-Beispielcode</link></p></item>
    </list>
  </item>
</list>

<p>Wenn Sie diesen Tutorials folgen, werden Sie die Grundlagen der GUI-Programmierung mithilfe von GTK+ erlernen.</p>
</synopsis>

<section id="tutorials">
<title>Einführungen</title>
</section>

<section id="samples">
<title>Codebeispiele</title>
  <p>So führen Sie die Codebeispiele aus:</p>
  <steps>
    <item><p>Kopieren und fügen Sie Code in <var>Dateiname</var>.vala ein</p></item>
    <item><p>Geben Sie Folgendes im Terminal ein:</p>
          <screen>valac --pkg gtk+-3.0 <var>filename</var>.vala</screen>
          <screen>./<var>filename</var></screen>
    </item>
  </steps>

  <section id="windows" style="2column"><title>Fenster</title>
    <p/>
  </section>
  <section id="display-widgets" style="2column"><title>Widgets anzeigen</title>
  </section>
  <section id="buttons" style="2column"><title>Knöpfe und Auslöser</title>
  </section>
  <section id="entry" style="2column"><title>Numerische und Textdateneingabe</title>
  </section>
  <section id="multiline" style="2column"><title>Ein mehrzeiliger Texteditor</title>
  </section>
  <section id="menu-combo-toolbar" style="2column"><title>Menü-, Kombinationsfeld- und Werkzeugleisten-Widgets</title>
  </section>
  <section id="treeview"><title>TreeView-Widget</title>
  </section>
  <section id="selectors"><title>Auswähler</title>
    <section id="file-selectors"><title>Dateiwähler</title>
    </section>
    <section id="font-selectors"><title>Schriftwähler</title>
    </section>
    <section id="color-selectors"><title>Farbwähler</title>
    </section>
  </section>
  <section id="layout"><title>Layout-Container</title>
  </section>
  <section id="ornaments"><title>Ornamente</title>
  </section>
  <section id="scrolling"><title>Bildlauf</title>
  </section>
  <section id="misc"><title>Verschiedenes</title>
  </section>
</section>

<section id="exercises">
<title>Übungen</title>
</section>

</page>
