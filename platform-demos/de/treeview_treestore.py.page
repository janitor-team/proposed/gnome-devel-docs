<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="treeview_treestore.py" xml:lang="de">
  <info>
    <title type="text">TreeView mit TreeStore (Python)</title>
    <link type="guide" xref="beginner.py#treeview"/>
    <link type="seealso" xref="model-view-controller.py"/>
    <link type="next" xref="model-view-controller.py"/>
    <revision version="0.1" date="2012-06-30" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>A TreeView displaying a TreeStore (simpler example)</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2013, 2016, 2018, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Simpler TreeView with TreeStore</title>
  <media type="image" mime="image/png" src="media/treeview_treestore.png"/>
  <p>This TreeView displays a TreeStore.</p>

  <links type="section"/>

  <section id="code">
    <title>Code, der zum Generieren dieses Beispiels verwendet wurde</title>

    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
from gi.repository import Pango
import sys

books = [["Tolstoy, Leo", "War and Peace", "Anna Karenina"],
         ["Shakespeare, William", "Hamlet", "Macbeth", "Othello"],
         ["Tolkien, J.R.R.", "The Lord of the Rings"]]


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Library", application=app)
        self.set_default_size(250, 100)
        self.set_border_width(10)

        # the data are stored in the model
        # create a treestore with one column
        store = Gtk.TreeStore(str)
        for i in range(len(books)):
            # the iter piter is returned when appending the author
            piter = store.append(None, [books[i][0]])
            # append the books as children of the author
            j = 1
            while j &lt; len(books[i]):
                store.append(piter, [books[i][j]])
                j += 1

        # the treeview shows the model
        # create a treeview on the model store
        view = Gtk.TreeView()
        view.set_model(store)

        # the cellrenderer for the column - text
        renderer_books = Gtk.CellRendererText()
        # the column is created
        column_books = Gtk.TreeViewColumn(
            "Books by Author", renderer_books, text=0)
        # and it is appended to the treeview
        view.append_column(column_books)

        # the books are sortable by author
        column_books.set_sort_column_id(0)

        # add the treeview to the window
        self.add(view)


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
    <title>Nützliche Methoden für ein TreeView-Widget</title>
    <p>The TreeView widget is designed around a <em>Model/View/Controller</em> design: the <em>Model</em> stores the data; the <em>View</em> gets change notifications and displays the content of the model; the <em>Controller</em>, finally, changes the state of the model and notifies the view of these changes. For more information and for a list of useful methods for TreeModel see <link xref="model-view-controller.py"/>.</p>
  </section>

  <section id="references">
    <title>API-Referenzen</title>
    <p>In diesem Beispiel haben wir Folgendes verwendet:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkTreeView.html">GtkTreeView</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkTreeModel.html">GtkTreeModel</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkTreeStore.html">GtkTreeStore</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkCellRendererText.html">GtkCellRendererText</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkTreeViewColumn.html">GtkTreeViewColumn</link></p></item>
    </list>
  </section>
</page>
