<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:e="http://projectmallard.org/experimental/" type="guide" style="task" id="properties.py" xml:lang="fr">

<info>
  <title type="text">Properties (Python)</title>
  <link type="guide" xref="beginner.py#theory"/>
  <link type="next" xref="grid.py"/>
  <revision version="0.1" date="2012-06-24" status="draft"/>

  <desc>Une explication des propriétés, des mécanismes d'obtention et de définition.</desc>
  <credit type="author copyright">
    <name>Sebastian Pölsterl</name>
    <email its:translate="no">sebp@k-d-w.org</email>
    <years>2011</years>
  </credit>
  <credit type="editor">
    <name>Marta Maria Casetti</name>
    <email its:translate="no">mmcasetti@gmail.com</email>
    <years>2012</years>
  </credit>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Rebert,</mal:name>
      <mal:email>traduc@rebert.name</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski,</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luis Menina</mal:name>
      <mal:email>liberforce@freeside.fr</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Propriétés</title>

<links type="section"/>

<section id="overview">
<title>Présentation</title>

<p><em>Properties</em> describe the configuration and state of widgets. Each
widget has its own particular set of properties. For example, a widget such as
a button has the property <code>label</code> which contains the text of the
widget. You can specify the name and value of any number of properties as
keyword arguments when creating an instance of a widget. For example, to
create a label with the text “Hello World”, an angle of 25 degrees, and
aligned to the right, you can use:</p>
<code>
label = Gtk.Label(label="Hello World", angle=25, halign=Gtk.Align.END)</code>

<p>Alternatively, you can define these properties separately by using the method associated with it.</p>
<code>
label = Gtk.Label()
label.set_label("Hello World")
label.set_angle(25)
label.set_halign(Gtk.Align.END)</code>

<p>Once you have created such a label, you can get the text of the label with <code>label.get_label()</code>, and analogously for the other properties.</p>

<p>Instead of using getters and setters you can also get and set the properties with <code>get_property(<var>"prop-name"</var>)</code> and <code>set_property(<var>"prop-name"</var>, <var>value</var>)</code>, respectively.</p>

</section>
<section id="references">
<title>Références</title>

<p><link href="http://python-gtk-3-tutorial.readthedocs.org/en/latest/basics.html">Basics - Properties</link> in Python GTK+ 3 Tutorial</p>
</section>

</page>
