<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="weatherAutotools.js" xml:lang="fr">
  <info>
    <link type="guide" xref="weatherApp.js#main" group="#last"/>
    <revision version="0.1" date="2012-03-09" status="stub"/>

    <credit type="author copyright">
      <name>Susanna Huhtanen</name>
      <email its:translate="no">ihmis.suski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Rebert,</mal:name>
      <mal:email>traduc@rebert.name</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski,</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luis Menina</mal:name>
      <mal:email>liberforce@freeside.fr</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Autotools et icônes</title>
  <synopsis>
    <p>Dans cette partie du guide, nous allons construire les « autotools » et personnaliser les icônes dont nous avons besoin pour que l'application météo fasse partie intégrante de notre bureau. Pour écrire et lancer tous les exemples de code, vous avez besoin d'un éditeur pour écrire le code, de l'application Terminal et d'un ordinateur sur lequel GNOME 3 ou supérieur est installé. Dans ce guide, nous illustrerons les éléments suivants :</p>

    <list>
      <item><p><link xref="#autotools"> Autotools</link></p></item>
      <item><p><link xref="#icons">Custom icons for your application</link></p></item>
    </list>
  </synopsis>



  <section id="autotools">
  <title>Autotools et fichiers nécessaires</title>
  <p>Having more than one file in your folder makes using autotools a bit tricky. You need the .desktop file, autogen.sh, Makefile.am, configure.ac and as a new file: myapp.sh.in file. Hacking the autotools file is a complicated field. More information can be found in many different sources, <link href="http://en.wikipedia.org/wiki/GNU_build_system">the Wikipedia article</link> provides a good overview on the subject.
  </p>
    <list>
      <item><p>weatherapp.desktop</p></item>
      <item><p>weatherapp.sh.in</p></item>
      <item><p>Makefile.am</p></item>
      <item><p>configure.ac</p></item>
      <item><p>autogen.sh</p></item>
    </list>
    <p>weatherapp.desktop</p>
    <code mime="text/.desktop" style="numbered"><![CDATA[
[Desktop Entry]
Version=1.0
Encoding=UTF-8
Name=Weather app
Comment=Weather showing application
Exec=weatherapp.sh
Icon=application-default-icon
Terminal=false
Type=Application
StartupNotify=true
Categories=GNOME;GTK;Utility;]]></code>
    <p>La chose à noter dans ce fichier est que la ligne « Exec » rendra fonctionnel ce fichier « desktop » seulement après avoir exécuté tous les autres « makefiles ». Weatherapp.sh est un petit script créé avec le weatherapp.sh.in.</p>

    <p>weatherapp.sh.in</p>
    <code mime="text/sh.in" style="numbered"><![CDATA[
#!/bin/sh
export GJS_PATH=@bindir@
gjs @bindir@/weatherapp.js]]></code>
    <p>Ce fichier est un modèle que le fichier Makefile va créer pour être lancé à partir de .desktop.</p>

    <p>Makefile.am</p>
    <code mime="text/am" style="numbered"><![CDATA[
# The actual runnable program is set to the SCRIPTS primitive. Prefix bin_ tells where to copy this
bin_SCRIPTS = weatherapp.js geonames.js weatherapp.sh
# List of files to be distributed
EXTRA_DIST=  \
    $(bin_SCRIPTS) \
    $(private_icons)	\
	  $(NULL)

CLEANFILES =

# The desktop files
desktopdir = $(datadir)/applications
desktop_DATA =weatherapp.desktop

# convenience command for doing Makefile variable substitutions in non-Makefile
# files (scripts, service files, etc.)
do_subst = sed -e 's|@abs_top_srcdir[@]|$(abs_top_srcdir)|g' \
               -e 's|@abs_top_builddir[@]|$(abs_top_builddir)|g' \
               -e 's|@localedir[@]|$(localedir)|g' \
               -e 's|@bindir[@]|$(bindir)|g' \
               -e 's|@libexecdir[@]|$(libexecdir)|g' \
	       -e 's|@pkglibdir[@]|$(pkglibdir)|g' \
	       -e 's|@pkgdatadir[@]|$(pkgdatadir)|g' \
	       -e 's|@have_libnotify[@]|$(HAVE_LIBNOTIFY)|g' \
	       -e 's|@have_libsoup[@]|$(HAVE_LIBSOUP)|g' \
	       -e 's|@have_cheese[@]|$(HAVE_CHEESE)|g'

weatherapp.sh: weatherapp.sh.in
	$(AM_V_GEN) $(do_subst) $< > $@
	chmod +x $@

CLEANFILES += weatherapp.sh
EXTRA_DIST += weatherapp.sh.in

#the application icon
appicondir=$(datadir)/icons/hicolor/scalable/apps
appicon_DATA=weather-icon.svg

#icons in the application
NULL =

private_icons = \
	weather-clear.svg \
  weather-few-clouds.svg \
  weather-fog.svg \
  weather-icon.svg \
  weather-overcast.svg \
  weather-showers.svg \
  weather-showers-scattered.svg \
  weather-snow.svg \
  $(NULL)

install-icons:
	for icon in $(private_icons); do \
		mkdir -p $(DESTDIR)$(pkgdatadir)/icons/; \
		$(INSTALL_DATA) $(srcdir)/$$icon $(DESTDIR)$(pkgdatadir)/icons/; \
	done

install-data-local: install-icons]]></code>
    <p>Ceci nécessite un peu plus d'explications. Comparé au Makefile.am de Helloworld, celui-ci a été fortement modifié. Parcourons tous les nouveaux blocs :</p>
    <p>« bin_scripts » sont les fichiers nécessaires pour exécuter votre application. Dans ce cas, les deux premiers fichiers sont les programmes eux-mêmes et le troisième est le script qui lance l'application.</p>
    <p>EXTRA_DIST sont les fichiers qui doivent être distribués.</p>
    <p>le bloc « do_subst » est quelque chose qui a besoin d'être là où il est.</p>
    <p>after the comment #icons in the application there are all the icons that are used by the program. For them to be useful you need to install the icons in correct places and that is done by the install-icons: bit</p>

    <p>configure.ac</p>
    <code mime="text/ac" style="numbered"><![CDATA[
dnl This file is processed by autoconf to create a configure script
AC_INIT([Weather App], 1.0)
AM_INIT_AUTOMAKE([1.10 no-define foreign])
AC_CONFIG_FILES(Makefile)
AC_OUTPUT]]></code>
    <p>autogen.sh</p>
    <code mime="text/sh" style="numbered"><![CDATA[
#!/bin/sh
# This will run autoconf, automake, etc. for us
autoreconf --force --install]]></code>
  </section>
  <section id="icons">
  <title>Icônes personnalisées pour votre application</title>
  <p>Lorsque vous pensez icônes personnalisées, une bonne règle est : souhaitez-vous que cette icône puisse être utilisée autre part ou est-elle privée ? Dans le premier cas (par ex. des icônes dans le fichier desktop qui sont affichées par le shell) alors vous avez besoin de /usr/share/hicolor sinon (par ex. les icônes météo de votre application) /usr/share/$application/bla/bla</p>
  <p>Pour utiliser autotools, vous devez apporter quelques modifications à vos fichiers .desktop et Makefile. Dans le fichier desktop, modifiez le nom de l'icône Icon=weather-icon. Dans le fichier Makefile.am, ajoutez ces deux lignes à la fin de votre application #the application icon.</p>
  <p>appicondir=$(datadir)/icons/hicolor/scalable/apps</p>
  <p>appicon_DATA=weather-icon.svg</p>
  </section>

</page>
