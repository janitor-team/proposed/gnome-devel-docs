<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:e="http://projectmallard.org/experimental/" type="guide" style="task" id="beginner.vala" xml:lang="el">

<info>
  <title type="text">Μάθημα για αρχάριους (Vala)</title>
  <link type="guide" xref="vala#code-samples"/>
  <revision version="0.1" date="2012-02-19" status="stub"/>

  <desc>Ένας οδηγός αρχαρίων στην ανάπτυξη διεπαφών χρήστη (GUI) χρησιμοποιώντας GTK+, συμπεριλαμβάνει δείγματα κώδικα και πρακτικές ασκήσεις.</desc>
  <credit type="author">
    <name>Tiffany Antopolski</name>
    <email its:translate="no">tiffany.antopolski@gmail.com</email>
  </credit>
    <credit type="editor">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasettii@gmail.com</email>
      <years>2013</years>
    </credit>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Θουκιδίδου</mal:name>
      <mal:email>marablack3@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gmail.com</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Μάθημα για αρχάριους και δείγματα κώδικα</title>
<synopsis>
 <p>Αν και αυτά τα μαθήματα σχεδιάστηκαν για αρχαρίους, δεν μπορούμε να καλύψουμε όλα τα βασικά. Πριν να προσπαθήσετε να παρακολουθήσετε αυτά τα μαθήματα, πρέπει να εξοικειωθείτε με τις παρακάτω έννοιες:</p>
<list type="numbered">
  <item><p>Αντικειμενοστραφής προγραμματισμός</p></item>
  <item><p>Η γλώσσα προγραμματισμού Vala:</p>
    <list>
     <item><p><link href="https://live.gnome.org/Vala/Tutorial">Μαθήματα Vala</link></p></item>
     <item><p><link href="https://live.gnome.org/Vala/Documentation#Sample_Code">Δείγμα κώδικα Vala</link></p></item>
    </list>
  </item>
</list>

<p>Παρακολουθώντας αυτά τα μαθήματα θα μάθετε τα βασικά του προγραμματισμού GUI χρησιμοποιώντας GTK+.</p>
</synopsis>

<section id="tutorials">
<title>Μαθήματα</title>
</section>

<section id="samples">
<title>Δείγματα κώδικα</title>
  <p>Για να εκτελέσετε τα δείγματα κώδικα:</p>
  <steps>
    <item><p>Αντιγραφή και επικόλληση του κώδικα στο <var>filename</var>.vala</p></item>
    <item><p>Πληκτρολογήστε στο τερματικό:</p>
          <screen>valac --pkg gtk+-3.0 <var>filename</var>.vala</screen>
          <screen>./<var>filename</var></screen>
    </item>
  </steps>

  <section id="windows" style="2column"><title>Παράθυρα</title>
    <p/>
  </section>
  <section id="display-widgets" style="2column"><title>Εμφάνιση γραφικών στοιχείων</title>
  </section>
  <section id="buttons" style="2column"><title>Κουμπιά και εναλλαγές</title>
  </section>
  <section id="entry" style="2column"><title>Καταχώριση δεδομένων αριθμητικών και κειμένου</title>
  </section>
  <section id="multiline" style="2column"><title>Επεξεργαστής κειμένου πολλαπλών γραμμών</title>
  </section>
  <section id="menu-combo-toolbar" style="2column"><title>Μενού, σύνθετο πλαίσιο και γραφικά στοιχεία εργαλειοθήκης</title>
  </section>
  <section id="treeview"><title>Γραφικό στοιχείο TreeView</title>
  </section>
  <section id="selectors"><title>Επιλογείς</title>
    <section id="file-selectors"><title>Επιλογείς αρχείων</title>
    </section>
    <section id="font-selectors"><title>Επιλογείς γραμματοσειρών</title>
    </section>
    <section id="color-selectors"><title>Επιλογείς χρώματος</title>
    </section>
  </section>
  <section id="layout"><title>Περιέκτες διάταξης</title>
  </section>
  <section id="ornaments"><title>Διακοσμητικά</title>
  </section>
  <section id="scrolling"><title>Κύλιση</title>
  </section>
  <section id="misc"><title>Διάφορα</title>
  </section>
</section>

<section id="exercises">
<title>Ασκήσεις</title>
</section>

</page>
