<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="grid.c" xml:lang="el">
  <info>
    <title type="text">Πλέγμα (C)</title>
    <link type="guide" xref="c#layout"/>
    <revision version="0.1" date="2012-06-04" status="draft"/>

    <credit type="author copyright">
      <name>Monica Kochofar</name>
      <email its:translate="no">monicakochofar@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Πακετάρισμα γραφικών στοιχείων σε γραμμές και στήλες</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Θουκιδίδου</mal:name>
      <mal:email>marablack3@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gmail.com</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Πλέγμα</title>

  <media type="image" mime="image/png" src="media/grid.png"/>
  <p>Ένα γραφικό στοιχείο κουμπιού συνδεμένο με μια γραμμή προόδου.</p>

      <code mime="text/x-csrc" style="numbered">
#include &lt;gtk/gtk.h&gt;



/*Signal handler for the "clicked" signal of the Button. Each
click generates a progress bar pulse*/
static void
on_button_click (GtkButton *button,
                 gpointer   user_data)
{
  GtkProgressBar *progress_bar = user_data;
  gtk_progress_bar_pulse (progress_bar);
}



static void
activate (GtkApplication *app,
          gpointer        user_data)
{
  GtkWidget *grid;
  GtkWidget *window;
  GtkWidget *button;
  GtkWidget *progress_bar;

  /*Create the window and set a title*/
  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "Grid Example");

  /*Create a button with a label*/
  button = gtk_button_new_with_label ("Button");

  /*Create the progress bar*/
  progress_bar = gtk_progress_bar_new ();

  /*Create a grid and attach the button and progress bar
  accordingly*/
  grid = gtk_grid_new ();
  gtk_grid_attach (GTK_GRID (grid), button, 1, 1, 1, 1);
  gtk_grid_attach_next_to (GTK_GRID (grid), 
                           progress_bar, 
                           button, 
                           GTK_POS_BOTTOM, 1, 1);

  /*Connecting the clicked signal to the callback function*/
  g_signal_connect (GTK_BUTTON (button), "clicked", 
                    G_CALLBACK (on_button_click), progress_bar);

  gtk_container_add (GTK_CONTAINER (window), GTK_WIDGET (grid));

  gtk_widget_show_all (window);
}



int
main (int argc, char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}
</code>
<p>Σε αυτό το παράδειγμα χρησιμοποιήσαμε τα παρακάτω:</p>
<list>
  <item><p><link href="http://developer.gnome.org/gtk3/3.4/GtkApplication.html">GtkApplication</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/3.4/GtkWindow.html">GtkWindow</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkProgressBar.html">GtkProgressBar</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkButton.html">GtkButton</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkGrid.html">GtkGrid</link></p></item>
</list>
</page>
