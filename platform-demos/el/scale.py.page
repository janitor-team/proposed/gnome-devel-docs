<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="scale.py" xml:lang="el">
  <info>
    <title type="text">Κλίμακα (Python)</title>
    <link type="guide" xref="beginner.py#entry"/>
    <link type="seealso" xref="grid.py"/>
    <link type="next" xref="textview.py"/>
    <revision version="0.2" date="2012-06-23" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Ένας ολισθητής γραφικού στοιχείου για επιλογή μιας τιμής από μια περιοχή</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Θουκιδίδου</mal:name>
      <mal:email>marablack3@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gmail.com</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Κλίμακα</title>
  <media type="image" mime="image/png" src="media/scale.png"/>
  <p>Ολίσθηση των κλιμάκων!</p>

  <links type="section"/>

  <section id="code">
    <title>Ο χρησιμοποιούμενος κώδικας για παραγωγή αυτού παραδείγματος</title>
    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Scale Example", application=app)
        self.set_default_size(400, 300)
        self.set_border_width(5)

        # two adjustments (initial value, min value, max value,
        # step increment - press cursor keys to see!,
        # page increment - click around the handle to see!,
        # page size - not used here)
        ad1 = Gtk.Adjustment(0, 0, 100, 5, 10, 0)
        ad2 = Gtk.Adjustment(50, 0, 100, 5, 10, 0)

        # an horizontal scale
        self.h_scale = Gtk.Scale(
            orientation=Gtk.Orientation.HORIZONTAL, adjustment=ad1)
        # of integers (no digits)
        self.h_scale.set_digits(0)
        # that can expand horizontally if there is space in the grid (see
        # below)
        self.h_scale.set_hexpand(True)
        # that is aligned at the top of the space allowed in the grid (see
        # below)
        self.h_scale.set_valign(Gtk.Align.START)

        # we connect the signal "value-changed" emitted by the scale with the callback
        # function scale_moved
        self.h_scale.connect("value-changed", self.scale_moved)

        # a vertical scale
        self.v_scale = Gtk.Scale(
            orientation=Gtk.Orientation.VERTICAL, adjustment=ad2)
        # that can expand vertically if there is space in the grid (see below)
        self.v_scale.set_vexpand(True)

        # we connect the signal "value-changed" emitted by the scale with the callback
        # function scale_moved
        self.v_scale.connect("value-changed", self.scale_moved)

        # a label
        self.label = Gtk.Label()
        self.label.set_text("Move the scale handles...")

        # a grid to attach the widgets
        grid = Gtk.Grid()
        grid.set_column_spacing(10)
        grid.set_column_homogeneous(True)
        grid.attach(self.h_scale, 0, 0, 1, 1)
        grid.attach_next_to(
            self.v_scale, self.h_scale, Gtk.PositionType.RIGHT, 1, 1)
        grid.attach(self.label, 0, 1, 2, 1)

        self.add(grid)

    # any signal from the scales is signaled to the label the text of which is
    # changed
    def scale_moved(self, event):
        self.label.set_text("Horizontal scale is " + str(int(self.h_scale.get_value())) +
                            "; vertical scale is " + str(self.v_scale.get_value()) + ".")


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
    <title>Χρήσιμες μέθοδοι για γραφικό στοιχείο κλίμακας</title>
    <p>Μια Gtk.Adjustment απαιτείται για να δομήσει την Gtk.Scale. Αυτή είναι η αναπαράσταση μιας τιμής με κάτω και άνω όριο, μαζί με αυξήσεις βήματος και σελίδας και ένα μέγεθος σελίδας και δομείται ως <code>Gtk.Adjustment(value, lower, upper, step_increment, page_increment, page_size)</code> όπου τα πεδία είναι του τύπου <code>float</code>· <code>step_increment</code> είναι η αύξηση/μείωση που παίρνεται χρησιμοποιώντας τα πλήκτρα δρομέα, <code>page_increment</code> αυτή που παίρνεται πατώντας στην ίδια την κλίμακα. Σημειώστε ότι το <code>page_size</code> δεν χρησιμοποιείται σε αυτήν την περίπτωση, θα πρέπει να οριστεί σε <code>0</code>.</p>
    <p>Στη γραμμή 28 το σήμα <code>"value-changed"</code> συνδέεται με τη συνάρτηση επανάκλησης <code>scale_moved()</code> χρησιμοποιώντας <code><var>widget</var>.connect(<var>signal</var>, <var>callback function</var>)</code>. Δείτε <link xref="signals-callbacks.py"/> για μια πιο λεπτομερή εξήγηση.</p>
    <list>
      <item><p>Η <code>get_value()</code> ανακτά την τρέχουσα τιμή της κλίμακας· η <code>set_value(value)</code> την ορίζει (εάν η <code>value</code>, του τύπου <code>float</code>, είναι έξω από την ελάχιστη ή μέγιστη περιοχή, θα δεσμευθεί να προσαρμοστεί μέσα τους). Αυτές είναι οι μέθοδοι της κλάσης Gtk.Range.</p></item>
      <item><p>Χρησιμοποιήστε <code>set_draw_value(False)</code> για αποφυγή εμφάνισης της τρέχουσας τιμής ως συμβολοσειράς δίπλα στον ολισθητή.</p></item>
      <item><p>Για τονισμό μέρους της κλίμακας μεταξύ της αρχικής και της τρέχουσας τιμής:</p>
        <code mime="text/x-python">
self.h_scale.set_restrict_to_fill_level(False)
self.h_scale.set_fill_level(self.h_scale.get_value())
self.h_scale.set_show_fill_level(True)</code>
        <p>στην συνάρτηση επανάκλησης του σήματος "value-changed", έτσι ώστε να συμπληρώνεται κάθε φορά που αλλάζει η τιμή. Αυτές είναι οι μέθοδοι της κλάσης Gtk.Range.</p>
      </item>
      <item><p><code>add_mark(value, position, markup)</code> προσθέτει ένα σημάδι στην <code>value</code> (<code>float</code> ή <code>int</code> εάν αυτή είναι η ακρίβεια της κλίμακας), σε <code>position</code> (<code>Gtk.PositionType.LEFT, Gtk.PositionType.RIGHT, Gtk.PositionType.TOP, Gtk.PositionType.BOTTOM</code>) με κείμενο <code>Null</code> ή <code>markup</code> στη γλώσσα επισήμανσης Pango. Για καθαρισμό σημαδιών, <code>clear_marks()</code>.</p></item>
      <item><p>Ο <code>set_digits(digits)</code> ορίζει την ακρίβεια της κλίμακας σε ψηφία <code>digits</code>.</p></item>
    </list>
  </section>

  <section id="references">
    <title>Αναφορές API</title>
    <p>Σε αυτό το παράδειγμα χρησιμοποιήσαμε τα παρακάτω:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkScale.html">GtkScale</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkAdjustment.html">GtkAdjustment</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/gtk3-Standard-Enumerations.html">Standard Enumerations</link></p></item>
    </list>
  </section>
</page>
