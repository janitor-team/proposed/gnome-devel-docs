<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="switch.py" xml:lang="ca">
  <info>
    <title type="text">Switch (Python)</title>
    <link type="guide" xref="beginner.py#buttons"/>
    <link type="next" xref="radiobutton.py"/>
    <revision version="0.1" date="2012-05-24" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>A "light switch" style toggle</desc>
  </info>

  <title>Switch</title>
  <media type="image" mime="image/png" style="floatend" src="media/switch_off.png"/>
  <media type="image" mime="image/png" src="media/switch_on.png"/>

  <p>This Switch makes the title appears and disappear.</p>

  <links type="section"/>

  <section id="code">
    <title>Code used to generate this example</title>

    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):
    # a window

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Switch Example", application=app)
        self.set_default_size(300, 100)
        self.set_border_width(10)

        # a switch
        switch = Gtk.Switch()
        # turned on by default
        switch.set_active(True)
        # connect the signal notify::active emitted by the switch
        # to the callback function activate_cb
        switch.connect("notify::active", self.activate_cb)

        # a label
        label = Gtk.Label()
        label.set_text("Title")

        # a grid to allocate the widgets
        grid = Gtk.Grid()
        grid.set_column_spacing(10)
        grid.attach(label, 0, 0, 1, 1)
        grid.attach(switch, 1, 0, 1, 1)

        # add the grid to the window
        self.add(grid)

    # Callback function. Since the signal is notify::active
    # we need the argument 'active'
    def activate_cb(self, button, active):
        # if the button (i.e. the switch) is active, set the title
        # of the window to "Switch Example"
        if button.get_active():
            self.set_title("Switch Example")
        # else, set it to "" (empty string)
        else:
            self.set_title("")


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>

  </section>
  <section id="methods">
    <title>Useful methods for a Switch widget</title>
    <p>In line 17 the signal <code>"notify::active"</code> is connected to the callback function <code>activate_cb()</code> using <code><var>widget</var>.connect(<var>signal</var>, <var>callback function</var>)</code>. See <link xref="signals-callbacks.py"/> for a more detailed explanation.</p>

  </section>
  <section id="references">
    <title>API References</title>
    <p>In this sample we used the following:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkSwitch.html">GtkSwitch</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkLabel.html">GtkLabel</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkGrid.html">GtkGrid</link></p></item>
    </list>
  </section>
</page>
