<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="colorbutton.js" xml:lang="ca">
  <info>
    <title type="text">ColorButton (JavaScript)</title>
    <link type="guide" xref="beginner.js#color-selectors"/>
    <revision version="0.1" date="2013-06-25" status="review"/>

    <credit type="author copyright">
      <name>Meg Ford</name>
      <email its:translate="no">megford@gnome.org</email>
      <years>2013</years>
    </credit>

    <desc>A button to launch a color selection dialog</desc>
  </info>

  <title>ColorButton</title>
  <media type="image" mime="image/png" src="media/colorbutton.png"/>
  <p>This ColorButton launches a color selection dialog and prints in the terminal the RGB values of the color selected.</p>

  <links type="sections"/>
  
  <section id="code">
  <title>Code used to generate this example</title>
  <code mime="application/javascript" style="numbered">#!/usr/bin/gjs

imports.gi.versions.Gdk = '3.0';
imports.gi.versions.Gtk = '3.0';

const Gdk = imports.gi.Gdk;
const GObject = imports.gi.GObject;
const Gtk = imports.gi.Gtk;

class ColorbuttonExample {

    // Create the application itself
    constructor() {
        this.application = new Gtk.Application ({ application_id: 'org.example.jscolorbutton' });

        // Connect 'activate' and 'startup' signals to the callback functions
        this.application.connect('activate', this._onActivate.bind(this));
        this.application.connect('startup', this._onStartup.bind(this));
    }

    // Callback function for 'activate' signal presents windows when active
    _onActivate() {
        this.window.present();
    }

    // Callback function for 'startup' signal builds the UI
    _onStartup() {
        this._buildUI();
    }

    // Build the application's UI
    _buildUI() {

        // Create the application window
        this.window = new Gtk.ApplicationWindow ({ application: this.application,
                                                   window_position: Gtk.WindowPosition.CENTER,
                                                   title: "ColorButton",
                                                   default_width: 150,
                                                   default_height: 50,
                                                   border_width: 10 });

        this.button = new Gtk.ColorButton();
        this.color = new Gdk.RGBA();
        this.color.red = 0.0;
        this.color.green = 0.0;
        this.color.blue = 1.0;
        this.color.alpha = 0.5;
        this.button.set_rgba(this.color);
        this.button.connect("color-set", this.onColorChosen.bind(this));
        this.label = new Gtk.Label();
        this.label.set_text("Click to choose a color");

        let grid = new Gtk.Grid();
        grid.attach(this.button, 0, 0, 2, 1);
        grid.attach(this.label, 0, 1, 2, 1);
        this.window.add(grid);
        this.window.show_all();
    }

    onColorChosen() {
        let colorName = this.color.to_string();
        this.label.set_text("You chose the color " + colorName);
    }
};

// Run the application
let app = new ColorbuttonExample ();
app.application.run (ARGV);
</code>
  </section>

  <section id="references">
  <title>API References</title>
  <p>In this sample we used the following:</p>
  <list>
    <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.ColorButton.html">GtkColorButton</link></p></item>
    <item><p><link href="http://developer.gnome.org/gdk3/stable/gdk3-RGBA-Colors.html">RGBA Colors</link></p></item>
  </list>
  </section>
</page>
