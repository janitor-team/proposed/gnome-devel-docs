<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="spinner.c" xml:lang="gl">
  <info>
    <title type="text">Spinner (C)</title>
    <link type="guide" xref="c#display-widgets"/>
    <link type="seealso" xref="togglebutton.c"/>
    <revision version="0.1" date="2012-06-14" status="draft"/>

    <credit type="author copyright">
      <name>Monica Kochofar</name>
      <email its:translate="no">monicakochofar@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Unha animación spinner</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Dieguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2012-2013.</mal:years>
    </mal:credit>
  </info>

  <title>Indicador de progreso</title>

  <media type="image" mime="image/png" src="media/spinner.png"/>
  <p>This Spinner is stopped and started by pressing the spacebar.</p>

      <code mime="text/x-csrc" style="numbered">
#include &lt;gtk/gtk.h&gt;
 


 /*Global variable used to indicate active state of the
spinner. TRUE = active, FALSE = not-active. This is because 
there isn't a current function for C that does this for us*/
gboolean active;  
 


/*This is the callback function. It is a handler function 
which reacts to the signal. In this case, it will cause the 
spinner to start and stop according to how many times the user 
presses the spacebar.*/ 
static gboolean
key_pressed_event (GtkWidget *widget,
                   GdkEvent  *event,
                   gpointer   user_data)
{
  GtkWidget *spinner = user_data;
  guint keyval;
  
  /*Extracts the keyval from an event. And stores it in the  variable 
  "keyval" (we give the function the address). In this case, the 
  event is GdkEventKey, a key press event*/
  gdk_event_get_keyval (event, &amp;keyval);  

  /*Grabbing the boolean value from the spinner*/
  g_object_get (GTK_SPINNER (spinner), "active", &amp;active, NULL);
  
  if (keyval == GDK_KEY_space) {
     if (active) {
         gtk_spinner_stop (GTK_SPINNER (spinner));
     }
     else {
         gtk_spinner_start (GTK_SPINNER (spinner));
     } 
  }
  
return TRUE;
}
 


static void
activate (GtkApplication *app,
          gpointer        user_data)
{
  GtkWidget *window;
  GtkWidget *spinner;
 
  /*Create a window with a title, border width and a default size*/
  window = gtk_application_window_new (app);
 
  gtk_window_set_title (GTK_WINDOW (window), "Spinner Example");
  gtk_window_set_default_size (GTK_WINDOW (window), 200, 200);
  gtk_container_set_border_width (GTK_CONTAINER(window), 30);
 
  /*Create a spinner, with extra horizontal and vertical space*/
  spinner = gtk_spinner_new ();
  gtk_spinner_start (GTK_SPINNER (spinner));
  
  gtk_container_add (GTK_CONTAINER (window), spinner);
 
  /*Connecting the key-press-event signal to the callback*/
  g_signal_connect (GTK_WINDOW (window), "key-press-event", 
                    G_CALLBACK (key_pressed_event), spinner);
 
  gtk_widget_show_all (window);
}
 



int
main (int argc, char **argv)
{
  GtkApplication *app;
  int status;
 
  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);
 
  return status;
}
</code>
<p>Neste exemplo empregaremos o seguinte:</p>
<list>
  <item><p><link href="http://developer.gnome.org/gtk3/3.4/GtkApplication.html">GtkApplication</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/3.4/GtkWindow.html">GtkWindow</link></p></item>
  <item><p><link href="http://developer.gnome.org/gobject/stable/gobject-The-Base-Object-Type.html#g-object-get">GObject</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkSpinner.html">GtkSpinner</link></p></item>
</list>
</page>
