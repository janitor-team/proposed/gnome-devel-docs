<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="button.py" xml:lang="es">
  <info>
    <title type="text">Botón (Python)</title>
    <link type="guide" xref="beginner.py#buttons"/>
    <link type="seealso" xref="signals-callbacks.py"/>
    <link type="next" xref="linkbutton.py"/>
    <revision version="0.2" date="2012-05-05" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Un widget de botón que emite una señal cuando se pulsa sobre él</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gmail.com</mal:email>
      <mal:years>2012 - 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Button</title>

  <media type="image" mime="image/png" src="media/button.png"/>
  <p>Un widget de botón conectado a una función de retorno de llamada.</p>

  <links type="section"/>

  <section id="code">
    <title>Código usado para generar este ejemplo</title>
    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):
    # a window

    def __init__(self, app):
        Gtk.Window.__init__(self, title="GNOME Button", application=app)
        self.set_default_size(250, 50)

        # a button
        button = Gtk.Button()
        # with a label
        button.set_label("Click me")
        # connect the signal "clicked" emitted by the button
        # to the callback function do_clicked
        button.connect("clicked", self.do_clicked)
        # add the button to the window
        self.add(button)

    # callback function connected to the signal "clicked" of the button
    def do_clicked(self, button):
        print("You clicked me!")


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>
  
  <section id="methods">
    <title>Métodos útiles para un widget «Button»</title>
    <p>En la línea 16, la señal <code>«clicked»</code> del botón se conecta a la función de retorno de llamada <code>do_clicked()</code> usando <code><var>widget</var>.connect(<var>señal</var>, <var>función de retorno de llamada</var>)</code>. Consulte la sección <link xref="signals-callbacks.py"/> para obtener una explicación más detallada.</p>
    <list>
      <item><p><code>set_relief(Gtk.ReliefStyle.NONE)</code> establece el estilo de relieve de los bordes del «Gtk.Button» a «ninguno», opuesto a <code>Gtk.ReliefStyle.NORMAL</code>.</p></item>
      <item><p>Si la etiqueta de un botón es un <link href="http://developer.gnome.org/gtk3/unstable/gtk3-Stock-Items.html">icono del almacén</link>, <code>set_use_stock(True)</code> establece la etiqueta al nombre del icono del almacén correspondiente.</p></item>
      <item><p>Para establecer una imagen (por ejemplo, una imagen del almacén) para el botón <code>button</code>:</p>
        <code>
image = Gtk.Image()
image.set_from_stock(Gtk.STOCK_ABOUT, Gtk.IconSize.BUTTON)
button.set_image(image)</code>
      <p>No debe establecer una etiqueta para el botón después de esto, ya que si lo hace mostrará la etiqueta y no la imagen.</p></item>
      <item><p>Si usa <code>set_focus_on_click(False)</code>, el botón no atrapará el foco cuando el ratón lo pulse. Esto podría ser útil en lugares como barras de herramientas, para que el foco del teclado no se quite del área principal de la aplicación.</p></item>
    </list>
  </section>
  
  <section id="references">
    <title>Referencias de la API</title>
    <p>En este ejemplo se usa lo siguiente:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkButton.html">GtkButton</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkWindow.html">GtkWindow</link></p></item>
    </list>
  </section>
</page>
