<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="radiobutton.py" xml:lang="es">
  <info>
    <title type="text">RadioButton (Python)</title>
    <link type="guide" xref="beginner.py#buttons"/>
    <link type="seealso" xref="grid.py"/>
    <link type="next" xref="buttonbox.py"/>
    <revision version="0.1" date="2012-05-09" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Botones mutuamente excluyentes.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gmail.com</mal:email>
      <mal:years>2012 - 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>RadioButton</title>
  <media type="image" mime="image/png" src="media/radiobutton.png"/>
  <p>Tres «RadioButton». Puede ver en la terminal si están activados o no.</p>

  <links type="section"/>

  <section id="code">
    <title>Código usado para generar este ejemplo</title>
    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="RadioButton Example", application=app)
        self.set_default_size(250, 100)
        self.set_border_width(20)

        # a new radiobutton with a label
        button1 = Gtk.RadioButton(label="Button 1")
        # connect the signal "toggled" emitted by the radiobutton
        # with the callback function toggled_cb
        button1.connect("toggled", self.toggled_cb)

        # another radiobutton, in the same group as button1
        button2 = Gtk.RadioButton.new_from_widget(button1)
        # with label "Button 2"
        button2.set_label("Button 2")
        # connect the signal "toggled" emitted by the radiobutton
        # with the callback function toggled_cb
        button2.connect("toggled", self.toggled_cb)
        # set button2 not active by default
        button2.set_active(False)

        # another radiobutton, in the same group as button1,
        # with label "Button 3"
        button3 = Gtk.RadioButton.new_with_label_from_widget(
            button1, "Button 3")
        # connect the signal "toggled" emitted by the radiobutton
        # with the callback function toggled_cb
        button3.connect("toggled", self.toggled_cb)
        # set button3 not active by default
        button3.set_active(False)

        # a grid to place the buttons
        grid = Gtk.Grid.new()
        grid.attach(button1, 0, 0, 1, 1)
        grid.attach(button2, 0, 1, 1, 1)
        grid.attach(button3, 0, 2, 1, 1)
        # add the grid to the window
        self.add(grid)

    # callback function
    def toggled_cb(self, button):
        # a string to describe the state of the button
        state = "unknown"
        # whenever the button is turned on, state is on
        if button.get_active():
            state = "on"
        # else state is off
        else:
            state = "off"
        # whenever the function is called (a button is turned on or off)
        # print on the terminal which button was turned on/off
        print(button.get_label() + " was turned " + state)


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
    <title>Métodos útiles para un widget «RadioButton»</title>
    <p>En la línea 16, la señal <code>«toggled»</code> se conecta a la función de retorno de llamada <code>toggled_cb()</code> usando <code><var>widget</var>.connect(<var>señal</var>, <var>función de retorno de llamada</var>)</code>. Consulte la sección <link xref="signals-callbacks.py"/> para una explicación más detallada.</p>

    <p>Como se vio en la <link xref="properties.py"/>, en lugar de <code>button1 = Gtk.RadioButton(label="Button 1")</code> se podría crear el botón y su etiqueta con</p>
    <code>
button1 = Gtk.RadioButton()
button1.set_label("Button 1").</code>
    <p>Otra manera de crear un «RadioButton» nuevo con una etiqueta es <code>button1 = Gtk.RadioButton.new_with_label(None, "Button 1")</code> (El primer argumento es el grupo de botones de radio, que puede obtener con <code>get_group()</code>, el segundo argumento es la etiqueta).</p>
  </section>

  <section id="references">
    <title>Referencias de la API</title>
    <p>En este ejemplo se usa lo siguiente:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkWindow.html">GtkWindow</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkGrid.html">GtkGrid</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkRadioButton.html">GtkRadioButton</link></p></item>
    </list>
  </section>
</page>
