<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="combobox_multicolumn.vala" xml:lang="es">
  <info>
    <title type="text">ComboBox (Vala)</title>
    <link type="guide" xref="beginner.vala#menu-combo-toolbar"/>
    <link type="seealso" xref="combobox.vala"/>
    <revision version="0.1" date="2013-06-18" status="review"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2013</years>
    </credit>

    <desc>Un widget usado para elegir de una lista de elementos</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gmail.com</mal:email>
      <mal:years>2012 - 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>ComboBox (dos columnas)</title>
  <media type="image" mime="image/png" src="media/combobox_multicolumn.png"/>
  <p>Esta ComboBox imprime su selección en la terminal cuando cambia la cambia.</p>

  <links type="section"/>

  <section id="code">
    <title>Código usado para generar este ejemplo</title>
    <code mime="text/x-csharp" style="numbered">class MyWindow : Gtk.ApplicationWindow {

	string[] file = {"Select", "New", "Open", "Save"};
	string[] stock_item = {"","gtk-new", "gtk-open", "gtk-save"};

	enum Column {
		FILE,
		STOCK_ITEM	
	}

	/* Constructor */
	internal MyWindow (MyApplication app) {
		Object (application: app, title: "Welcome to GNOME");

		this.set_default_size (200, -1);
		this.border_width = 10;

		Gtk.ListStore liststore = new Gtk.ListStore (2, typeof (string), typeof (string));

		for (int i = 0; i &lt; file.length; i++){
			Gtk.TreeIter iter;
			liststore.append (out iter);
			liststore.set (iter, Column.FILE, file[i]);
			liststore.set (iter, Column.STOCK_ITEM, stock_item[i]);
		}

		Gtk.ComboBox combobox = new Gtk.ComboBox.with_model (liststore);

		/* CellRenderers render the data. */
		Gtk.CellRendererText cell = new Gtk.CellRendererText ();
		Gtk.CellRendererPixbuf cell_pb = new Gtk.CellRendererPixbuf ();
		
                /* we pack the cell into the beginning of the combobox, allocating
		 * no more space than needed;
		 * first the image, then the text;
		 * note that it does not matter in which order they are in the model,
		 * the visualization is decided by the order of the cellrenderers
		 */
		combobox.pack_start (cell_pb, false);
		combobox.pack_start (cell, false);

		/* associate a property of the cellrenderer to a column in the model
		 * used by the combobox
		 */
		combobox.set_attributes (cell_pb, "stock_id", Column.STOCK_ITEM);
		combobox.set_attributes (cell, "text", Column.FILE);

		/* Set the first item in the list to be selected (active). */
		combobox.set_active (0);

		/* Connect the 'changed' signal of the combobox
		 * to the signal handler (aka. callback function).
		 */
		combobox.changed.connect (this.item_changed);

		/* Add the combobox to this window */
		this.add (combobox);
		combobox.show ();
	}

	void item_changed (Gtk.ComboBox combo) {
		if (combo.get_active () !=0) {
			print ("You chose " + file [combo.get_active ()] +"\n");
		}
	}
}

class MyApplication : Gtk.Application {
        protected override void activate () {
                new MyWindow (this).show ();
        }
}

int main (string[] args) {
	return new MyApplication ().run (args);
}
</code>

  </section>

  <section id="references">
    <title>Referencias de la API</title>
    <p>En este ejemplo se usa lo siguiente:</p>
    <list>
      <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.ComboBox.html">GtkComboBox</link></p></item>
      <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.ListStore.html">GtkListStore</link></p></item>
      <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.CellRendererText.html">GtkCellRendererText</link></p></item>
      <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.CellRendererPixbuf.html">GtkCellRendererPixbuf</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/gtk3-Stock-Items.html">Elementos del almacén</link></p></item>
    </list>
  </section>
</page>
