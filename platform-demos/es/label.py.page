<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:e="http://projectmallard.org/experimental/" type="guide" style="task" id="label.py" xml:lang="es">
  <info>
    <title type="text">Label (Python)</title>
    <link type="guide" xref="beginner.py#display-widgets"/>
    <link type="seealso" xref="properties.py"/>
    <link type="seealso" xref="strings.py"/>
    <link type="next" xref="properties.py"/>
    <revision version="0.2" date="2012-06-18" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author">
      <name>Sebastian Pölsterl</name>
      <email its:translate="no">sebp@k-d-w.org</email>
      <years>2012</years>
    </credit>

    <desc>Un widget que muestra una cantidad pequeña o mediana de texto</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gmail.com</mal:email>
      <mal:years>2012 - 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Etiqueta</title>
  <media type="image" mime="image/png" src="media/label.png"/>
  <p>Una etiqueta sencilla</p>

  <links type="section"/>

  <section id="code">
  <title>Código usado para generar este ejemplo</title>

  <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):
    # constructor for a Gtk.ApplicationWindow

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Welcome to GNOME", application=app)
        self.set_default_size(200, 100)

        # create a label
        label = Gtk.Label()
        # set the text of the label
        label.set_text("Hello GNOME!")
        # add the label to the window
        self.add(label)


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>

    <p>Otra manera de obtener lo que se tiene en el ejemplo es crear la etiqueta como una instancia de otra clase y añadirle la instancia de <code>MyWindow</code> en el método <code>do_activate(self)</code>:</p>
    <note>
      <p>Las líneas de texto resaltadas indican que el código es diferente al del fragmento anterior.</p>
    </note>
      <code mime="text/x-python">
# a class to define a window
class MyWindow(Gtk.ApplicationWindow):
    def __init__(self, app):
        Gtk.Window.__init__(self, title="Welcome to GNOME", application=app)
        self.set_default_size(200, 100)

# a class to define a label
<e:hi>
class MyLabel(Gtk.Label):
    def __init__(self):
        Gtk.Label.__init__(self)
        self.set_text("Hello GNOME!")
</e:hi>

class MyApplication(Gtk.Application):
    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        # create an instance of MyWindow
        win = MyWindow(self)

        # create an instance of MyLabel
<e:hi>
        label = MyLabel()
</e:hi>
        # and add it to the window
<e:hi>
        win.add(label)
</e:hi>
        # show the window and everything on it
        win.show_all()</code>

  </section>

  <section id="methods">
  <title>Métodos útiles para un widget «Label»</title>
  
  <note style="tip">
    <p>Puede encontrar una explicación de cómo trabajar con cadenas en GTK+ en la <link xref="strings.py"/>.</p>
  </note>

  <list>
    <item><p><code>set_line_wrap(True)</code> rompe líneas si el texto de la etiqueta excede el tamaño del widget.</p></item>
    <item><p><code>set_justify(Gtk.Justification.LEFT)</code> (o <code>Gtk.Justification.RIGHT, Gtk.Justification.CENTER, Gtk.Justification.FILL</code>) establece la alineación de las líneas en el texto de la etiqueta respecto de sí. El método no tiene efecto en una etiqueta de una sola línea.</p></item>
    <item><p>Para decorar el texto se puede usar <code>set_markup("texto")</code>, donde <code>"texto"</code> es un texto en el <link href="http://developer.gnome.org/pango/stable/PangoMarkupFormat.html">lenguaje de marcación Pango</link>. Un ejemplo:</p>
      <code mime="text/x-python">
label.set_markup("Text can be &lt;small&gt;small&lt;/small&gt;, &lt;big&gt;big&lt;/big&gt;, "
                 "&lt;b&gt;bold&lt;/b&gt;, &lt;i&gt;italic&lt;/i&gt; and even point to somewhere "
                 "on the &lt;a href=\"http://www.gtk.org\" "
                 "title=\"Click to find out more\"&gt;internet&lt;/a&gt;.")</code>
    </item>
  </list>
  </section>

  <section id="references">
  <title>Referencias de la API</title>
  <p>En este ejemplo se usa lo siguiente:</p>
  <list>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkLabel.html">GtkLabel</link></p></item>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkWindow.html">GtkWindow</link></p></item>
  </list>
  </section>
</page>
