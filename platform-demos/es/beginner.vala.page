<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:e="http://projectmallard.org/experimental/" type="guide" style="task" id="beginner.vala" xml:lang="es">

<info>
  <title type="text">Tutorial para principiantes (Vala)</title>
  <link type="guide" xref="vala#code-samples"/>
  <revision version="0.1" date="2012-02-19" status="stub"/>

  <desc>Una guía de programación de interfaces usuario en GTK+ para principiantes, incluyendo ejemplos de código y ejercicios prácticos.</desc>
  <credit type="author">
    <name>Tiffany Antopolski</name>
    <email its:translate="no">tiffany.antopolski@gmail.com</email>
  </credit>
    <credit type="editor">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasettii@gmail.com</email>
      <years>2013</years>
    </credit>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gmail.com</mal:email>
      <mal:years>2012 - 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Tutorial para principiantes y ejemplos de código</title>
<synopsis>
 <p>Aunque estos tutoriales están diseñados para principiantes, no se pueden cubrir todos los conceptos básicos. Antes de intentar seguir estos tutoriales, debe estar familiarizado con los siguientes conceptos:</p>
<list type="numbered">
  <item><p>Programación orientada a objetos</p></item>
  <item><p>El lenguaje de programación Vala:</p>
    <list>
     <item><p><link href="https://live.gnome.org/Vala/Tutorial">El tutorial de Vala</link></p></item>
     <item><p><link href="https://live.gnome.org/Vala/Documentation#Sample_Code">Código de ejemplo de Vala</link></p></item>
    </list>
  </item>
</list>

<p>Siguiendo estos tutoriales, aprenderá los conceptos básicos de la programación de IGU usando GTK+.</p>
</synopsis>

<section id="tutorials">
<title>Tutoriales</title>
</section>

<section id="samples">
<title>Ejemplos de código</title>
  <p>Para ejecutar los ejemplos de código:</p>
  <steps>
    <item><p>Copie y pegue el código en <var>nombre_archivo</var>.vala</p></item>
    <item><p>Escriba en la terminal:</p>
          <screen>valac --pkg gtk+-3.0 <var>nombre_archivo</var>.vala</screen>
          <screen>./<var>nombre_archivo</var></screen>
    </item>
  </steps>

  <section id="windows" style="2column"><title>Ventanas</title>
    <p/>
  </section>
  <section id="display-widgets" style="2column"><title>Widgets de visualización</title>
  </section>
  <section id="buttons" style="2column"><title>Botones y casillas</title>
  </section>
  <section id="entry" style="2column"><title>Entrada de daos numérica y de texto</title>
  </section>
  <section id="multiline" style="2column"><title>Editor de texto multilínea</title>
  </section>
  <section id="menu-combo-toolbar" style="2column"><title>Widgets de menú, caja combinada y barra de herramientas</title>
  </section>
  <section id="treeview"><title>Widget TreeView</title>
  </section>
  <section id="selectors"><title>Selectores</title>
    <section id="file-selectors"><title>Selectores de archivos</title>
    </section>
    <section id="font-selectors"><title>Selectores de tipografías</title>
    </section>
    <section id="color-selectors"><title>Selectores de color</title>
    </section>
  </section>
  <section id="layout"><title>Contenedores de la distribución</title>
  </section>
  <section id="ornaments"><title>Adornos</title>
  </section>
  <section id="scrolling"><title>Desplazamiento</title>
  </section>
  <section id="misc"><title>Varios</title>
  </section>
</section>

<section id="exercises">
<title>Ejercicios</title>
</section>

</page>
