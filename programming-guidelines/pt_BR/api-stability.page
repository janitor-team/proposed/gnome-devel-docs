<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="api-stability" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="index#maintainer-guidelines"/>

    <credit type="author copyright">
      <name>Philip Withnall</name>
      <email its:translate="no">philip.withnall@collabora.co.uk</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Compatibilidade reversa nas APIs</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Estabilidade de API</title>

  <synopsis>
    <title>Resumo</title>

    <list>
      <item><p>Defina garantias de estabilidade de API para seu projeto. (<link xref="#stability"/>)</p></item>
      <item><p>Certifique-se de que números de versão sejam alterados conforme apropriado quando a API é alterada. (<link xref="#versioning"/>)</p></item>
    </list>
  </synopsis>

  <section id="api-and-abi">
    <title>API e ABI</title>

    <p>Em um nível alto, uma API – <em>Application Programming Interface</em> ou interface de programação de aplicação – é o limite entre dois componentes quando se está desenvolvendo para eles. Ela está intimamente relacionada a uma ABI – <em>Application Binary Interface</em> ou interface binária de aplicação – que é o limite em tempo real. Ela define as formas possíveis nas quais outros componentes podem interagir com um componente. Mais concretamente, ela normalmente significa que os cabeçalhos C de uma biblioteca formam sua API, e biblioteca compilada simboliza sua ABI. A diferença entre uma API e uma ABI é dada pela compilação do código: há certas coisas em um cabeçalho C, tal como <code>#define</code>s, que podem fazer com que uma API da biblioteca seja alterada sem alterar sua ABI. Mas essas diferenças são em sua maioria acadêmicas e, para todos os propósitos práticos, API e ABI podem ser tratados de forma intercambiável.</p>

    <p>Exemplos de alterações incompatíveis com API para uma função C seria adicionar um novo parâmetro, alterar o tipo de retorno da função ou remover um parâmetro.</p>

    <p>Porém, muitas outras partes de um projeto podem formar uma API. Se um daemon se expõe no D-Bus, as interfaces exportadas lá formam uma API. Similarmente, se uma API C é exposta em linguagens de alto nível pelo uso de GIR, o arquivo GIR forma uma outra API — se ela é alterada, qualquer código de alto nível usando-a também é alterado.</p>

    <p>Outros exemplos de APIs mais incomuns são locais e formatos de arquivos de configuração, e esquemas de GSettings. Quaisquer alterações nestes poderia exigir que o código usando sua biblioteca seja alterado.</p>
  </section>

  <section id="stability">
    <title>Estabilidade</title>

    <p>Estabilidade da API se refere a algum nível de garantia de um projeto que sua API será alterada apenas em formas definidas no futuro, ou jamais será alterada. Geralmente, um API é considerada ‘estável’ se ela se compromete em manter compatibilidade com versões anteriores (definido abaixo); mas APIs pode também se comprometer em ser instáveis ou até mesmo compatíveis com versões posteriores. O propósito das garantias de estabilidade de API é permitir que as pessoas usem seu projeto para seus próprios código se preocupar em atualizar constantemente seu código para se manter em dia com as alterações da API. Garantias comuns de estabilidade de API significa que o código que é compilado com uma versão de uma biblioteca vai funcionar sem problemas contra todas versões futuras daquela biblioteca com o mesmo número de versão maior — ou similarmente àquele código que funciona com um daemon vai continuar a funcionar em todas as versões futuras daquele daemon com o mesmo número de versão maior.</p>

    <p>É possível aplicar diferentes níveis de estabilidade de API a componentes dentro de um projeto. Por exemplo, as funções centrais em uma biblioteca poderiam ser estáveis e, consequentemente, sua API mantida inalterada no futuro; enquanto as funções mais novas e menos centrais serem mantidas instáveis e poderem ser alteradas descontroladamente até que o design certo seja encontrado, ponto no qual elas poderiam ser marcadas como estáveis.</p>

    <p>Vários tipos de estabilidade comumente considerados:</p>
    <terms>
      <item>
        <title>Instável (unstable)</title>
        <p>A API poderia alterar ou ser removida no futuro.</p>
      </item>
      <item>
        <title>Compatibilidade com versões anteriores</title>
        <p>Apenas alterações que permitem código compilado com a API não modificada para continuar funcionando com a API modificada são permitidos (por exemplo, funções não podem ser removidas).</p>
      </item>
      <item>
        <title>Compatibilidade com versões posteriores</title>
        <p>Apenas alterações que permitem código compilado com a API modificada para funcionar com a API modificada são permitidos (por exemplo, funções não podem ser adicionadas).</p>
      </item>
      <item>
        <title>Totalmente estável</title>
        <p>Nenhuma alteração é permitida à API, apenas para a implementação.</p>
      </item>
    </terms>

    <p>Geralmente, projetos se comprometem com compatibilidade reversa quando eles dizem que uma API é ‘estável’. Muito poucos projetos se comprometem à total estabilidade por impediria quase que todos os desenvolvimentos posteriores do projeto.</p>
  </section>

  <section id="versioning">
    <title>Versionamento</title>

    <p>Garantias de estabilidade de API estão fortemente vinculadas a versionamento do projeto; tanto versionamento de pacote e versionamento do libtool. Versionamento do libtool existe totalmente para o propósito de rastrear a estabilidade da ABI e é explicado em detalhes em <link href="https://autotools.io/libtool/version.html">Autotools Mythbuster</link> (Caçador de mitos de autotools) ou <link xref="versioning"/>.</p>

    <p>Versionamento de pacote (<em>maior.menor.micro</em>) está fortemente vinculado à estabilidade da API: geralmente, o número da versão maior é incrementado quando alterações incompatíveis com versões anteriores são feitas (por exemplo, quando funções são renomeadas, parâmetros são alterados, ou funções são removidas). O número da versão menor é incrementada quando alterações incompatíveis com versões posteriores são feitas (por exemplo, quando uma nova API pública é adicionada). O número da versão micro é incrementada quando as alterações no código são feitas sem modificar a API. Veja <link xref="versioning"/> para mais informações.</p>

    <p>Versionamento de API é tão importante para APIs de D-Bus e esquemas de GSettings (se eles são suscetíveis de alterações) quanto é para APIs C. Veja a <link href="http://dbus.freedesktop.org/doc/dbus-api-design.html#api-versioning">documentação sobre versionamento de API de D-Bus</link> para detalhes.</p>

    <p>Para APIs GIR, sua estabilidade geralmente segue a estabilidade da API C, como elas são geradas a partir da API C. Uma complexidade é que sua estabilidade depende adicionalmente na versão do gobject-introspection usada na geração do GIR, mas versões recentes não foram muito alteradas, de forma que isso não é uma grande preocupação.</p>
  </section>

  <section id="external-links">
    <title>Links externos</title>

    <p>O tópico de estabilidade de API é coberto nos seguintes artigos (em inglês):</p>
    <list>
      <item><p>
        <link href="https://en.wikipedia.org/wiki/Application_programming_interface">Wikipedia
        page on APIs</link>
      </p></item>
      <item><p>
        <link href="https://en.wikipedia.org/wiki/Application_binary_interface">Wikipedia
        page on ABIs</link>
      </p></item>
      <item><p><link href="http://dbus.freedesktop.org/doc/dbus-api-design.html#api-versioning">Documentação de versionamento de API D-Bus</link></p></item>
    </list>
  </section>
</page>
