<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="introduction" xml:lang="es">
   <info>
     <link type="guide" xref="index#intro"/>
   
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2012 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2009-2010</mal:years>
    </mal:credit>
  </info>
         <title>¿Qué se está optimizando?</title>
        <p>Al optimizar GNOME lo primero que hay que recordar es esto: no se está intentando mejorar el programa, se está intentando hacer que las personas usen el equipo más felices.</p>
        <p>Programas mejores hacen a la gente más feliz pero existen algunas mejoras que los harán más felices que otras: tiempo de respuesta, tiempo de inicio, facilidad para acceder a comandos y que el equipo no tenga que usar la memoria de intercambio cuando más de dos programas estén abiertos.</p>
        <p>La optimización tradicional contempla conceptos como el uso de CPI, el tamaño del código, el número de pulsaciones del ratón y el uso de memoria de programa. Se ha elegido esta segunda lista para correlar con la primera, no obstante existe una diferencia importante. A la persona que usa GNOME no le importa la segunda lista, pero sí la primera. Al optimizar los programas de GNOME se reducirá el uso de CPU, el uso de memoria y todo aquello, pero son conceptos para el fin, no el objetivo final. Se optimiza para las personas.</p>

	<section id="doing-the-optimization">
          <title>Realizar la optimización</title>
        <p>En la sección anterior se omitió un calificador importante: optimizar algo debe ser medible. No se puede medir la felicidad. No obstante se puede medir el tiempo de inicio de un programa para saber si se ha mejorado. La felicidad del usuario aumentará pues.</p>
        <p>La optimización es el proceso de medida, refinamiento y remedida. Lo primero que debe hacer es encontrar una forma de medir lo que está optimizando. Idealmente la medida es un simple número, por ejemplo: el tiempo que se tarda en realizar una tarea. Esta es su prueba, es la única forma de saber si está ganando o perdiendo. Existe una gran diferencia entre un programa que <em>debería</em> ser rápido y un programa que <em>es</em> rápido.</p>
        <p>Una vez que tiene una prueba de rendimiento básica debe encontrar por qué su código no lo está haciendo tan bien como debería. Es tentador hacerlo inspeccionando: simplemente mirar el código y tratar de encontrar algo que parece que necesita una mejora. Estará perdiendo el tiempo, Usar un perfilador para obtener una lista detallada de lo que su programa está haciendo es, realmente, la única forma de estar seguro.</p>
        <p>Generalmente el problema está aislado en pequeñas partes del código. Elija la peor parte y concéntrese en ella primero. Una vez que lo haya hecho, vuelva al perfilador y repita el proceso. Según proceda, las mejoras obtenidas en cada paso se harán cada vez más pequeñas, en algún punto tendrá que decidir que los resultados son suficientes. Si sus esfuerzos sólo están obteniendo un 10% de mejoras entonces hace tiempo que pasó el punto en el que debería haber parado.</p>
        <p>No se olvide del objetivo final. Por ejemplo, en lugar de intentar mejorar un trozo de código, pregúntese si realmente se necesita ejecutar ese código. ¿Se podría combinar con otro trozo de código? ¿Se pueden guardar y reusar cálculos previos? No necesitará optimizarse si está en un lugar donde el usuario no lo va a notar. O aún peor, el código puede ya estar optimizado y está haciendo cálculos pesados ahora para evitar hacerlos luego. El código no se ejecuta aisladamente ni tampoco lo hace el proceso de optimización.</p>
	</section>

	<section id="hints">
        <title>Sugerencias</title>

        <terms>
          <item>
            <title>Lo fundamental</title>
            <list type="ordered">
            <item>
                <p>Vuelva a ejecutar su prueba de rendimiento después de cada cambio que realice sobre el código y mantenga un registro de todo lo que cambia y de cómo afecta al rendimiento. Esto le permite deshacer errores y también le ayuda a no repetirlos.</p>
            </item>
            <item>
                <p>Asegúrese de que su código es correcto y está libre de errores antes de optimizarlo. Compruebe que permanece correcto y libre de errores después de haberlo optimizado.</p>
            </item>
            <item>
                <p>Optimizar al nivel más alto antes de optimizar los detalles.</p>
            </item>
            <item>
                <p>Usar el algoritmo correcto. El clásico ejemplo de libro de texto es usar ordenación rápida en lugar de ordenación de burbuja. Existen muchos otros, algunos ahorran memoria, algunos ahorran CPU. También debe ver qué atajos de teclado puede crear: puede hacerlo más rápido que una ordenación rápida si está preparado para tomar ciertos compromisos.</p>
            </item>
            <item>
                <p>La optimización es intercambio. Obtener resultados mejora los cálculos pero aumenta la memoria en uso. Guardar datos al disco ahorra memoria pero cuesta tiempo al cargarlos de nuevo desde el disco.</p>
            </item>
            <item>
                <p>Asegúrese de elegir cierta variedad de entradas que optimizar. Si no lo hace es fácil que termine con un trozo de código cuidadosamente optimizado para un campo y no para otros.</p>
            </item>
            <item>
                <p>Evite las operaciones caras: múltiples lecturas de disco pequeñas. Use un montón de memoria para que el área de intercambio («swap») se haga innecesario. Evite cualquier cosa que escriba o lea innecesariamente del disco. La red también es lenta. Evite también operaciones gráficas que necesitan una respuesta del servidor X.</p>
	    </item>
	    </list>
        </item>
        <item>
            <title>Trampas para los imprudentes</title>
            <list type="ordered">
            <item>
                <p>Esté atento a efectos laterales. Generalmente son interacciones extrañas entre diferentes secciones del código, una extensión de una parte puede retardar otra.</p>
            </item>
            <item>
                <p>Al cronometrar el tiempo del código, incluso en un sistema silencioso, los eventos fuera del programa añaden ruido a los resultados del tiempo. Haga la media sobre múltiples ejecuciones. Si el código es muy pequeño la resolución del tiempo también es un problema. En este caso mida el tiempo que el equipo tarda en ejecutar el código 100 o 1000 veces. Si los tiempos que está obteniendo son algo superiores a unos pocos segundos todo debería estar correcto.</p>
            </item>
            <item>
                <p>Es muy fácil perderse con el perfilador. Existen historias de programadores optimizando el bucle de inactividad porque es donde se perdía todo su tiempo. No optimice código del que el usuario no se preocupe.</p>
            </item>
            <item>
                <p>Recuerde los resultados del servidor X. El uso de memoria de su programa no incluye los pixmaps almacenados en los procesos del servidor X, pero aún siguen usando memoria. Use xrestop para ver los recursos que está usando su programa.</p>
            </item>
	    </list>
        </item>
	<item>
          <title>Consejos de bajo nivel</title>
            <list type="ordered">
            <item>
                <p>Al optimizar el uso de memoria considere la diferencia entre el uso de pico y el uso medio de memoria. Alguna memoria siempre está reservada, esto generalmente es malo. Alguna está temporalmente reservada, esto puede ser aceptable. Herramientas como massif usan el concepto de espacio-tiempo, el producto de la memoria usada y el tiempo durante el que estuvo reservada.</p>
            </item>
            <item>
                <p>Controle el tiempo de trozos de código simplificados que sólo hacen cosas esenciales, esto proporciona un límite absoluto inferior en el tiempo que usará el código. Por ejemplo, al optimizar un bucle, controle el tiempo del bucle vacío. Si aún es mucho tiempo cualquier intento de optimización no ayudará y deberá cambiar su diseño. Asegúrese de que el compilador no desoptimiza el bucle vacío.</p>
            </item>
            <item>
                <p>Mueva el código fuera de los bucles internos. Un trozo de código más complicado que se ejecuta una sola vez es mucho más rápido que un trozo de código más simple que se ejecuta mil veces. Evite llamar habitualmente a código lento.</p>
            </item>
            <item>
                <p>Proporcione al compilador tantos sugerencias como le sea posible. Use la palabra clave «const». Use <code>G_INLINE_FUNC</code> para funciones cortas frecuentemente llamadas. Busque <code>G_GNUC_PURE</code>, <code>G_LIKELY</code> y otras macros misceláneas de glib. Use las macros en lugar de palabras clave específicas de gcc para asegurar la portabilidad.</p>
            </item>
            <item>
                <p>No use lenguajes de ensamblado. El código no es portable y puede ser más rápido en un procesador que en otro, además no está garantizado que sea rápido en cada procesador que soporta esa arquitectura (ej. Athlon contra Pentium 4).</p>
            </item>
            <item>
                <p>No reescriba una rutina existente de una biblioteca a no ser que esté seguro de que es demasiado lenta. Muchas rutinas de bibliotecas de uso intensivo por la CPU ya se han optimizado. Algunas rutinas de bibliotecas son lentas, especialmente aquellas que realizan llamadas al sistema operativo.</p>
            </item>
            <item>
                <p>Minimice el número de bibliotecas a las que enlaza. Cuanto menor sea el número de bibliotecas que enlazar, más rápido se iniciará el programa. Es una tarea difícil en GNOME.</p>
            </item>
	    </list>
        </item>
	<item>
          <title>Trucos de alto nivel</title>
          <list type="ordered">
            <item>
                <p>La ventaja de la concurrencia. Esto no significa simplemente usar varios procesadores, también significa aprovecharse del tiempo que el usuario pasa pensando en qué harán después, para realizar algunos cálculos anticipados. Haga cálculos mientras se espera la carga de datos desde el disco. Aprovéchese de los recursos múltiples, úselos todos a la vez.</p>
            </item>
            <item>
                <p>Engañe. El usuario sólo tiene que pensar que el equipo es rápido, no importa si realmente lo es o no. Es el tiempo entre el comando y la respuesta es lo que importa, no importa si la respuesta estaba precalculada, cacheada o se trabajará en ella en cualquier momento posterior conveniente, siempre que el usuario obtenga lo que espera.</p>
            </item>
            <item>
                <p>Realice trabajo en el bucle de inactividad. Es más fácil de programar que usar programación completa para multihilo pero aún así las cosas se hacen fuera de la vista del usuario. No obstante, sea cuidadoso, si se queda demasiado tiempo en el bucle de inactividad su programa acabará siendo lento. Por eso debe devolver regularmente el control al bucle principal.</p>
            </item>
            <item>
                <p>Si todo lo demás falla, dígale al usuario que el código va a ser lento y ponga una barra de progreso. No estarán tan contentos como si hubiese mostrado el resultado pero al menos sabrán que el programa no se ha colgado y que se pueden ir a preparar un café.</p>
            </item>
	  </list>
        </item>
      </terms>
    </section>
  </page>
