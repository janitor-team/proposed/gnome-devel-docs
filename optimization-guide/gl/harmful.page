<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="harmful" xml:lang="gl">
    <info>
     <link type="guide" xref="index#harm"/>
    
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2020</mal:years>
    </mal:credit>
  </info>
    <title>Buscas de disco consideradas dañinas</title>
    <p>As buscas de disco son unha das operacións máis caras que pode realizar. Pode que non saiba isto simplemente mirando cantas se realizan, pero crea que realmente o son. Por iso, evite os seguintes comportamentos:</p>
    <list type="unordered">
        <item>
            <p>Localizar montóns de pequenos ficheiros por todo o disco.</p>
        </item>
        <item>
            <p>Abrir, iniciar e ler montóns de ficheiros por todo o disco</p>
        </item>
        <item>
            <p>Realizar o anterior sobre ficheiros que se abren en diferentes momentos, para asegurarse de que están fragmentados e causan aínda máis buscas no disco.</p>
        </item>
        <item>
            <p>
                Doing the above on files that are in different directories, so as to ensure that they are in different cylinder groups and cause even more seeking.
            </p>
        </item>
        <item>
            <p>Realizar de forma repetida o anterior cando só se precisa realizar unha vez.</p>
        </item>
    </list>
    <p>Formas nas que pode optimizar o seu código para que sexa amigábel á hora de facer buscas:</p>
    <list type="unordered">
        <item>
            <p>Consolidar os datos nun só ficheiro.</p>
        </item>
        <item>
            <p>Manter os datos xuntos no mesmo cartafol.</p>
        </item>
        <item>
            <p>Cachear os datos para non ter que volver a lelos constantemente.</p>
        </item>
        <item>
            <p>Compartir os datos para non ter que volver a lelos do disco cada vez que unha aplicación se carga.</p>
        </item>
        <item>
            <p>
                Consider caching all of the data in a single binary file that is properly aligned and can be mmapped.
            </p>
        </item>
    </list>
    <p>O problema coas buscas nos discos complícase para as lecturas, que é desafortunadamente o que se busca. Lembre, as lecturas son xeralmente síncronas mentres que as escrituras son asíncronas. Isto só complica o problema, serializando cada lectura e contribuíndo á latencia do programa.</p>
</page>
