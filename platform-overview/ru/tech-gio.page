<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-gio" xml:lang="ru">

  <info>
    <link type="guide" xref="tech" group="gio"/>
    <revision pkgversion="3.0" date="2011-04-05" status="review"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011</years>
    </credit>
    <credit type="copyright editor">
      <name>Federico Mena Quintero</name>
      <email its:translate="no">federico@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>File and URI handling, asynchronous file operations, volume
    handling</desc>
  </info>

<title>GIO для файлов</title>

<p>GIO provides APIs for asynchronously reading and writing files and
other streams. Files are referenced by URIs (uniform resource identifiers), and backends can
provide access to more than just local files. When running under the
GNOME desktop, GIO uses GVFS to allow access to files over SFTP, FTP,
WebDAV, SMB, and other popular protocols. This transparent network
file access is free to all applications using GIO.</p>

<p>Файловый программный интерфейс GIO проектировался для использования в графических интерфейсах, управляемых событиями. Его неблокирующая, асинхронная архитектура не приводит к «замиранию» пользовательского интерфейса при выполнении операций с файлами. Также доступны синхронные версии программного интерфейса, которые, иногда, более удобны при использовании в процессах обрабатывающих нитей.</p>

<p>Также GIO предоставляет процедуры для работы с устройствами и томами, получение типов файлов и значков, и поиск приложений для открытия файлов.</p>

<list style="compact">
  <item><p><link href="http://developer.gnome.org/gio/stable/">GIO Reference Manual</link></p></item>
</list>

</page>
