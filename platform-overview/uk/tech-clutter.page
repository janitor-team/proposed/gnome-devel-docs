<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-clutter" xml:lang="uk">

  <info>
    <link type="guide" xref="tech" group="clutter"/>
    <revision pkgversion="3.0" date="2011-04-05" status="incomplete"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011–2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Анімація та сценічна графіка</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Максим Дзюманенко</mal:name>
      <mal:email>dziumanenko@gmail.com</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Clutter</title>

<p>Clutter — бібліотека для створення анімацій і використання напівпросторових полотен. За її допомогою ви можете створювати графічні об'єкти із зображень та працювати з ними з метою пересування, обертання та надання їм квазіпросторового вигляду.</p>

<p>У Clutter використано стандартні для індустрії програмні інтерфейси <link href="http://www.khronos.org/opengl/">OpenGL</link> та <link href="http://www.khronos.org/opengles/">OpenGL|ES</link> для доступу до апаратного прискорення показу графіки у середовищах робочих станцій та мобільних пристроїв. Бібліотека надає змогу уникнути складної роботи із конвеєрним програмуванням для графічних процесорів.</p>

<p>Clutter не визначає жодного візуального стилю і не надає жодних складних попередньо визначених засобів керування інтерфейсом користувача — бібліотека просто забезпечує для розробника можливість визначення потрібних йому засобів за допомогою гнучкого програмного інтерфейсу сценічної графіки із довільним розташуванням елементів сцени (або <em>акторів</em>) у основній області перегляду (або <em>сцені</em>).</p>

  <p>Clutter постачається із попередньо визначеними акторами для показу суцільних кольорів, даних зображень, тексту та нетипових високоточних плоских креслень за допомогою програмного інтерфейсу <link xref="tech-cairo">Cairo</link>. Крім того, у Clutter реалізовано загальні класи для структурування інтерфейсу користувача за допомогою одразу моделі пакування прямокутників, подібної до <link xref="tech-gtk">GTK</link>, та послідовності <em>обмежень</em> довільної форми.</p>

<p>Clutter надає у ваше розпорядження придатну до розширення бібліотеку анімації та графічних ефектів. Анімацію прив'язано до шкали часу і зміни однієї або декількох властивостей одного або декількох акторів з часом, наприклад, їхнього обертання у певному вимірі, масштабування, розміру, непрозорості тощо.</p>

<p>Декілька сторонніх бібліотек уможливлюють інтеграцію із іншими технологіями, зокрема, Clutter-GTK, для вбудовування сцени Clutter до програми на основі GTK; Clutter-GStreamer, для вбудовування конвеєрів обробки відео та звуку GStreamer; Clutter-Box2D і Clutter-Bullet, для додавання фізичної взаємодії у середовищах дво- та тривимірної графіки.</p>

<list style="compact">
  <item><p><link href="https://developer.gnome.org/clutter-cookbook/stable/">Кухарська книга з Clutter</link></p></item>
  <item><p><link href="http://developer.gnome.org/clutter/stable">Довідник-підручник з Clutter</link></p></item>
  <item><p><link href="http://www.clutter-project.org">Сайт Clutter</link></p></item>
</list>

</page>
