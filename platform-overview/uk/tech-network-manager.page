<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-network-manager" xml:lang="uk">

  <info>
    <link type="guide" xref="tech" group="network-manager"/>

    <credit type="author copyright">
      <name>Federico Mena Quintero</name>
      <email its:translate="no">federico@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Керування з'єднаннями мережі і спостереження за станом «у мережі»/«поза мережею»</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Максим Дзюманенко</mal:name>
      <mal:email>dziumanenko@gmail.com</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>NetworkManager</title>

  <p>NetworkManager керує з'єднаннями комп'ютера із мережею. Він може виконувати дії з узгодження DHCP для отримання IP-адреси для комп'ютера під час початкової активації мережі. NetworkManager надає користувачам змогу вибирати між різними дротовими та бездротовими мережами, налаштовувати віртуальні приватні мережі (VPNs) та встановлювати з'єднання із мережами за допомогою модемів.</p>

  <p>NetworkManager надає у ваше розпорядження багатий на можливості програмний інтерфейс, за допомогою якого програми можуть керувати з'єднаннями мережі. Втім, цей інтерфейс буде цікавим лише для розробників програмного забезпечення, яке реалізує базові можливості стільничного середовища. Звичайні програми можуть користуватися програмним інтерфейсом NetworkManager для спостереження за станом «у мережі»-«поза мережею» для комп'ютера і виконання інших високорівневих завдань, які пов'язано із мережею.</p>

  <p>Базова оболонка стільниці у GNOME містить добре помітну піктограму NetworkManager. На внутрішньому рівні вона використовує програмний інтерфейс NetworkManager для зміни параметрів роботи мережі на основі вибору користувача. Крім того, NetworkManager використовують програми, зокрема Evolution, яким потрібні дані щодо стану роботи комп'ютера у мережі.</p>

  <list style="compact">
    <item><p><link href="https://wiki.gnome.org/Projects/NetworkManager">Домашня сторінка NetworkManager</link></p></item>
    <item><p><link href="https://wiki.gnome.org/Projects/NetworkManager/Developers">Довідник з програмного інтерфейсу NetworkManager</link></p></item>
  </list>


</page>
