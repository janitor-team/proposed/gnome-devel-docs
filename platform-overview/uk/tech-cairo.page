<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-cairo" xml:lang="uk">

  <info>
    <link type="guide" xref="tech" group="cairo"/>
    <revision pkgversion="3.0" date="2011-04-05" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011–2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Двовимірне малювання на основі векторів для високоякісної графіки</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Максим Дзюманенко</mal:name>
      <mal:email>dziumanenko@gmail.com</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Cairo</title>

<p>Cairo — бібліотека для роботи із двовимірною графікою, у якій реалізовано продуманий програмний інтерфейс для малювання векторної графіки, компонування зображень та показу тексту зі згладжуванням. У Cairo реалізовано підтримку роботи із декількома пристроями виведення даних, зокрема системою керування вікнами X, Microsoft Windows та буферами зображень у пам'яті, уможливлюючи для вас написання незалежного від програмної платформи коду для малювання графіки на різних носіях.</p>

<p>Модель малювання Cairo є подібною до тієї, яку реалізовано у PostScript і PDF. У програмному інтерфейсі Cairo передбачено функції для таких дій з малювання, як штрихування та заповнення кубічними сплайнами Безьє, компонування зображень та виконання афінних перетворень. За допомогою цих векторних операцій можна отримувати красиву згладжену графіку.</p>

<p>Багата на можливості модель малювання Cairo забезпечує високоякісний показ даних на різних носіях. Тим самим програмним інтерфейсом можна скористатися для створення графіки і тексту на екрані, для показу зображень або для створення чітких зображень для друку.</p>

<p>Вам слід використовувати Cairo кожного разу, коли ви малюєте графіку у вашій програмі поза межами віджетів, які надаються у ваше розпорядження GTK. Майже усі дії з малювання у GTK виконуються із використанням Cairo. Використання Cairo для ваших нетипових креслень надасть вашій програмі змогу показувати високоякісну, згладжену та незалежну від роздільної здатності графіку.</p>

<list style="compact">
  <item><p><link href="http://www.cairographics.org/manual/">Підручник з Cairo</link></p></item>
  <item><p><link href="http://www.cairographics.org">Сайт Cairo</link></p></item>
</list>
</page>
