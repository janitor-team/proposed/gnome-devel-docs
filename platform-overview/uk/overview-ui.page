<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="overview-ui" xml:lang="uk">
  <info>
    <link type="guide" xref="index" group="ui"/>
    <revision version="0.1" date="2013-06-19" status="stub"/>

    <credit type="author copyright">
      <name>Майкл Гілл (Michael Hill)</name>
      <email its:translate="no">mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Стандартні елементи інтерфейсу користувача, обробка і анімація.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Максим Дзюманенко</mal:name>
      <mal:email>dziumanenko@gmail.com</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Інтерфейс користувача і графіка</title>

<list>
 <item>
  <p><em style="strong">Використання єдиного набору інструментів для усіх стандартних віджетів у вашій програмі</em></p>
 </item>
 <item>
  <p><em style="strong">Створення швидкого візуально багатого графічного інтерфейсу</em></p>
 </item>
 <item>
  <p><em style="strong">Високоякісна, згладжена та незалежна від роздільності екрана графіка</em></p>
 </item>
 <item>
  <p><em style="strong">Просте додавання інтернет-можливостей до вашої програми</em></p>
 </item>
 <item>
  <p><em style="strong">Доступ до вбудованих допоміжних технологій</em></p>
 </item>
</list>

<p>Скористайтеся потужними основами платформи GNOME для створення однорідних і гнучких інтерфейсів. Зробіть так, щоб ваші програми були доступними якнайширшій аудиторії, розгорнувши їх на інших платформах. Стандартні елементи інтерфейсу користувача є типово доступними. Ви також можете доволі просто додати підтримку доступності до будь-яких створених вами нетипових елементів інтерфейсу користувача.</p>

<section id="what">
 <title>Що робити?</title>

  <p>Для програм із <em style="strong">стандартними елементами керування</em>, які будуть звичними для більшості користувачів, скористайтеся <em style="strong"><link xref="tech-gtk">GTK</link></em>. Будь-яка програма, яка є частиною GNOME, використовує GTK, тому вам слід використовувати саме цей набір бібліотек для того, щоб ваша програма була схожою на інші і мала доступ до багатьох віджетів, можливостей, зокрема підтримки друку, та тем у форматі CSS.</p>

  <p>Створювати <em style="strong">анімації, ефекти та красиві компонування</em> дуже просто, якщо користуватися <em style="strong"><link xref="tech-clutter">Clutter</link></em>. Крім того, у цій бібліотеці передбачено підтримку сенсорного введення та жестів.</p>

  <p><em style="strong">Високоякісна, згладжена та незалежна від роздільної здатності плоска графіка</em> забезпечується бібліотекою <em style="strong"><link xref="tech-cairo">Cairo</link></em>. Cairo використовується для малювання віджетів у GTK. Її також можна використати для виведення даних у форматах PDF та SVG.</p>

  <p><em style="strong"><link xref="tech-webkit">WebKitGTK</link></em> спрощує додавання <em style="strong">вебможливостей</em> до вашої програми — від показу вмісту файла HTML до забезпечення роботи повноцінного інтерфейсу користувача у HTML5.</p>

  <p>У GTK, Clutter і WebKitGTK є <em style="strong">вбудована підтримка допоміжних технологій</em> на основі <em style="strong"><link xref="tech-atk">ATK</link></em>. Скористайтеся Orca, Caribou OSK та вбудованими засобами доступності GTK або побудуйте нетипові інструменти на основі ATK.</p>

</section>

<!-- TODO Link to code examples.
<section id="samples">
 <title>Code samples</title>
 <list>
  <item><p>A sample we should write</p></item>
  <item><p><link xref="samples">More…</link></p></item>
 </list>
</section>
-->

<section id="realworld">
 <title>Приклади з реального життя</title>

  <p>Технології інтерфейсу користувача GNOME застосовуються у багатьох проєктах із відкритим кодом. Деякі з прикладів наведено нижче.</p>
  <list>
  <item>
    <p><em style="strong">Тенета</em> — браузер GNOME, у якому використано GTK і WebKitGTK. У цьому браузері повністю реалізовано можливості доступності.</p>
    <p>( <link href="https://wiki.gnome.org/Apps/Web">Сайт</link> | <link href="https://gitlab.gnome.org/GNOME/epiphany/">Початковий код</link> )</p>
  </item>
  <item>
    <p><em style="strong">MonoDevelop</em> — багатоплаформове комплексне середовище розробки, яке створено для роботи із мовою програмування C# та іншими мовами .NET. Воно працює у Linux, Mac OS X та Windows.</p>
    <p>( <link href="https://www.monodevelop.com/">Сайт</link> | <link href="https://www.monodevelop.com/screenshots/">Знімки вікон</link> | <link href="https://github.com/mono/monodevelop">Початковий код</link>)</p>
  </item>
  <item>
    <p><em style="strong">Відео</em> — мультимедійний програвач GNOME, у якому використано Clutter для показу відеоданих.</p>
    <p>(<link href="https://wiki.gnome.org/Apps/Videos">Сайт</link> | <link href="https://gitlab.gnome.org/GNOME/totem/">Початковий код</link>)</p>
  </item>
 </list>
</section>
</page>
