<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-gdk" xml:lang="sv">

  <info>
    <link type="guide" xref="tech" group="gdk"/>
    <revision pkgversion="3.0" date="2011-04-05" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011–2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Lågnivåabstraktion för fönstersystemet</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2007</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

<title>GDK</title>

  <p>GDK är lågnivåbiblioteket som används av <link xref="tech-gtk">GTK</link> för att interagera med fönstersystemet för grafik och indataenheter. Även om du sällan kommer att använda GDK direkt i programkod så innehåller det all nödvändig funktionalitet för att skapa lågnivåfönster på skärmen och för att interagera med användaren med olika inmatningsenheter. GDK agerar som en abstraktion över diverse fönstersystem, så att GTK kan porteras till dem alla: X Window System (X11), Microsoft Windows, Mac OS X Quartz.</p>

<p>GDK låter dig komma åt händelser från tangentbord, möss och andra inmatningsenheter. Implementationer av komponenter i GTK använder denna funktionalitet och översätter händelserna till signaler på högre nivå som i sin tur kan användas från programkod. Till exempel kommer en <code>GtkButton</code>-komponent att spåra <code>GDK_BUTTON_PRESS</code>- och <code>GDK_BUTTON_RELEASE</code>-händelser, vilka kommer från musen, och översätta dem som lämpligt till en <code>GtkButton::clicked</code>-signal när användaren trycker ner och släpper upp knappen på rätt plats.</p>

<p>GDK tillhandahåller också lågnivårutiner för att komma åt dra-och-släpp och urklippsdata från systemet. Då anpassade kontroller implementeras kan du behöva komma åt dessa funktioner för att implementera lämpligt beteende för användarinteraktion.</p>

<p>GDK tillhandahåller annan funktionalitet som behövs för att implementera en fullständig grafisk verktygslåda som GTK. Eftersom GDK agerar som en plattformsabstraktion, vilken låter GTK köra under flera miljöer, så tillhandahåller den ett API för all systemfunktionalitet som behövs av GTK. Detta inkluderar information om visning över flera skärmar, upplösning och färgdjup, färgkartor och markörer.</p>

  <p>Du bör använda GDK närhelst du behöver lågnivååtkomst till det underliggande fönstersystemet, inklusive lågnivååtkomst till händelser, fönster och urklipp. Att använda GDK för dessa uppgifter säkerställer att din kod är portabel och integrerad med resten av din GTK-kod. De enkla utritningsrutinerna i GDK bör i allmänhet inte användas, dessa är en kvarleva från när GDK helt enkelt var ett omslag till fönstersystemets utritningsprimitiver. Istället bör du använda den omfattande funktionalitet som tillhandahålls av <link xref="tech-cairo">Cairo</link> för att rita 2D-grafik med hög kvalitet.</p>

<list style="compact">
  <item><p><link href="https://developer.gnome.org/gdk3/stable/">GDK-referenshandbok</link></p></item>
</list>
</page>
