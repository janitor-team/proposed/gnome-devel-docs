<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="overview-net" xml:lang="sv">
  <info>
    <link type="guide" xref="index" group="net"/>
    <revision version="0.1" date="2013-08-06" status="review"/>

    <credit type="author copyright">
      <name>David King</name>
      <email its:translate="no">davidk@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>HTTP-kommunikation för klient och server, portabel uttagsbaserad nätverks-IO, och nätverksenhetshantering.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2007</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

<title>Lågnivå-nätverk</title>

<list>
 <item>
  <p><em style="strong">Skapa kraftfulla och flexibla HTTP-servrar och klienter</em></p>
 </item>
 <item>
  <p><em style="strong">Använd portabla uttagsbaserade API:er i ett användargränssnitt utan att blockera</em></p>
 </item>
 <item>
  <p><em style="strong">Upptäck och hantera nätverkets anslutningstillstånd</em></p>
 </item>
</list>

<p>Dra fördel av de portabla nätverks-API:erna för att komma åt nätverkstjänster. Asynkron inmatning/utmatning behåller ditt programs användargränssnitt kvickt att reagera under tiden inmatning/utmatning pågår. Upptäck skillnader i systemets nätverkstillstånd så att ditt program kan svara adekvat då det inte finns någon internetåtkomst.</p>

<!-- TODO: Add screenshot.
<media type="image" mime="image/png" src="test_comm1.png" width="65%">
 <p>IMAGE</p>
</media>
-->

<section id="what">
  <title>Vad kan du göra?</title>

  <p>För att <em style="strong">asynkront komma åt lågnivå-API:er för nätverk</em>, använd <em style="strong" xref="tech-gio-network">GIO-nätverk</em>. API:er på högre nivå finns tillgängliga för att <em style="strong">slå upp proxyservrar och DNS-poster</em> såväl som att använda <em style="strong">säkra uttag (TLS)</em>.</p>

  <p>Enkel övervakning av nätverkstillstånd finns tillgänglig i GIO, men <em style="strong" xref="tech-network-manager">Nätverkshanteraren</em> tillhandahåller <em style="strong">omfattande stöd för nätverksenheter</em> och nätverkstopologier.</p>

  <p><em style="strong" xref="tech-soup">Libsoup</em> tillhandahåller ett flexibelt gränssnitt för <em style="strong">HTTP-servrar och klienter</em>. Både synkrona och asynkrona API:er tillhandahålls.</p>

</section>

<!-- TODO Add link to code examples.
<section id="samples">
 <title>Code samples</title>
 <list>
  <item><p>A sample we should write</p></item>
  <item><p><link xref="samples">More…</link></p></item>
 </list>
</section>
-->

<section id="realworld">
  <title>Exempel från verkligheten</title>

  <p>Du kan se många faktiska tillämpningar av GNOME-nätverksteknologier i öppna källkodsprojekt, så som exemplen nedan.</p>
  <list>
    <item>
      <p><em style="strong">Webb</em> är GNOME:s webbläsare, vilken använder libsoup för åtkomst till HTTP-tjänster.</p>
      <p>( <link href="https://wiki.gnome.org/Apps/Web">Webbplats</link> | <link href="https://gitlab.gnome.org/GNOME/epiphany/raw/master/data/screenshot.png">Skärmbild</link> | <link href="https://gitlab.gnome.org/GNOME/epiphany/">Källkod</link> )</p>
    </item>
    <item>
      <p><em style="strong">GNOME Shell</em> är det användarsynliga GNOME-skrivbordet, vilket använder Nätverkshanteraren för nätverksstatusmenyn, inklusive hantering av trådbundna och trådlösa nätverk, 3G-modem och VPN-nätverkssystem.</p>
      <p>( <link href="https://wiki.gnome.org/Projects/GnomeShell">Webbplats</link> | <link href="http://www.gnome.org/gnome-3/">Skärmbild</link> | <link href="https://gitlab.gnome.org/GNOME/gnome-shell/">Källkod</link> )</p>
    </item>
    <!-- TODO: Add low-level GIO network IO example. -->
  </list>
</section>
</page>
