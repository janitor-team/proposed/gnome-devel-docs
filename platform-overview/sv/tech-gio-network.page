<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-gio-network" xml:lang="sv">

  <info>
    <link type="guide" xref="tech" group="gio-network"/>
    <revision pkgversion="3.0" date="2011-04-05" status="review"/>

    <credit type="author copyright">
      <name>Federico Mena Quintero</name>
      <email its:translate="no">federico@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Nätverks- och uttags-API med strömmar</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2007</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

<title>GIO-nätverk</title>

  <p>GIO-nätverk är byggt ovanpå ström-API:erna som <link xref="tech-gio">används för filer</link>. Det tillhandahåller högnivå-API:er för att kommunicera över TCP/IP och UNIX-domänuttag. Du kan använda GIO-nätverks-API:erna för att ansluta till en server, lyssna efter händelser, och läsa resurser. Det asynkrona API:t betyder att ditt program inte blockerar under tiden det väntar på ett svar från nätverket.</p>

<list style="compact">
  <item><p><link href="https://developer.gnome.org/gio/stable/">GIO-referenshandbok</link></p></item>
  <item><p><link href="https://developer.gnome.org/gio/stable/networking.html">Lågnivåstöd för nätverk</link></p></item>
  <item><p><link href="https://developer.gnome.org/gio/stable/highlevel-socket.html">Högnivåfunktionalitet för nätverk</link></p></item>
</list>

</page>
