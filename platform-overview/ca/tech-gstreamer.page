<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-gstreamer" xml:lang="ca">

  <info>
    <link type="guide" xref="tech" group="gstreamer"/>
    <revision pkgversion="3.0" date="2011-04-05" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011–2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Playing, mixing, and manipulating sound and video</desc>
  </info>

<title>GStreamer</title>

<p>La GStreamer és una biblioteca multimèdia molt potent per reproduir, crear i manipular so, vídeo i d'altres elements multimèdia. Podeu utilitzar al GStreamer per reproduir so i vídeo, per enregistrar les entrades de múltiples fonts i editar continguts multimèdia. La GStreamer, per defecte, permet codificar i descodificar un gran nombre de formats, els altres formats també es poden gestionar gràcies a connectors addicionals.</p>

<p>La GStreamer proporciona una arquitectura molt flexible amb la qual un element multimèdia és processat a través d'un conducte d'elements. Cada element pot aplicar filtres al contingut, com ara codificar-lo o descodificar-lo, combinar-lo amb altres fonts o transformar-lo. Aquesta arquitectura permet disposar els elements de forma totalment arbitrària, de manera que qualsevol efecte és possible amb la GStreamer. La GStreamer s'ha dissenyat per tal que tingui molt poc impacte en els recursos, de manera que es pot utilitzar en aplicacions que requereixin una latència molt baixa.</p>

<p>Encara que la GStreamer proporciona unes API molt potents per manipular continguts multimèdia, també proporciona utilitats simples per a la reproducció. La GStreamer pot construir automàticament un conducte per llegir i reproduir fitxers de qualsevol format que sigui conegut, de manera que permetre reproduir àudio i vídeo a les aplicacions sigui molt senzill.</p>

<p>L'arquitectura de la GStreamer permet que els connectors afegeixin codificadors, descodificadors i qualsevol tipus de filtre de continguts. Qualsevol desenvolupador pot crear connectors de GStreamer que automàticament estaran disponibles a qualsevol altra aplicació que també utilitzi la GStreamer. Els connectors poden gestionar nous formats de fitxer o proporcionar funcions i efectes addicionals.</p>

<p>Hauríeu d'utilitzar la GStreamer sempre la vostra aplicació hagi de llegir o reproduir continguts multimèdia o hagi de manipular so i vídeo. En utilitzar la GStreamer facilitareu el desenvolupament de l'aplicació i podreu disposar d'un gran nombre d'elements molt ben provats que compliran amb les vostres necessitats.</p>

<list style="compact">
  <item><p><link href="http://gstreamer.freedesktop.org/data/doc/gstreamer/head/manual/html/index.html">The GStreamer Application Development Manual</link></p></item>
  <item><p><link href="http://gstreamer.freedesktop.org/data/doc/gstreamer/head/gstreamer/html/">The GStreamer 1.0 Core Reference Manual</link></p></item>
  <item><p><link href="http://gstreamer.freedesktop.org/documentation/">The GStreamer documentation page</link></p></item>
  <item><p><link href="http://gstreamer.freedesktop.org">The GStreamer web site</link></p></item>
</list>

</page>
