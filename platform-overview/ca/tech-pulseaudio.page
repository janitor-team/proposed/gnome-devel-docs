<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-pulseaudio" xml:lang="ca">

  <info>
    <link type="guide" xref="tech" group="pulseaudio"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Low-level audio API</desc>
  </info>

<title>PulseAudio</title>

  <p>
    PulseAudio is GNOME's low-level audio API.  It is what sits between
    applications and the kernel's audio API.  PulseAudio lets you re-route
    sound through the network, or to a Bluetooth headset.  It can mix several
    sources of audio, or change the sample rate of an audio stream.
  </p>

  <p>All the parts of GNOME that produce audio use PulseAudio in one way or
  another, either directly, or indirectly through higher-level sound
  manipulation APIs like <link xref="tech-gstreamer">GStreamer</link>.</p>

  <list style="compact">
    <item><p><link href="http://www.freedesktop.org/wiki/Software/PulseAudio">PulseAudio home page</link></p></item>
  </list>

</page>
