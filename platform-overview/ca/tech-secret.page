<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-secret" xml:lang="ca">

  <info>
    <link type="guide" xref="tech" group="secret"/>
    <revision pkgversion="3.0" date="2011-04-05" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Emmagatzematge segur per a les contrasenyes i altres dades</desc>
  </info>

<title>Secret</title>

<p>GNOME uses libsecret as a secure keyring manager, to store users'
passwords and other sensitive data.  Applications can use the keyring
manager library to store and access passwords, and users can manage
their passwords using GNOME's <app>Seahorse</app>
application.</p>

<p>El gestor d'anells de claus permet utilitzar tants anells de claus com facin falta, a més a cada anell hi poden haver tants elements com sigui necessari. Els elements d'un anell de claus emmagatzemen una dada, normalment una contrasenya. Cada anell de claus es bloqueja individualment i l'usuari ha de proporcionar una contrasenya per desbloquejar-lo. Un cop l'usuari l'hagi desbloquejat podrà accedir als elements que contingui l'anell de claus.</p>

<p>El gestor d'anells de claus proporciona llistes de control d'accés per a cada element de l'anell de claus, controlant quines aplicacions estan autoritzades a accedir a cada element. Si una aplicació desconeguda intenta accedir a l'element de l'anell de claus, el gestor de l'anell de claus demanarà a l'usuari si li vol permetre o denegar l'accés. Aquest mecanisme permet evitar que programes maliciosos o senzillament mal escrits puguin accedir a les dades privades de l'usuari.</p>

<p>Les dades emmagatzemades al sistema de fitxers per l'anell de claus s'encripten amb el criptògraf de bloc AES i s'utilitza el mètode SHA1 per els resums dels atributs dels elements. Gràcies a l'ús dels resums dels atributs, el gestor de l'anell de claus pot consultar els elements que sol·liciten les aplicacions sense ni tan sols haver de desblocar l'anell de claus. S'ha de desblocar l'anell de claus quan es troba un element que coincideix i s'hi vol accedir.</p>

<p>El gestor de l'anell de claus també proporciona un anell de claus de la sessió. Els elements de l'anell de claus de la sessió no es desen mai a disc i es perden de seguida que s'acaba la sessió de l'usuari. L'anell de claus de la sessió es pot utilitzar per emmagatzemar contrasenyes que s'utilitzin durant la sessió actual i prou.</p>

  <p>If you use <link xref="tech-gio">GIO</link> to access remote servers, you
  automatically get the benefits of the keyring manager. Whenever GIO needs to
  authenticate the user, it provides the option to store the password, either
  in the default keyring or in the session keyring.</p>

<p>You should use libsecret's keyring manager whenever your application needs
to store passwords or other sensitive data for users.  Using the keyring
manager provides a better user experience while still keeping user data
safe and secure.</p>

  <note>
    <p>
      GNOME used a library called gnome-keyring before version 3.6 was
      released.  In version 3.6 onward, libsecret is used instead.  This allows
      sharing the keyring service between GNOME and other desktop environments
      and applications.
    </p>
  </note>

<list style="compact">
  <item><p><link href="http://developer.gnome.org/libsecret/unstable/">Libsecret Reference Manual</link></p></item>
</list>

</page>
