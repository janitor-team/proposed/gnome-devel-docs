<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dev-launching-startupnotify" xml:lang="es">

  <info>
    <link type="next" xref="dev-launching-mime"/>
    <revision version="0.1" date="2014-01-28" status="draft"/>

    <credit type="author">
      <name>David King</name>
      <email its:translate="no">davidk@gnome.org</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011-2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2007-2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Carrión</mal:name>
      <mal:email>mario@monouml.org</mal:email>
      <mal:years>2006</mal:years>
    </mal:credit>
  </info>

  <title>Notificación de inicio</title>

  <links type="series" style="floatend">
    <title>Lanza su aplicación</title>
  </links>

  <p>Notifica al usuario cuando la aplicación ha terminado de cargarse.</p>

  <p>GNOME implements the
  <link href="https://standards.freedesktop.org/startup-notification-spec/startup-notification-latest.txt">Startup
  Notification protocol</link>, to give feedback to the user when application
  startup finishes.</p>

  <p>GTK applications automatically support startup notification, and by
  default notify that application startup is complete when the first window is
  shown. Your application must declare that it supports startup notification by
  adding <code>StartupNotify=true</code> to its desktop file.</p>

  <p>Algunos escenarios de inicio más complicados, como puede ser mostrar una imagen durante el inicio, se deben gestionar de manera personalizada con <code href="https://developer.gnome.org/gdk3/stable/gdk3-General.html#gdk-notify-startup-complete">gdk_notify_startup_complete()</code>.</p>

</page>
