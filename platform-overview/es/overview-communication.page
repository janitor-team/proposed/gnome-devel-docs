<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="overview" id="overview-communication" xml:lang="es">
  <info>
    <revision version="0.1" date="2012-02-19" status="stub"/>
    <link type="guide" xref="index" group="communication"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email its:translate="no">philbull@gmail.com</email>
      <years>2012</years>
    </credit>

    <title type="link" role="trail">Comunicación</title>
    <desc>Soporte para mensajería instantánea, redes, redes sociales, correo-e y calendario.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011-2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2007-2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Carrión</mal:name>
      <mal:email>mario@monouml.org</mal:email>
      <mal:years>2006</mal:years>
    </mal:credit>
  </info>

<title>Comunicación y redes sociales</title>

<list>
 <item>
  <p><em style="strong">Conectarse a servicios de mensajería instantánea y de redes sociales</em></p>
 </item>
 <item>
  <p><em style="strong">Configurar conexiones multiprotocolo con servicios web u otros clientes</em></p>
 </item>
 <item>
  <p><em style="strong">Gestionar servicios de correo, contactos en línea y calendario</em></p>
 </item>
</list>

<p>Haga que sus usuarios estén conectados y se comuniquen con sus amigos y contactos mediante mensajería instantánea, medios sociales y correo-e. La pila de comunicaciones ampliable de GNOME le proporciona un acceso abstracto de alto nivel a protocolos de correo-e y mensajería instantánea complicados. Para necesidades de comunicación más especializada, también puede acceder a los entresijos mediante API de bajo nivel.</p>

<media type="image" mime="image/png" src="test_comm1.png" width="65%">
 <p>Cliente de mensajería instantánea Empathy</p>
</media>

<section id="what">
 <title>¿Qué puede hacer?</title>

 <p>Use <em style="strong">Telepathy</em> para <em style="strong">conectarse a servicios de mensajería instantánea</em>. Proporciona un potente marco de trabajo para interactuar con los contactos de mensajería instantánea del usuario y soporta un amplio rango de protocolos de mensajería. Con Telepathy, todas las cuentas y las conexiones se gestionan con un demonio de sesión que está ampliamente integrado en GNOME. Las aplicaciones pueden usar este servicio para comunicarse con los contactos.</p>

 <p>Crear juegos para varios jugadores o editores colaborativos que se integran con los servicios de mensajería instantánea del escritorio. Con la API de <em style="strong" xref="tech-telepathy">Telepathy Tubes</em> puede <em style="strong">usar un protocolo cualquiera mediante un túnel</em> sobre los protocolos de mensajería instantánea modernos, como Jabber, para crear aplicaciones interactivas.</p>

 <p>Permitir a los usuarios ver a otras personas con las que poder chatear, buscar impresoras, archivos compartidos y colecciones de música compartidas tan pronto como se conectan a la red. La API de <em style="strong" xref="tech-avahi">Avahi</em> permite <em style="strong">descubrir servicios</em> en la red local mediante el conjunto de protocolos mDNS/DNS-SD. Es compatible con la tecnología similar de Windows y MacOS.</p>

 <p>Gestionar las libretas de direcciones locales y en línea de los usuarios con <em style="strong" xref="tech-eds">Evolution Data Server</em> (EDS). Proporciona una manera de almacenar información de la cuenta y de interactuar con ella...</p>

 <p>Con <em style="strong" xref="tech-folks">Folks</em>, tendrá acceso a una API sencilla para gestionar redes sociales, chat, correo-e y comunicaciones de sonido/vídeo.</p>

</section>

<!--<section id="samples">
 <title>Code samples</title>
 <list>
  <item><p>Change the IM status</p></item>
  <item><p>Fetch a contact from a Gmail address book</p></item>
  <item><p>Scan the network for zeroconf printers</p></item>
  <item><p>Something with Telepathy Tubes</p></item>
 </list>
</section>-->

<section id="realworld">
 <title>Ejemplos del mundo real</title>

 <p>Puede ver muchos ejemplos de aplicaciones del mundo real con tecnologías de comunicaciones de GNOME en proyectos de código abierto, como las indicadas en los siguientes ejemplos.</p>
 <list>
  <item>
   <p><em style="strong">Empathy</em> es una aplicación de mensajería instantánea que soporta un amplio rango de servicios de mensajería. usa Telepathy para gestionar las conexiones, la presencia y la información de contactos para todos los protocolos que soporta.</p>
   <p>(<link href="https://wiki.gnome.org/Apps/Empathy">Página web</link> | <link href="https://wiki.gnome.org/Apps/Empathy#Screenshots">Screenshots</link> | <link href="https://gitlab.gnome.org/GNOME/empathy/">Código fuente de Empathy</link> )</p>
  </item>

  <item>
   <p>Con el soporte de Telepathy Tubes la colección de <em style="strong">juegos de GNOME</em> pudo añadir el soporte multijugador mediante el protocolo Jabber.</p>
   <p>(<link href="https://wiki.gnome.org/Projects/Games">Página web</link> | <link href="https://wiki.gnome.org/Apps/Chess#Screenshots">Captura de pantalla</link> | <link href="https://gitlab.gnome.org/GNOME/gnome-chess/">Código de GNOME Chess multijugador en línea</link> )</p>
  </item>

  <item>
   <p>Avahi soporta que los usuarios de <em style="strong">Rhythmbox</em> vean las colecciones de música compartida en su red local, usando DAAP.</p>
   <p>(<link href="https://wiki.gnome.org/Apps/Rhythmbox">Página web</link> | <link href="https://wiki.gnome.org/Apps/Rhythmbox/Screenshots">Capturas de pantalla</link> | <link href="https://gitlab.gnome.org/GNOME/rhythmbox/tree/master/plugins/daap">Código de DAAP</link> )</p>
  </item>
 </list>

</section>

</page>
