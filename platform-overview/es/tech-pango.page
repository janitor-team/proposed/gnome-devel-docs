<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-pango" xml:lang="es">

  <info>
    <link type="guide" xref="tech" group="pango"/>
    <revision pkgversion="3.0" date="2011-04-05" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011–2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Renderizado de texto internacional con soporte completo para Unicode</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011-2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2007-2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Carrión</mal:name>
      <mal:email>mario@monouml.org</mal:email>
      <mal:years>2006</mal:years>
    </mal:credit>
  </info>

<title>Pango</title>

<p>Pango is the core text and font handling library in the GNOME
platform.  It is responsible for laying out and rendering text, and
is used throughout GTK.</p>

<note><p>El motor de dibujo Pango se puede usar con diferentes backends de tipografías y dibujado. En la mayoría de sistemas GNOME, Pango usará FreeType, fontconfig y Cairo para acceder a las tipografías y renderizar texto. En otros sistemas, Pango usará la tipografía nativa de los sistemas, tales como Uniscribe en Microsoft Windows y ATSUI en MacOs.</p></note>

<p>Pango tiene un amplio soporte para los distintos sistemas de escritura usados en todo el mundo. Muchos de los sistemas de escritura usados en los idiomas tienen complejas reglas de dibujado de glifos y composición de caracteres. Con Pango, casi todos los idiomas pueden escribirse y mostrarse correctamente, permitiendo a los usuarios en cualquier parte ver texto en sus idiomas maternos. El soporte de Pango para múltiples sistemas de escritura es automático; los desarrolladores de aplicaciones no tienen que escribir ningún código especial para soportar otros idiomas.</p>

<p>Pango supports the kind of text styling used in typical documents
and interfaces, including italics, font weights, and underlines.
Pango uses a simple XML-like vocabulary called PangoMarkup which
enables you to set font size, color, styles, and other text
attributes.  Using PangoMarkup, you can specify inline styles
without manually iterating over text blocks.  PangoMarkup can
be used directly from GTK, enabling you to style text in your
graphical interfaces easily.</p>

<p>You should use Pango directly whenever you need to lay
text out on the screen or on a different medium.  Using Pango
will allow your text layout to work seamlessly with GTK and
the rest of the GNOME platform.  It will help you create
portable code, and most importantly, it will ensure that your
application can render text correctly in hundreds of different
languages.</p>

<list style="compact">
  <item><p><link href="https://developer.gnome.org/pango/stable/">Manual de referencia de GIO</link></p></item>
  <item><p><link href="http://www.pango.org">El sitio web de Pango</link></p></item>
</list>
</page>
