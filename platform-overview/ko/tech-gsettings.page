<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-gsettings" xml:lang="ko">

  <info>
    <link type="guide" xref="tech" group="gsettings"/>
    <revision pkgversion="3.0" date="2013-01-30" status="candidate"/>

    <credit type="author copyright">
      <name>Federico Mena Quintero</name>
      <email its:translate="no">federico@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>프로그램 기본 설정용 설정 저장소</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2016, 2017, 2018.</mal:years>
    </mal:credit>
  </info>

<title>GSettings</title>

  <p>GSettings는 표준 방식으로 프로그램 설정 및 사용자의 취향을 저장하는 <link xref="tech-glib">GLib</link>의 일부입니다.</p>

  <p>GSettings를 활용하느 프로그램에는 설정 키 <em>스키마</em>를 정의합니다. 각 키의 스키마에는 키 이름, 사람이 알아볼 수 있는 키 용도 설명, 키 형식(문자열, 정수형 등), 그리고 기본값 정보가 들어있습니다.</p>

  <p>GSettings는 운영 체제의 설정 데이터 저장소를 활용합니다. GNU 시스템에서는 dconf를, 윈도우에서는 레지스트리를, Mac OS 에서는 넥스트스텝 속성 목록 기술을 활용합니다.</p>

  <p>GSettings는 키 값 바뀜을 살펴볼 수 있어, 프로그램에서 전역 설정 바뀜에 즉각적으로 반응할 수 있습니다. 예를 들면, 시계를 표시하는 모든 프로그램은 다시 시작하지 않아도 즉시 12시간/24시간 시계 표시 형식의 전역 설정에 반응할 수 있습니다.</p>

  <list style="compact">
    <item><p><link href="http://developer.gnome.org/gio/stable/GSettings.html">GSettings 참고 설명서</link></p></item>
</list>


</page>
