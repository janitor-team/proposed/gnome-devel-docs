<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-tracker" xml:lang="ko">

  <info>
    <link type="guide" xref="tech" group="tracker"/>

    <credit type="author copyright">
      <name>Federico Mena Quintero</name>
      <email its:translate="no">federico@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>문서 메타데이터를 저장하고 전달합니다</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2016, 2017, 2018.</mal:years>
    </mal:credit>
  </info>

  <title>트래커</title>

  <p>트래커는 RDF(자원 데이터 형식) 저장소 엔진입니다. RDF는 제목:동사:객체 <em>세 부분</em>으로 이루어져있습니다. 예를 들어, 책 제목과 저자를 <code>Othello:has-author:William Shakespeare</code> 같은 식의 표준 3컬럼으로 둘 수 있습니다. 표준화 3컬럼 양식을 <em>온톨로지</em>라고 합니다.</p>

  <p>트래커는 이런 3컬럼 형식에 맞는 저장소와 SPARQL 질의 언어 처리 엔진을 제공합니다.</p>

  <p>그놈은 문서 메타데이터 처리용도로 트래커를 활용합니다. 문서 메타데이터에는 제목, 저자, 저작권, 수정일시, 키워드 정보가 있습니다. 여기 제시한 모든 메타데이터는 트래커에 RDF 3 컬럼 형식으로 저장하며 그놈 문서 같은 프로그램에서 SPARQL로 정보를 요청합니다. <link href="http://developer.gnome.org/ontology/unstable/">활용 온톨로지</link>는 다양한 표준 하위 온톨로지에 기반합니다. 문서 메타데이터에 활용하는 <link href="http://dublincore.org/">더블린 코어</link>, 주석, 파일, 연락처 기타 항목 용도로 활용하는 <link href="http://nepomuk.kde.org/">네포묵</link>이 있습니다.</p>

  <list style="compact">
    <item><p><link href="https://wiki.gnome.org/Projects/Tracker">트래커 홈페이지</link></p></item>
    <item><p><link href="https://wiki.gnome.org/Projects/Tracker/Documentation">트래커 문서</link></p></item>
  </list>

</page>
