<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="license" xml:lang="ko">

  <info>
    <link type="guide" xref="index" group="license"/>
    <revision pkgversion="3.14" date="2014-05-01" status="draft"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>프로그램에 어떤 라이선스를 적용할까요?</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2016, 2017, 2018.</mal:years>
    </mal:credit>
  </info>

  <title>프로그램에 라이선스 부여</title>

  <p>새 프로그램 또는 라이브러리를 작성할 때 라이선스를 선택하여, 다른 사람이 결과물을 어떻게 활용할 수 있을 지, 어떻게 재활용할 수 있을 지 알 수 있게 해야합니다.</p>

  <p>그놈에서 활용하는 라이브러리는 보통 <link href="http://www.gnu.org/">GNU</link> <link href="https://www.gnu.org/licenses/old-licenses/lgpl-2.1.html">LGPL 2.1+</link>을 라이선스로 부여합니다.</p>

  <p>일부 오래된 그놈 프로그램은  <link href="http://www.gnu.org/licenses/gpl-2.0.html">GPL2+</link>로 라이선스를 부여하는데 반해, 대부분의 최신 그놈 프로그램은 <link href="http://www.gnu.org/licenses/gpl-3.0.html">GPL3+</link>로 라이선스를 부여합니다.</p>

  <p>그놈 문서 팀이 작성한 사용자 도움말은 <link href="http://creativecommons.org/licenses/by-sa/3.0/">CC-by-SA 3.0 Unported</link>로 라이선스를 부여합니다. 문서팀에서는 위키피디아 및 기타 참고 소스에서 재활용할 수 있게 일관된 라이선스를 활용하려합니다.</p>

  <p>번역은 상위 문자열에 적용한 라이선스와 동일한 라이선스를 취합니다. 프로그램 문자열은 보통 GPL2+ 또는 GPL3+, 사용자 문서는 CC-by-SA 3.0입니다.</p>

  <p>그놈에서는 어떤 라이선스를 선택해야 하는지는 법적으로 자문해드릴 수 없지만, <link href="http://opensource.org/licenses">오픈 소스 헌장</link>, <link href="http://www.gnu.org/licenses/license-recommendations.html">FSF</link>, <link href="https://blogs.gnome.org/bolsh/2014/04/17/choosing-a-license/">Dave Neary의 "라이선스 선택하기" 블로그 게시글</link>에서 원하는 내용을 찾아보실 수 있을지도 모르겠습니다. 또한 플러그인을 활용하는 지스트리머 예제에 관심이 있다면, <link href="http://gstreamer.freedesktop.org/documentation/licensing.html">GStreamer 라이선스 정보</link>를 살펴보셔도 좋습니다.</p>
</page>
