<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="overview" id="overview-communication" xml:lang="el">
  <info>
    <revision version="0.1" date="2012-02-19" status="stub"/>
    <link type="guide" xref="index" group="communication"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email its:translate="no">philbull@gmail.com</email>
      <years>2012</years>
    </credit>

    <title type="link" role="trail">Επικοινωνία</title>
    <desc>Άμεσα μηνύματα, δικτύωση, κοινωνικά μέσα, αλληλογραφία και υποστήριξη ημερολογίου.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2010-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Τζένη Πετούμενου</mal:name>
      <mal:email>epetoumenou@gmail.com</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Θουκυδίδου</mal:name>
      <mal:email>marablack3@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Επικοινωνία και κοινωνική δικτύωση</title>

<list>
 <item>
  <p><em style="strong">Σύνδεση με άμεσα μηνύματα και υπηρεσίες κοινωνικής δικτύωσης</em></p>
 </item>
 <item>
  <p><em style="strong">Ορισμός συνδέσεων πολλαπλών πρωτοκόλλων με υπηρεσίες ιστού ή άλλους πελάτες</em></p>
 </item>
 <item>
  <p><em style="strong">Διαχείριση αλληλογραφίας, δικτυακές επαφές και υπηρεσίες ημερολογίου</em></p>
 </item>
</list>

<p>Συνδέστε τους χρήστες σας για να επικοινωνήσουν με τους φίλους τους και τις επαφές τους μέσα από άμεσα μηνύματα, κοινωνικά μέσα και αλληλογραφία. Η εκτεταμένη στοίβα επικοινωνιών του GNOME σας δίνει υψηλού επιπέδου, αποσπασμένη πρόσβαση σε περίπλοκα άμεσα μηνύματα και πρωτόκολλα αλληλογραφίας. Για περισσότερο εξειδικευμένες ανάγκες επικοινωνίας, υπάρχει πρόσβαση μέσα από χαμηλότερου επίπεδου APIs επίσης.</p>

<media type="image" mime="image/png" src="test_comm1.png" width="65%">
 <p>Πελάτης άμεσων μηνυμάτων Empathy</p>
</media>

<section id="what">
 <title>Τι μπορείτε να κάνετε;</title>

 <p>Για <em style="strong">σύνδεση με υπηρεσίες άμεσων μηνυμάτων</em>, χρησιμοποιήστε <em style="strong">Telepathy</em>. Παρέχει έναν ισχυρό σκελετό για αλληλεπίδραση με τις επαφές άμεσων μηνυμάτων του χρήστη και έχει υποστήριξη για μια πλατιά περιοχή πρωτοκόλλων μηνυμάτων. Με την Telepathy, όλες όλοι οι λογαριασμοί και οι συνδέσεις διαχειρίζονται από μια υπηρεσία συνεδρίας διαύλου δεδομένων που είναι βαθιά ενσωματωμένη στο GNOME. Οι εφαρμογές μπορούν να σχετιστούν με αυτήν την υπηρεσία για επικοινωνία με επαφές.</p>

 <p>Δημιουργήστε παιχνίδια πολλών παικτών ή συνεργαζόμενων επεξεργαστών που ενσωματώνονται με υπηρεσίες άμεσων μηνυμάτων ευρείας επιφάνειας εργασίας. Με το API <em style="strong" xref="tech-telepathy">Telepathy Tubes</em>, μπορείτε <em style="strong">να διοχετεύσετε ένα ελεύθερο πρωτόκολλο</em> μέσα από σύγχρονα πρωτόκολλα άμεσων μηνυμάτων όπως Jabber για τη δημιουργία διαδραστικών εφαρμογών.</p>

 <p>Επιτρέψτε στους χρήστες να δουν άλλα άτομα με τα οποία να μπορούν να συνομιλήσουν και βρείτε εκτυπωτές, κοινόχρηστα αρχεία και κοινόχρηστες μουσικές συλλογές μόλις συνδεθούν σε ένα δίκτυο. Η API <em style="strong" xref="tech-avahi">Avahi</em> παρέχει <em style="strong">εύρεση υπηρεσιών</em> σε ένα τοπικό δίκτυο μέσα από την σειρά πρωτοκόλλου mDNS/DNS-SD. Είναι συμβατή με παρόμοια τεχνολογία που βρίσκεται σε MacOS Χ και Windows.</p>

 <p>Διαχειριστείτε τα τοπικά και διαδικτυακά βιβλία διευθύνσεων και ημερολόγια των χρηστών με τον <em style="strong" xref="tech-eds">διακομιστή δεδομένων Evolution</em> (EDS). Παρέχει έναν τρόπο για αποθήκευση και αλληλεπίδραση με τις πληροφορίες του λογαριασμού...</p>

 <p>Με το <em style="strong" xref="tech-folks">Folks</em>, θα έχετε πρόσβαση σε ένα μοναδικό API για διαχείριση κοινωνικής δικτύωσης, συνομιλίας, αλληλογραφίας και επικοινωνίες ήχου/βίντεο.</p>

</section>

<!--<section id="samples">
 <title>Code samples</title>
 <list>
  <item><p>Change the IM status</p></item>
  <item><p>Fetch a contact from a Gmail address book</p></item>
  <item><p>Scan the network for zeroconf printers</p></item>
  <item><p>Something with Telepathy Tubes</p></item>
 </list>
</section>-->

<section id="realworld">
 <title>Πραγματικά παραδείγματα</title>

 <p>Μπορείτε να δείτε πολλές πραγματικές εφαρμογές των τεχνολογιών επικοινωνιών του GNOME σε έργα ανοικτού λογισμικού, όπως τα παραδείγματα που δίνονται παρακάτω.</p>
 <list>
  <item>
   <p>Το <em style="strong">Empathy</em> είναι μια εφαρμογή άμεσου μηνύματος με υποστήριξη για μια πλατιά περιοχή υπηρεσιών μηνυμάτων. Χρησιμοποιεί Telepathy για διαχείριση συνδέσεων, παρουσίας και πληροφοριών επαφών για όλα τα πρωτόκολλα που υποστηρίζει.</p>
   <p>(<link href="https://wiki.gnome.org/Apps/Empathy">Website</link> | <link href="https://wiki.gnome.org/Apps/Empathy#Screenshots">Screenshots</link> | <link href="https://gitlab.gnome.org/GNOME/empathy/">Empathy source code</link> )</p>
  </item>

  <item>
   <p>Με υποστήριξη Telepathy Tubes, η συλλογή <em style="strong">Παιχνίδια GNOME</em> μπορεί να προσθέσει υποστήριξη παιχνιδιού πολλών παικτών μέσα από το πρωτόκολλο Jabber.</p>
   <p>(<link href="https://wiki.gnome.org/Projects/Games">Website</link> | <link href="https://wiki.gnome.org/Apps/Chess#Screenshots">Screenshot</link> | <link href="https://gitlab.gnome.org/GNOME/gnome-chess/">GNOME Chess online multiplayer code</link> )</p>
  </item>

  <item>
   <p>Η υποστήριξη Avahi επιτρέπει στους χρήστες του αναπαραγωγού μουσικής <em style="strong">Rhythmbox</em> να δει κοινόχρηστες μουσικές συλλογές στο τοπικό τους δίκτυο, χρησιμοποιώντας DAAP.</p>
   <p>(<link href="https://wiki.gnome.org/Apps/Rhythmbox">Website</link> | <link href="https://wiki.gnome.org/Apps/Rhythmbox/Screenshots">Screenshots</link> | <link href="https://gitlab.gnome.org/GNOME/rhythmbox/tree/master/plugins/daap">DAAP Code</link> )</p>
  </item>
 </list>

</section>

</page>
