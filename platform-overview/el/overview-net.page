<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="overview-net" xml:lang="el">
  <info>
    <link type="guide" xref="index" group="net"/>
    <revision version="0.1" date="2013-08-06" status="review"/>

    <credit type="author copyright">
      <name>David King</name>
      <email its:translate="no">davidk@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Επικοινωνία πελάτη και διακομιστή HTTP, φορητή δικτύωση εισόδου/εξόδου βασισμένα σε υποδοχές και διαχείριση συσκευής δικτύου.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2010-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Τζένη Πετούμενου</mal:name>
      <mal:email>epetoumenou@gmail.com</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Θουκυδίδου</mal:name>
      <mal:email>marablack3@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Χαμηλού επιπέδου δικτύωση</title>

<list>
 <item>
  <p><em style="strong">Δημιουργήστε ισχυρούς και ευέλικτους διακομιστές HTTP και πελάτες</em></p>
 </item>
 <item>
  <p><em style="strong">Χρησιμοποιήστε φορητά API βασισμένα σε υποδοχές σε μια γραφική διεπαφή χωρίς περιορισμούς</em></p>
 </item>
 <item>
  <p><em style="strong">Αναγνωρίστε και διαχειριστείτε την κατάσταση σύνδεσης του δικτύου</em></p>
 </item>
</list>

<p>Επωφεληθείτε από τα φορητά API δικτύωσης για να προσπελάσετε υπηρεσίες δικτύου. Η ασύγχρονη είσοδος/έξοδος κρατά σε επαγρύπνηση τη διεπαφή χρήστη της εφαρμογής ενώ εξελίσσεται η είσοδος/έξοδος. Ανιχνεύστε τις αλλαγές στην κατάσταση δικτύωσης του συστήματος, για να κάνετε την εφαρμογή σας να απαντά κατάλληλα όταν δεν υπάρχει πρόσβαση στο διαδίκτυο.</p>

<!-- TODO: Add screenshot.
<media type="image" mime="image/png" src="test_comm1.png" width="65%">
 <p>IMAGE</p>
</media>
-->

<section id="what">
  <title>Τι μπορείτε να κάνετε;</title>

  <p>Για <em style="strong">ασύγχρονη πρόσβαση σε χαμηλού επιπέδου δικτύωσης API</em>, χρησιμοποιήστε το <em style="strong" xref="tech-gio-network">GIO networking</em>. Υψηλότερου επιπέδου API είναι διαθέσιμο για <em style="strong">επίλυση μεσολαβητών και εγγραφών DNS</em> καθώς και για χρήση <em style="strong">ασφαλών υποδοχών (TLS)</em>.</p>

  <p>Η παρακολούθηση κατάστασης του δικτύου είναι διαθέσιμη στο GIO, αλλά ο <em style="strong" xref="tech-network-manager">NetworkManager</em> παρέχει <em style="strong">εκτεταμένη υποστήριξη για συσκευές δικτύωσης</em> και τοπολογίες δικτύου.</p>

  <p>Το <em style="strong" xref="tech-soup">Libsoup</em> παρέχει μια ευέλικτη διεπαφή για <em style="strong">διακομιστές HTTP και πελάτες</em>. Παρέχονται σύγχρονα και ασύγχρονα API.</p>

</section>

<!-- TODO Add link to code examples.
<section id="samples">
 <title>Code samples</title>
 <list>
  <item><p>A sample we should write</p></item>
  <item><p><link xref="samples">More…</link></p></item>
 </list>
</section>
-->

<section id="realworld">
  <title>Πραγματικά παραδείγματα</title>

  <p>Μπορείτε να δείτε πολλές πραγματικές εφαρμογές των τεχνολογιών δικτύωσης του GNOME σε έργα ανοικτού λογισμικού, όπως τα παραδείγματα που δίνονται παρακάτω.</p>
  <list>
    <item>
      <p>Ο <em style="strong">Ιστός</em> είναι ο περιηγητής GNOME, που χρησιμοποιεί libsoup για να προσπελάσει υπηρεσίες HTTP.</p>
      <p>( <link href="https://wiki.gnome.org/Apps/Web">Website</link> | <link href="https://gitlab.gnome.org/GNOME/epiphany/raw/master/data/screenshot.png">Screenshot</link> | <link href="https://gitlab.gnome.org/GNOME/epiphany/">Source code</link> )</p>
    </item>
    <item>
      <p>Το <em style="strong">GNOME Shell</em> είναι η επιφάνεια εργασίας GNOME ορατή από τον χρήστη, που χρησιμοποιεί NetworkManager για το μενού κατάστασης δικτύου, συμπεριλαμβανόμενης ενσύρματης διαχείρισης, ασύρματης, μόντεμ 3G και συστήματα δικτύωσης VPN.</p>
      <p>( <link href="https://wiki.gnome.org/Projects/GnomeShell">Website</link> | <link href="http://www.gnome.org/gnome-3/">Screenshot</link> | <link href="https://gitlab.gnome.org/GNOME/gnome-shell/">Source Code</link> )</p>
    </item>
    <!-- TODO: Add low-level GIO network IO example. -->
  </list>
</section>
</page>
