<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-help" xml:lang="de">

  <info>
    <link type="guide" xref="tech" group="help"/>
    <revision pkgversion="3.0" date="2011-04-05" status="final"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011–2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Themenorientiertes Internet-Hilfesystem</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2009-2012, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011, 2012, 2015, 2017, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aljosha Papsch</mal:name>
      <mal:email>al@rpapsch.de</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

<title>Hilfe</title>

<p>Benutzer benötigen gelegentlich etwas Hilfe, selbst für die perfektesten Anwendungen. GNOME verfügt über ein eingebautes, themenorientiertes Hilfesystem, welches die Auszeichnungssprache <link href="http://projectmallard.org/">Mallard</link> nutzt. Von den GNOME-Entwicklern ins Leben gerufen, ist Mallard eine agile und dynamische Sprache, die das Schreiben und Überarbeiten erheblich erleichtert. Themenorientiert bedeutet in diesem Zusammenhang, dass die Benutzer die benötigten Antworten finden können, ohne das Handbuch vollständig lesen zu müssen. Mit seinem einzigartigen Verweis- und Organisationssystem ist Mallard die einzige Sprache, die mit Plugins und Hinzufügungen von Drittanbietern in einem einzigen zusammenhängenden Dokument umgehen kann.</p>

<p>Falls Sie linear aufgebaute Handbücher benötigen, unterstützt GNOME auch das als Industriestandard designierte <link href="http://docbook.org/">DocBook</link>-Format.</p>

<list style="compact">
  <item><p><link href="http://projectmallard.org/about/learn/tenminutes.html">Zehnminütige Mallard-Tour</link></p></item>
  <item><p><link href="http://projectmallard.org/">Die Webseite von Mallard</link></p></item>
  <item><p><link href="http://docbook.org/">Die Webseite von DocBook</link></p></item>
</list>
</page>
