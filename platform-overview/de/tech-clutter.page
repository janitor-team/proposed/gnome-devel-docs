<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-clutter" xml:lang="de">

  <info>
    <link type="guide" xref="tech" group="clutter"/>
    <revision pkgversion="3.0" date="2011-04-05" status="incomplete"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011–2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Animationen und Szene-Graphen</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2009-2012, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011, 2012, 2015, 2017, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aljosha Papsch</mal:name>
      <mal:email>al@rpapsch.de</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

<title>Clutter</title>

<p>Clutter ist eine Bibliothek zum Darstellen von Animationen auf einer 2,5-D-Zeichenfläche. Sie erstellen grafische Objekte aus Bildern, Sie können diese später verschieben und drehen oder ihnen Quasi-3D-Effekte verleihen.</p>

<p>Clutter benutzt die <link href="http://www.khronos.org/opengl/">OpenGL</link>- und <link href="http://www.khronos.org/opengles/">OpenGL|ES</link>-Industriestandard-Schnittstelle für den Zugriff auf beschleunigte Grafikhardware sowohl auf dem Arbeitsplatzrechner als auch auf mobilen Umgebungen und all das ohne die Komplexität der GPU-Pipeline-Programmierung offenzulegen.</p>

<p>Clutter spezifiziert keinen visuellen Stil und stellt keine vordefinierten, komplexen Steuerungen für Benutzerschnittstellen bereit. Es lässt dem Entwickler offen was benötigt wird. Durch eine flexible Szenengraphen-API können Szenenelemente (oder <em>Akteure</em>) frei auf dem Hauptsichtfeld (oder <em>Stage</em>) platziert werden.</p>

  <p>Clutter ist mit vordefinierten Akteuren zum Darstellen von einheitlichen Farben, Bilddaten, Texten und eigenen hochpräzisen 2D-Zeichnungen ausgestattet, die mithilfe der <link xref="tech-cairo">Cairo</link>-Schnittstelle ermöglicht werden. Clutter stellt auch generische Klassen zum Strukturieren einer Benutzerschnittstelle bereit, ganz ähnlich wie bei <link xref="tech-gtk">GTK</link> mit einem Boxenmodell und einer Reihe von Freiform-<em>Einschränkungen</em>.</p>

<p>Clutter bietet ein erweiterbares Animations-Framework und grafische Effekte an. Eine Animation ist mit einer Zeitachse verknüpft und ändert mit der Zeit eine oder mehrere Eigenschaften von einem oder mehreren Akteuren, z.B. die Rotation in einer bestimmten Dimension, Maßstab, Größe, Deckkraft, etc.</p>

<p>Eine Reihe an Bibliotheken von Drittanbietern ermöglicht die Integration mit anderen Technologien, wie zum Beispiel: Clutter-GTK für das Einbetten eines Clutter-Sichtfelds in einer GTK-Anwendung; Clutter-GStreamer für das Einbetten von GStreamers Video- und Audio-Pipelines; Clutter-Box2D und Clutter-Bullet für physikalische Interaktion sowohl in 2D- als auch in 3D-Umgebungen.</p>

<list style="compact">
  <item><p><link href="https://developer.gnome.org/clutter-cookbook/stable/">Das Clutter-Kochbuch</link></p></item>
  <item><p><link href="http://developer.gnome.org/clutter/stable">Das Referenzhandbuch von Clutter</link></p></item>
  <item><p><link href="http://www.clutter-project.org">Die Clutter-Webseite</link></p></item>
</list>

</page>
