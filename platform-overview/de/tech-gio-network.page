<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-gio-network" xml:lang="de">

  <info>
    <link type="guide" xref="tech" group="gio-network"/>
    <revision pkgversion="3.0" date="2011-04-05" status="review"/>

    <credit type="author copyright">
      <name>Federico Mena Quintero</name>
      <email its:translate="no">federico@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Netzwerk- und Socket-API mit Datenströmen</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2009-2012, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011, 2012, 2015, 2017, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aljosha Papsch</mal:name>
      <mal:email>al@rpapsch.de</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

<title>Netzwerk mit GIO</title>

  <p>Aufgebaut auf den Datenstrom-Schnittstellen <link xref="tech-gio">für Dateien</link> bietet GIO allgemeine Netzwerk-Programmierschnittstellen zur Kommunikation über TCP/IP und UNIX-Domain-Sockets. Sie können die GIO-Netzwerk-Programmierschnittstellen dazu benutzen, sich zu einem Server zu verbinden, auf Ereignisse zu lauschen und Ressourcen zu lesen. Das asynchrone Design der Programmierschnittstellen bedeutet, dass Ihre Anwendung nicht blockiert, während sie auf eine Antwort wartet.</p>

<list style="compact">
  <item><p><link href="https://developer.gnome.org/gio/stable/">Das Referenzhandbuch von GIO</link></p></item>
  <item><p><link href="https://developer.gnome.org/gio/stable/networking.html">Systemnahe Netzwerk-Unterstützung</link></p></item>
  <item><p><link href="https://developer.gnome.org/gio/stable/highlevel-socket.html">Abstrakte Netzwerk-Funktionalität</link></p></item>
</list>

</page>
