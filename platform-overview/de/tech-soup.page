<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-soup" xml:lang="de">

  <info>
    <link type="guide" xref="tech" group="soup"/>

    <credit type="author copyright">
      <name>Federico Mena Quintero</name>
      <email its:translate="no">federico@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Asynchrone HTTP-Bibliothek mit Cookies, SSL und XML-RPC</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2009-2012, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011, 2012, 2015, 2017, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aljosha Papsch</mal:name>
      <mal:email>al@rpapsch.de</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

<title>Soup</title>

  <p>Soup, allgemein libsoup genannt, ist eine HTTP-Bibliothek, die in grafischen Anwendungen genutzt wird, die asynchrone Operationen benötigen, damit die Benutzeroberfläche nicht blockiert wird, wenn Netzwerkanfragen laufen.</p>

  <p>Soup stellt Funktionalität für die Verwendung von HTTP-Cookies, für SSl-verschlüsselte Verbindungen und das auf HTTP basierende XML-RPC-Protokoll bereit.</p>

  <note>
    <p>Nebenbei: Soup heißt »soup«, weil es als Bibliothek für SOAP-Anfragen über HTTP entworfen wurde. spanische Muttersprachler, die Englisch lernen, wurden häufig durch die Wörter »soup« und »soap« verwirrt, daher erschien dies als lustige und interessante Namenswahl.</p>
  </note>

  <list style="compact">
    <item><p><link href="https://developer.gnome.org/libsoup/stable/">Das Referenzhandbuch von Soup</link></p></item>
  </list>

</page>
