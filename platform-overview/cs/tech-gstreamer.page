<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-gstreamer" xml:lang="cs">

  <info>
    <link type="guide" xref="tech" group="gstreamer"/>
    <revision pkgversion="3.0" date="2011-04-05" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011 – 2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Přehrávání, mixování a další činnosti se zvukem a videem</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lucas Lommer</mal:name>
      <mal:email>llommer@svn.gnome.org</mal:email>
      <mal:years>2009.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2015.</mal:years>
    </mal:credit>
  </info>

<title>GStreamer</title>

<p>GStreamer je mocná multimediální knihovna sloužící k přehrávání, vytváření a úpravám zvuku, videa a dalších multimédií. Můžete ji využít k přehrávání zvuku a videa, k nahrávání z různých multimediálních zdrojů a k úpravám multimediálního obsahu. GStreamer podporuje již v základu kódování a dekódování řady formátů a pro další je možné přidat podporu pomocí zásuvných modulů.</p>

<p>GStreamer poskytuje flexibilní architekturu, ve které jsou multimédia zpracovávána zkrz rouru sestavenou z elementů. Každý z elementů může na obsah aplikovat filtry, jako je zakódování nebo dekódování, spojení více zdrojů dohromady nebo transformace multimediálního obsahuj. Tato architektura umožňuje libovolně uspořádat takovéto elementy, takže můžete virtuálně dosáhnout libovolného efektu. Navíc je GStreamer navržen tak, aby způsoboval co nejmenší ztrátu výkonu, takže jej lze použít i v aplikacích s vysokými požadavky na minimální spoždění.</p>

<p>Mimo to, že GStreamer poskytuje mocné API pro manipulaci s multimédii, nabízí i pohodlné rutiny pro prosté přehrávání. Umí automaticky sestavit rouru, která načte a přehraje soubory v kterémkoliv z podporovaných formátů, takže ve své aplikaci můžete jednoduše použít zvuky a videa.</p>

<p>Architektura GStreamer dokáže používat zásuvné moduly, přes které se přidávájí kodéry, dekodéry a všechny další typy filtrů obsahu. Vývojáři třetích stran mohou poskytnou zásuvné moduly, které budou automaticky dostupné ostatním aplikacím používajícím GStreamer. Zásuvné moduly mohou nabídnout podporu pro další multimediální formáty nebo další funkcionalitu a efekty.</p>

<p>GStreamer byste měli použít vždy, když podtřebujete ve své aplikaci číst a přehrávat multimediální obsah nebo když vaše aplikace potřebuje pracovat se zvukem a videem. GStreamer zajistí, že vývoj vaší aplikace bude snadný a nabídne dobře otestované prvky pro většinu toho, co potřebujete.</p>

<list style="compact">
  <item><p><link href="http://gstreamer.freedesktop.org/data/doc/gstreamer/head/manual/html/index.html">Příručka k vývoji aplikací využívajících GStreamer</link></p></item>
  <item><p><link href="http://gstreamer.freedesktop.org/data/doc/gstreamer/head/gstreamer/html/">Základní příručka knihovny GStreamer 1.0</link></p></item>
  <item><p><link href="http://gstreamer.freedesktop.org/documentation/">Stránka s dokumentací knihovny GStreamer</link></p></item>
  <item><p><link href="http://gstreamer.freedesktop.org">Webové stránky knihovny GStreamer</link></p></item>
</list>

</page>
