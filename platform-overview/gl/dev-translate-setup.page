<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dev-translate-setup" xml:lang="gl">

  <info>
    <link type="next" xref="dev-translate-build"/>
    <revision version="0.1" date="2013-06-19" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2009-2020.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leandro Regueiro</mal:name>
      <mal:email>leandro.regueiro@gmail.com</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  </info>

  <title>Mark strings for translation</title>

  <links type="series" style="floatend">
    <title>Set up translations</title>
  </links>

  <p>Before the strings from your application can be translated, they need to
  be extracted from the source code.</p>

  <p>Wrap messages or <em>string literals</em> in your code with the
  '<code>_()</code>' macro.</p>

  <note>
    <p>For C, this macro is defined in the <file>glib/gi18n.h</file> header
    file, which must be included at the top of your application source.</p>
  </note>

  <p>Your wrapped strings should look like this:</p>
  <code>_("Press a key to continue")</code>

  <p>This marks the strings as translatable, and at runtime calls
  <app>gettext</app> to substitute the translated strings.</p>

</page>
